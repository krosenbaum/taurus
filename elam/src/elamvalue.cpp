//  value definition implementation
//
// (c) Konrad Rosenbaum, 2010
// protected under the GNU LGPL v3 or at your option any newer

#include "elamvalue.h"

#include<QDebug>

namespace ELAM {

Exception::Exception(const Exception& e)
{
	operator=(e);
}

Exception::Exception(const QVariant& v)
{
	operator=(v.value<Exception>());
}
Exception::Exception(Exception::ErrorType type, Position pos, QString errorText)
{
	mtype=type;
	merr=errorText;
	mpos=pos;
}


Exception::Exception(ErrorType tp,QString errText, Position pos)
{
	mtype=tp;
	merr=errText;
	mpos=pos;
}

static int Exception_metaid=qRegisterMetaType<Exception>();
int Exception::metaTypeId()
{
	return Exception_metaid;
}

Exception& Exception::operator=(const Exception& e)
{
	mtype=e.mtype;
	merr=e.merr;
	mpos=e.mpos;
	return *this;
}

QString Exception::errorTypeName() const
{
	switch(mtype){
		case NoError:return "no error";
		///error while parsing the expression
		case ParserError:return "parser error";
		///the operator is not known
		case UnknownOperatorError:return "unknown operator";
		///the function is not known
		case UnknownFunctionError:return "unknown function";
		///unknown variable or constant
		case UnknownValueError:return "unknown variable or constant";
		///the operator or function is known, but the type of one of the arguments is not supported
		case TypeMismatchError:return "type mismatch";
		///the amount of arguments to a function is wrong or the syntax of arguments
		case ArgumentListError:return "invalid argument list";
		///the operation itself failed, e.g. division by zero
		case OperationError:return "invalid operation";
		default:return "unknown error";
	}
}


static int AnyType_metaid=qRegisterMetaType<AnyType>();
int AnyType::metaTypeId()
{
	return AnyType_metaid;
}

QDebug& operator<< ( QDebug dbg, const ELAM::Position& pos)
{
	if(!pos.isValid())dbg.nospace()<<"Position(invalid)";
	else dbg.nospace()<<"Position(line="<<pos.line()<<",col="<<pos.column()<<")";
	return dbg.space();
}

QDebug&operator<<(QDebug dbg,const Exception&ex)
{
	dbg.nospace()<<"Exception(type=";
	switch(ex.errorType()){
		case Exception::NoError:dbg<<"NoError";break;
		case Exception::ParserError:dbg<<"ParserError";break;
		case Exception::UnknownOperatorError:dbg<<"UnknownOperation";break;
		case Exception::UnknownFunctionError:dbg<<"UnknownFunction";break;
		case Exception::UnknownValueError:dbg<<"UnknownValue";break;
		case Exception::TypeMismatchError:dbg<<"TypeMismatch";break;
		case Exception::ArgumentListError:dbg<<"ArgumentList";break;
		case Exception::OperationError:dbg<<"Operation";break;
		default:dbg<<"Unknown:"<<(int)ex.errorType();break;
	}
	if(ex.errorText()!="")
		dbg<<",text="<<ex.errorText();
	dbg<<",pos="<<ex.errorPos();
	dbg.nospace()<<")";
	return dbg.space();
}
QDebug&operator<<(QDebug dbg,const AnyType&){dbg.nospace()<<"AnyType";return dbg.space();}

static bool Exception_Conversion=
	QMetaType::registerConverter<Exception,QString>([](const Exception&e)->QString{return QString("%1 at %2:%3: %4").arg(e.errorTypeName()).arg(e.position().line()).arg(e.position().column()).arg(e.errorText());});

static int structTypeId=qRegisterMetaType<Structure>();
static int listTypeId=qRegisterMetaType<List>();

int structureMetaTypeId()
{
    return structTypeId;
}

int listMetaTypeId()
{
    return listTypeId;
}


//end of namespace
};
