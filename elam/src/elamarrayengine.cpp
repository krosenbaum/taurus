// ELAM array/structure engine definition implementation
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU LGPL v3 or at your option any newer

// //////////////////////////////////////////////
// IMPORTANT NOTE
// ================
//
// If you change methods in here, please double check all other functions/methods/calls/etc.
// Also check:
//  - elamexpression.cpp, Expression::evalDot()/::evalArray()
//  - elamengine.cpp, traverseValue static function
//
// //////////////////////////////////////////////

#include "elamarrayengine.h"
#include "elamexpression.h"

#include <QDebug>
#include <QList>
#include <QMap>
#include <QMetaObject>
#include <QMetaProperty>

#include <algorithm>

using namespace ELAM;

ArrayEngine::ArrayEngine()
{
    configureArrayEngine(*this);
}

///internal error: argument is not any array type at all
#define E_NOARRAY -1
///internal array: argument's QObject/QGadget accessors don't work
#define E_OBJERR -2

int ArrayEngine::countArray(const QVariant&ar)
{
    const QVariant&data=ar;
    //numbered array
    const int utype=data.userType();
    if(utype==listMetaTypeId())
        return data.value<QList<QVariant>>().size();
    //trivial structure
    if(utype==structureMetaTypeId())
        return data.value<QMap<QString,QVariant>>().size();
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        const QMetaObject*mo=mt.metaObject();
        return mo->propertyCount();
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return E_OBJERR;
        }
        return o->metaObject()->propertyCount()+o->dynamicPropertyNames().size();
    }
    //must be a non-array type
    return E_NOARRAY;
}

static QVariant countArray(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument of array or structure type");
    int r=ArrayEngine::countArray(in[0]);
    switch(r){
        case E_NOARRAY:return Exception(Exception::ArgumentListError,"argument must be of array or structure type");
        case E_OBJERR:return Exception(Exception::OperationError,"unable to resolve QObject* argument");
        default:return r;
    }
}

bool ArrayEngine::isArray(const QVariant&ar)
{
    const QVariant&data=ar;
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==listMetaTypeId() || utype==structureMetaTypeId())
        return true;
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget)
        return true;
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>())
        return true;
    //must be a non-array type
    return false;
}

static QVariant isArray(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    return ArrayEngine::isArray(in[0]);
}

static QVariant isNumArray(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    const QVariant&data=in[0];
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==listMetaTypeId())
        return true;
    //must be a non-array or named array type
    return false;
}

static QVariant isMap(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    const QVariant&data=in[0];
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==structureMetaTypeId())
        return true;
    //something else
    return false;
}

static QVariant isGadget(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    const QVariant&data=in[0];
    //numbered array or simple structure
    QMetaType mt(data.userType());
    if(mt.flags()&QMetaType::IsGadget)
        return true;
    //something else
    return false;
}

static QVariant isQObject(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    const QVariant&data=in[0];
    //numbered array or simple structure
    QMetaType mt(data.userType());
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>())
        return true;
    //something else
    return false;
}

static QVariant isStructure(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument");
    const QVariant&data=in[0];
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==structureMetaTypeId())
        return true;
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget)
        return true;
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>())
        return true;
    //must be a non-array type
    return false;
}

QVariantList ArrayEngine::arrayKeys(const QVariant&data)
{
    //numbered array or simple structure
    const int utype=data.userType();

    if(utype==listMetaTypeId()){
        QList<QVariant>ret;
        const auto sz=data.value<QList<QVariant>>().size();
        for(int i=0;i<sz;i++)ret.append(i);
        return ret;
    }
    if(utype==structureMetaTypeId()){
        QList<QVariant>ret;
        for(const auto&k:data.value<QMap<QString,QVariant>>().keys())
            ret.append(k);
        return ret;
    }
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        QList<QVariant>ret;
        const QMetaObject*mo=mt.metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            ret.append(mo->property(i).name());
        return ret;
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return QVariantList();
        }
        QList<QVariant>ret;
        const auto*mo=o->metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            ret.append(mo->property(i).name());
        for(const auto&k:o->dynamicPropertyNames())
            ret.append(QString::fromLatin1(k));
        return ret;
    }
    //must be a non-array type
    return QVariantList();
}

static QVariant arrayKeys(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument of array/structure type");
    const QVariant&data=in[0];
    if(!ArrayEngine::isArray(data))
        return Exception(Exception::ArgumentListError,"non-structure argument has no keys");
    else
        return ArrayEngine::arrayKeys(data);
}

bool ArrayEngine::arrayHasKey(const QVariant&data,const QVariant&key)
{
    //numbered array or simple structure
    const int utype=data.userType();

    if(utype==listMetaTypeId()){
        const auto sz=data.value<QList<QVariant>>().size();
        if(key.canConvert<qlonglong>()){
            auto k=key.value<qlonglong>();
            return k>=0 && k<sz;
        }else
            return false;
    }
    if(utype==structureMetaTypeId()){
        auto k=key.toString();
        return data.value<QMap<QString,QVariant>>().keys().contains(k);
    }
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        auto k=key.toString();
        const QMetaObject*mo=mt.metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            if(mo->property(i).name() == k)return true;
        return false;
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return false;
        }
        auto k=key.toString();
        const auto*mo=o->metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            if(mo->property(i).name() == k)return true;
        return o->dynamicPropertyNames().contains(k.toLatin1());
    }
    //must be a non-array type
    return false;
}

static QVariant arrayHasKey(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=2)
        return Exception(Exception::ArgumentListError,"expected 2 arguments: array/structure, key");
    const QVariant&data=in[0];
    const QVariant&key=in[1];
    if(!ArrayEngine::isArray(data))
        return Exception(Exception::ArgumentListError,"non-structure argument has no keys");
    if(!key.canConvert<qlonglong>() && !key.canConvert<QString>())
        return Exception(Exception::ArgumentListError,"key must be integer or string");
    return ArrayEngine::arrayHasKey(data,key);
}

QVariantList ArrayEngine::arrayValues(const QVariant&data)
{
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==listMetaTypeId())
        return data.value<QVariantList>();
    if(utype==structureMetaTypeId())
        return data.value<QMap<QString,QVariant>>().values();
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        QList<QVariant>ret;
        const QMetaObject*mo=mt.metaObject();
        const void*ptr=data.data();
        for(int i=0;i<mo->propertyCount();i++)
            ret.append(mo->property(i).readOnGadget(ptr));
        return ret;
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return QVariantList();
        }
        QList<QVariant>ret;
        const auto*mo=o->metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            ret.append(mo->property(i).read(o));
        for(const auto&k:o->dynamicPropertyNames())
            ret.append(o->property(k));
        return ret;
    }
    //must be a non-array type
    return QVariantList();
}

static QVariant arrayValues(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return Exception(Exception::ArgumentListError,"expected 1 argument or array/structure type");
    const QVariant&data=in[0];
    if(!ArrayEngine::isArray(data))
        return Exception(Exception::ArgumentListError,"argument is not an array or structure");
    else
        return ArrayEngine::arrayValues(data);
}

Structure ArrayEngine::arrayStructure(const QVariant&data)
{
    //numbered array or simple structure
    const int utype=data.userType();
    if(utype==listMetaTypeId()){
        auto list=data.value<QVariantList>();
        Structure r;
        for(qlonglong i=0;i<list.size();i++)r.insert(QString::number(i),list[i]);
        return r;
    }
    if(utype==structureMetaTypeId())
        return data.value<QMap<QString,QVariant>>();
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        Structure ret;
        const QMetaObject*mo=mt.metaObject();
        const void*ptr=data.data();
        for(int i=0;i<mo->propertyCount();i++){
            auto prop=mo->property(i);
            ret.insert(prop.name(),prop.readOnGadget(ptr));
        }
        return ret;
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return Structure();
        }
        Structure ret;
        const auto*mo=o->metaObject();
        for(int i=0;i<mo->propertyCount();i++){
            auto prop=mo->property(i);
            ret.insert(prop.name(),prop.read(o));
        }
        for(const auto&k:o->dynamicPropertyNames()){
            ret.insert(k,o->property(k));
        }
        return ret;
    }
    //must be a non-array type
    return Structure();
}

static QVariant arrayCat(const QList<QVariant>&in,Engine&)
{
    return in;
}

static QVariant arraySortStr(const QList<QVariant>&in,Engine&)
{
    if(in.size()<1 || in.size()>2)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 1/2 arguments: list, [reverse]");
    if(!in[0].canConvert<QList<QVariant>>())
        return ELAM::Exception(Exception::ArgumentListError,"Expecting a list (numerically indexed array).");
    QList<QVariant>data=in[0].value<QList<QVariant>>();
    bool rev=false;
    if(in.size()>1){
        if(!in[1].canConvert<bool>())
            return ELAM::Exception(Exception::ArgumentListError,"Expecting a boolean for reverse.");
        rev=in[1].toBool();
    }
    if(rev)
        std::sort(data.begin(),data.end(),[](const QVariant&v1,const QVariant&v2)->bool{return v1.toString()>v2.toString();});
    else
        std::sort(data.begin(),data.end(),[](const QVariant&v1,const QVariant&v2)->bool{return v1.toString()<v2.toString();});
    return data;
}

static QVariant arraySortInt(const QList<QVariant>&in,Engine&)
{
    if(in.size()<1 || in.size()>2)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 1/2 arguments: list, [reverse]");
    if(!in[0].canConvert<QList<QVariant>>())
        return ELAM::Exception(Exception::ArgumentListError,"Expecting a list (numerically indexed array).");
    QList<QVariant>data=in[0].value<QList<QVariant>>();
    bool rev=false;
    if(in.size()>1){
        if(!in[1].canConvert<bool>())
            return ELAM::Exception(Exception::ArgumentListError,"Expecting a boolean for reverse.");
        rev=in[1].toBool();
    }
    if(rev)
        std::sort(data.begin(),data.end(),[](const QVariant&v1,const QVariant&v2)->bool{return v1.toLongLong()>v2.toLongLong();});
    else
        std::sort(data.begin(),data.end(),[](const QVariant&v1,const QVariant&v2)->bool{return v1.toLongLong()<v2.toLongLong();});
    return data;
}

static QVariant arrayRange(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=3)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 3 arguments: list, first, length.");
    if(!in[0].canConvert<QList<QVariant>>())
        return ELAM::Exception(Exception::ArgumentListError,"Expecting a list (numerically indexed array).");
    QList<QVariant>data=in[0].value<QList<QVariant>>();
    if(!in[1].canConvert<qlonglong>() || !in[2].canConvert<qlonglong>())
        return ELAM::Exception(Exception::ArgumentListError,"Expecting integer for first and length.");
    const qlonglong first=in[1].toLongLong(), length=in[2].toLongLong();
    return data.mid(first,length);
}

static QVariant arrayReverse(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=1)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting one argument.");
    if(!in[0].canConvert<QList<QVariant>>())
        return ELAM::Exception(Exception::ArgumentListError,"Expecting a list (numerically indexed array).");
    QList<QVariant>data=in[0].value<QList<QVariant>>();
    return QList<QVariant>(data.rbegin(),data.rend());
}

static QVariant arraySeq(const QList<QVariant>&in,Engine&)
{
    if(in.size()<2 || in.size()>3){
        return ELAM::Exception(Exception::ArgumentListError,"Expecting: start,end [,incr]");
    }
    if(!in[0].canConvert<qlonglong>() || !in[1].canConvert<qlonglong>()){
        return ELAM::Exception(Exception::TypeMismatchError,"Expecting: start,end must be integer.");
    }
    qlonglong start=in[0].toLongLong(),end=in[1].toLongLong(),incr=1;
    if(in.size()>2){
        if(!in[2].canConvert<qlonglong>())
            return ELAM::Exception(Exception::TypeMismatchError,"Expecting: incr must be integer.");
        incr=in[2].toLongLong();
        if(incr<=0)
            return ELAM::Exception(Exception::ArgumentListError,"Expecting: incr must be >=1.");
    }
    QVariantList r;
    for(qlonglong i=start;i<=end;i+=incr)r.append(i);
    return r;
}

static QVariant arrayMSelect(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=2)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 2 arguments: sourcelist,keylist");
    if(!ArrayEngine::isArray(in[0]) || !ArrayEngine::isArray(in[1]))
        return ELAM::Exception(Exception::TypeMismatchError,"Arguments must be arrays.");
    auto map=ArrayEngine::arrayStructure(in[0]);
    auto keys=ArrayEngine::arrayValues(in[1]);
    Structure r;
    for(const auto &vk:keys){
        const QString k=vk.toString();
        if(map.contains(k))
            r.insert(k,map[k]);
    }
    return r;
}

static QVariant arrayLSelect(const QList<QVariant>&in,Engine&)
{
    if(in.size()!=2)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 2 arguments: sourcelist,keylist");
    if(!ArrayEngine::isArray(in[0]) || !ArrayEngine::isArray(in[1]))
        return ELAM::Exception(Exception::TypeMismatchError,"Arguments must be arrays.");
    auto map=ArrayEngine::arrayStructure(in[0]);
    auto keys=ArrayEngine::arrayValues(in[1]);
    QVariantList r;
    for(const auto &vk:keys){
        const QString k=vk.toString();
        if(map.contains(k))
            r.append(map[k]);
    }
    return r;
}

static QVariant arrayMMerge(const QList<QVariant>&in,Engine&)
{
    Structure r;
    int i=0;
    for(const auto &val:in){
        i++;
        if(!ArrayEngine::isArray(val))
            return ELAM::Exception(Exception::TypeMismatchError,QString("Argument %1 is not an array.").arg(i));
        auto map=ArrayEngine::arrayStructure(val);
        for(const auto&k:map.keys())
            if(!r.contains(k))
                r.insert(k,map[k]);
    }
    return r;
}

static QVariant arrayLMerge(const QList<QVariant>&in,Engine&)
{
    QVariantList r;
    int i=0;
    for(const auto &val:in){
        i++;
        if(!ArrayEngine::isArray(val))
            return ELAM::Exception(Exception::TypeMismatchError,QString("Argument %1 is not an array.").arg(i));
        for(const auto&v:ArrayEngine::arrayValues(val))
            r.append(v);
    }
    return r;
}

static QVariant arrayForeach(const QList<QVariant>&in,Engine&eng)
{
    if(in.size()!=3)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 3 arguments: sourcelist,variablenames,algorithmstring");
    if(!ArrayEngine::isArray(in[0]))
        return ELAM::Exception(Exception::TypeMismatchError,"First argument must be an array!");
    if(!in[1].canConvert<QString>() || !in[2].canConvert<QString>())
        return ELAM::Exception(Exception::TypeMismatchError,"variablenames and algorithm must be strings");
    //variable names
    QStringList vnames=in[1].toString().split(',',Qt::SkipEmptyParts);
    if(vnames.size()<1 || vnames.size()>2)
        return ELAM::Exception(Exception::ArgumentListError,"Variables names must name 1 or 2 variables");
    auto cc=eng.characterClasses();
    for(const auto&vn:vnames)if(!cc.isName(vn))return ELAM::Exception(Exception::TypeMismatchError,QString("Invalid variable name '%1'").arg(vn));
    QString vkey,vval;
    if(vnames.size()>1)vkey=vnames.takeFirst();
    vval=vnames.takeLast();
    //algo
    Expression algo=eng.expression(in[2].toString());
    //run
    QVariantList r;
    if(in[0].userType()==listMetaTypeId()){
        QVariantList lst=in[0].value<QVariantList>();
        for(qlonglong i=0;i<lst.size();i++){
            if(!vkey.isEmpty())eng.setVariable(vkey,i);
            eng.setVariable(vval,lst[i]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            r.append(vr);
        }
    }else{
        auto map=ArrayEngine::arrayStructure(in[0]);
        for(const auto&k:map.keys()){
            if(!vkey.isEmpty())eng.setVariable(vkey,k);
            eng.setVariable(vval,map[k]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            r.append(vr);
        }
    }
    return r;
}

static QVariant arrayLWhere(const QList<QVariant>&in,Engine&eng)
{
    if(in.size()!=3)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 3 arguments: sourcelist,variablenames,algorithmstring");
    if(!ArrayEngine::isArray(in[0]))
        return ELAM::Exception(Exception::TypeMismatchError,"First argument must be an array!");
    if(!in[1].canConvert<QString>() || !in[2].canConvert<QString>())
        return ELAM::Exception(Exception::TypeMismatchError,"variablenames and algorithm must be strings");
    //variable names
    QStringList vnames=in[1].toString().split(',',Qt::SkipEmptyParts);
    if(vnames.size()<1 || vnames.size()>2)
        return ELAM::Exception(Exception::ArgumentListError,"Variables names must name 1 or 2 variables");
    auto cc=eng.characterClasses();
    for(const auto&vn:vnames)if(!cc.isName(vn))return ELAM::Exception(Exception::TypeMismatchError,QString("Invalid variable name '%1'").arg(vn));
    QString vkey,vval;
    if(vnames.size()>1)vkey=vnames.takeFirst();
    vval=vnames.takeLast();
    //algo
    Expression algo=eng.expression(in[2].toString());
    //run
    QVariantList r;
    if(in[0].userType()==listMetaTypeId()){
        QVariantList lst=in[0].value<QVariantList>();
        for(qlonglong i=0;i<lst.size();i++){
            if(!vkey.isEmpty())eng.setVariable(vkey,i);
            eng.setVariable(vval,lst[i]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            if(!vr.canConvert<bool>())
                return ELAM::Exception(Exception::TypeMismatchError,"algorithm did not return boolean");
            if(vr.toBool())
                r.append(lst[i]);
        }
    }else{
        auto map=ArrayEngine::arrayStructure(in[0]);
        for(const auto&k:map.keys()){
            if(!vkey.isEmpty())eng.setVariable(vkey,k);
            eng.setVariable(vval,map[k]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            if(!vr.canConvert<bool>())
                return ELAM::Exception(Exception::TypeMismatchError,"algorithm did not return boolean");
            if(vr.toBool())
                r.append(map[k]);
        }
    }
    return r;
}

static QVariant arrayMWhere(const QList<QVariant>&in,Engine&eng)
{
    if(in.size()!=3)
        return ELAM::Exception(Exception::ArgumentListError,"Expecting 3 arguments: sourcelist,variablenames,algorithmstring");
    if(!ArrayEngine::isArray(in[0]))
        return ELAM::Exception(Exception::TypeMismatchError,"First argument must be an array!");
    if(!in[1].canConvert<QString>() || !in[2].canConvert<QString>())
        return ELAM::Exception(Exception::TypeMismatchError,"variablenames and algorithm must be strings");
    //variable names
    QStringList vnames=in[1].toString().split(',',Qt::SkipEmptyParts);
    if(vnames.size()<1 || vnames.size()>2)
        return ELAM::Exception(Exception::ArgumentListError,"Variables names must name 1 or 2 variables");
    auto cc=eng.characterClasses();
    for(const auto&vn:vnames)if(!cc.isName(vn))return ELAM::Exception(Exception::TypeMismatchError,QString("Invalid variable name '%1'").arg(vn));
    QString vkey,vval;
    if(vnames.size()>1)vkey=vnames.takeFirst();
    vval=vnames.takeLast();
    //algo
    Expression algo=eng.expression(in[2].toString());
    //run
    Structure r;
    if(in[0].userType()==listMetaTypeId()){
        QVariantList lst=in[0].value<QVariantList>();
        for(qlonglong i=0;i<lst.size();i++){
            if(!vkey.isEmpty())eng.setVariable(vkey,i);
            eng.setVariable(vval,lst[i]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            if(!vr.canConvert<bool>())
                return ELAM::Exception(Exception::TypeMismatchError,"algorithm did not return boolean");
            if(vr.toBool())
                r.insert(QString::number(i),lst[i]);
        }
    }else{
        auto map=ArrayEngine::arrayStructure(in[0]);
        for(const auto&k:map.keys()){
            if(!vkey.isEmpty())eng.setVariable(vkey,k);
            eng.setVariable(vval,map[k]);
            QVariant vr=eng.evaluate(algo);
            if(vr.userType()==ELAM::Exception::metaTypeId())
                return vr;
            if(!vr.canConvert<bool>())
                return ELAM::Exception(Exception::TypeMismatchError,"algorithm did not return boolean");
            if(vr.toBool())
                r.insert(k,map[k]);
        }
    }
    return r;
}

void ArrayEngine::configureArrayEngine(ELAM::Engine&en)
{
    //simple functions
    en.setFunction("count",&::countArray);
    en.setFunction("isArray",&::isArray);
    en.setFunction("isNumArray",&isNumArray);
    en.setFunction("isStructure",&isStructure);
    en.setFunction("isMapStructure",&isMap);
    en.setFunction("isGadget",&isGadget);
    en.setFunction("isQObject",&isQObject);
    //key/value access
    en.setFunction("arrayKeys",&::arrayKeys);
    en.setFunction("arrayValues",&::arrayValues);
    en.setFunction("hasKey",&::arrayHasKey);
    //construction
    en.setFunction("array",&::arrayCat);
    en.setFunction("lseq",&::arraySeq);
    ///conversion, algorithms
    en.setFunction("lreverse",&::arrayReverse);
    en.setFunction("lsorts",&::arraySortStr);
    en.setFunction("lsorti",&::arraySortInt);
    en.setFunction("lrange",&::arrayRange);
    en.setFunction("mselect",&::arrayMSelect);
    en.setFunction("lselect",&::arrayLSelect);
    en.setFunction("lmerge",&::arrayLMerge);
    en.setFunction("mmerge",&::arrayMMerge);
    en.setFunction("lforeach",&arrayForeach);
    en.setFunction("lwhere",&arrayLWhere);
    en.setFunction("mwhere",&arrayMWhere);

}

bool ArrayEngine::iterate(const QVariant&data,PairCallback callback)
{
    //numbered array or simple structure
    const int utype=data.userType();

    if(utype==listMetaTypeId()){
        auto val=data.value<QList<QVariant>>();
        const auto sz=val.size();
        for(int i=0;i<sz;i++)
            if(!callback(i,val[i]))return false;
        return true;
    }
    if(utype==structureMetaTypeId()){
        auto map=data.value<QMap<QString,QVariant>>();
        for(const auto&k:map.keys())
            if(!callback(k,map[k]))return false;
        return true;
    }
    //gadget
    QMetaType mt(utype);
    if(mt.flags()&QMetaType::IsGadget){
        const QMetaObject*mo=mt.metaObject();
        const void*ptr=data.data();
        for(int i=0;i<mo->propertyCount();i++)
            if(!callback(mo->property(i).name(),mo->property(i).readOnGadget(ptr)))return false;
        return true;
    }
    //QObject*
    if(mt.flags()&QMetaType::PointerToQObject || data.canConvert<QObject*>()){
        QObject*o=data.value<QObject*>();
        if(!o){
            return false;
        }
        const auto*mo=o->metaObject();
        for(int i=0;i<mo->propertyCount();i++)
            if(!callback(mo->property(i).name(),mo->property(i).read(o)))return false;
        for(const auto&k:o->dynamicPropertyNames())
            if(!callback(QString::fromLatin1(k),o->property(k)))return false;
        return true;
    }
    //must be a non-array type
    return false;
}
