//  engine definition implementation
//
// (c) Konrad Rosenbaum, 2010
// protected under the GNU LGPL v3 or at your option any newer

#include "elamengine.h"
#include "elamvalue.h"

#include "metatypeid.h"

#include <QDebug>
#include <DPtr>
#include <QStringList>
#include <QMetaObject>
#include <QMetaType>
#include <QMetaProperty>
#include <QRegularExpression>

#include <algorithm>

#define xDebug() QNoDebug()
// #define xDebug() qDebug()

namespace ELAM {

ELAM_EXPORT QString versionInfo(){return "0.4 alpha";}
///////////////////////////////////////////////////////////////////////////////
// Engine

class DPTR_CLASS_NAME(Engine):public DPtr
{
	public:
		CharacterClassSettings cclass;
		QMap<QString,QVariant> vars,consts;
		QMap<QString,Function> funcs;
		struct LiteralParser_s
		{
			LiteralParser parser;
			QString start;
			int prio,parserid;
			//this operator is reversed, so that highest prio is in element 0 after qSort
			bool operator<(const LiteralParser_s&l2)const
			{return prio>l2.prio;}
		};
		QList<LiteralParser_s>parsers;
		int nextparserid;
		QMap<QString,UnaryOperator>unary;
		struct BinaryOperator_s {
			BinaryOperator_s(int p=1){prio=p;}
			BinaryOperator_s(BinaryOperator o,int p){op=o;prio=p;}
			BinaryOperator op;
			int prio;
		};
		QMap<QString,BinaryOperator_s>binary;
		struct CastFunc_s {
			CastFunc_s(int p=50){prio=p;target=-1;cfunc=0;}
			int target,prio;
			QList<int>origin;
			TypeCast cfunc;
		};
		QList<CastFunc_s>casts;
		QList<int>primtypes;
		QString pipe;
		int pipeprio=0;
		bool allownamedoperators=false;
		Private():nextparserid(1){}
};
DEFINE_DPTR(Engine);

Engine::Engine(QObject* parent): QObject(parent)
{
	registerType(Exception::metaTypeId());
    registerType(structureMetaTypeId());
    registerType(listMetaTypeId());
}

QVariant Engine::evaluate(QString ex)
{
	return evaluate(expression(ex));
}

QVariant Engine::evaluate(Expression ex)
{
	return ex.evaluate();
}

Expression Engine::expression(QString s)
{
	return Expression(this,tokenize(s));
}

Expression Engine::expression(QList< Token > tok)
{
	return Expression(this,tok);
}

QList< Token > Engine::tokenize(QString ex)
{
	//presets
	Token::Type ctype=Token::Invalid;
	QString ctok;
	QList<Token> ret;
	int cline=1,ccol=0;
	int sline=cline,scol=ccol;
	int litend=-1;
	//go through expression
	for(int i=0;i<ex.size();i++){
		if(ex[i]=='\n'){cline++;ccol=0;}
		ccol++;
		//skip everything till end of a literal
		if(litend>i)continue;
		//get type of current character
		Token::Type ntype=d->cclass.charType(ex[i],ctype);
		//check for invalid stuff
		if(ntype==Token::Invalid)
			return QList<Token>()<<Token(Position(cline,ccol));
		//check whether we are still inside the same token
		if(ntype==ctype)
			ctok+=ex[i];
		else{
			//store old token
			if(ctype!=Token::Invalid && ctype!=Token::Whitespace && ctok!="")
				ret<<Token(ctok,ctype,Position(sline,scol));
			//reset state
			ctok.clear();
			sline=cline;
			scol=ccol;
			//handle current char
			switch(ntype){
				case Token::ParOpen:
				case Token::ParClose:
                case Token::ArrOpen:
                case Token::ArrClose:
				case Token::Comma:
                case Token::DotOp:
					//special chars
					ret<<Token(QString(ex[i]),ntype,Position(cline,ccol));
					ctype=Token::Invalid;
					break;
				case Token::Whitespace:
				case Token::Operator:
				case Token::Name:
					//start new token
					ctype=ntype;
					ctok+=ex[i];
					break;
				case Token::Literal:{
					//parse it
					QPair<QString,QVariant>lt=parseLiteral(ex,i);
					//check for failure
					if(lt.first=="")
						return QList<Token>()<<Token(Position(cline,ccol));
					//store token
					ret<<Token(lt.first,lt.second,Position(cline,ccol));
					//make the loop skip the rest
					litend=i+lt.first.size();
					ctype=Token::Invalid;
					break;}
				default://nothing
					qDebug()<<"oops. unexpected token type at line"<<cline<<"col"<<ccol<<"in expression"<<ex;
					ctype=Token::Invalid;
					break;
			}
		}
	}
	//add remaining stuff
	if(ctype!=Token::Invalid && ctype!=Token::Whitespace && ctok!="")
		ret<<Token(ctok,ctype,Position(sline,scol));
	return ret;
}

CharacterClassSettings Engine::characterClasses()
{
	return d->cclass;
}

//Note: for backwards compatibility this helper is able to parse dots - normally this is done in the expression evaluation
//Note 2: if you fix bugs or add capabilities also fix elamexpression.cpp, Expression::evalDot()/::evalArray(); and of course ArrayEngine
static inline QPair<bool,QVariant>traverseValue(QMap< QString, QVariant > list, QString name, QChar dot)
{
// 	qDebug()<<"varx?"<<name;
	//simple:
	if(list.contains(name))return QPair<bool,QVariant>(true,list[name]);
	//parse for dots
	if(dot.isNull()){
		xDebug()<<"Variable/Constant"<<name<<"does not exist and there is no way to split it!";
		return QPair<bool,QVariant>(false,QVariant());
	}
	QStringList nlist=name.split(dot);
// 	qDebug()<<"looking for var"<<nlist;
	//split and explore
	QString n1=nlist.takeFirst();
	if(!list.contains(n1)){
		xDebug()<<"Root"<<n1<<"of variable/constant"<<name<<"not found.";
		return QPair<bool,QVariant>(false,QVariant());
	}
	QVariant cur=list[n1];
	while(nlist.size()>0){
// 		qDebug()<<"   c"<<nlist[0]<<cur;
		QByteArray n=nlist.takeFirst().toLatin1();
		n1+=dot+QString::fromLatin1(n);
		//is it valid at all?
		if(!cur.isValid()){
			qDebug()<<"Component"<<n1<<"of variable/constant"<<name<<"is not valid.";
			return QPair<bool,QVariant>(false,QVariant());
		}
		//try to flatten out pointer like types
		QString tn=cur.typeName();
		static QRegularExpression ptrreg("^[a-zA-Z0-9_]+<([a-zA-Z0-9_]+)>$");
        auto ptrmatch=ptrreg.match(tn);
		if(ptrmatch.hasMatch()){
			xDebug()<<"  flattening"<<n1<<"from"<<tn<<"to"<<ptrmatch.captured(1);
			if( !canConvert(cur,ptrmatch.captured(1)) ){
				qDebug()<<"Failed to convert component"<<n1<<"of variable/constant"<<name<<"from type"<<tn<<"to"<<ptrmatch.captured(1);
				return QPair<bool,QVariant>(false,QVariant());
			}
		}
		//check known types...
		QMetaType mt(cur.userType());
		//it it a variant map?
		if(mt.id()==structureMetaTypeId()){
            const auto&value=cur.value<Structure>();
            if(value.contains(n)){
                cur=value[n];
                continue;
            }
            //not found...
            qDebug()<<"Structure"<<n1<<"does not contain a component"<<n<<"in variable/constant"<<name;
            return  QPair<bool,QVariant>(false,QVariant());
        }
		//is it a gadget?
		if(mt.flags()&QMetaType::IsGadget){
			const QMetaObject*mo=mt.metaObject();
			int idx=mo->indexOfProperty(n.constData());
			if(idx<0){
				qDebug()<<"Gadget"<<n1<<"of variable/constant"<<name<<"has no property"<<n;
				return QPair<bool,QVariant>(false,QVariant());
			}
			QMetaProperty mp=mo->property(idx);
			void*ptr=cur.data();
			cur=mp.readOnGadget(ptr);
			continue;
		}
		//check whether it is pointer to QObject
		if(mt.flags()&QMetaType::PointerToQObject || cur.canConvert<QObject*>()){
			QObject*o=cur.value<QObject*>();
			if(!o){
				qDebug()<<"Unable to resolve QObject* of component"<<n1<<"of variable/constant"<<name;
				return QPair<bool,QVariant>(false,QVariant());
			}
			if(!o->dynamicPropertyNames().contains(n)&&o->metaObject()->indexOfProperty(n.data())<0){
				qDebug()<<"QObject"<<n1<<"of variable/constant"<<name<<"has no property"<<n;
				return QPair<bool,QVariant>(false,QVariant());
			}
			cur=o->property(n.constData());
			continue;
		}
		//not a known structured type
		qDebug()<<"Unknown structure type"<<tn<<"in component"<<n1<<"of variable/constant"<<name;
		return QPair<bool,QVariant>(false,QVariant());
	}
// 	qDebug()<<"   found"<<n1<<"as"<<name<<":"<<cur;
	return QPair<bool,QVariant>(cur.isValid(),cur);
}

bool Engine::valueExists ( QMap< QString, QVariant > list, QString name )const
{
	return traverseValue(list,name,d->cclass.dotChar()).first;
}

QVariant Engine::findValue ( QMap< QString, QVariant > list, QString name )const
{
	return traverseValue(list,name,d->cclass.dotChar()).second;
}

QVariant Engine::getConstant(QString n) const
{
	return findValue(d->consts,n);
}

QVariant Engine::getVariable(QString n) const
{
	return findValue(d->vars,n);
}

QVariant Engine::getValue(QString n) const
{
	if(hasConstant(n))return getConstant(n);
	if(hasVariable(n))return getVariable(n);
	return QVariant();
}

bool Engine::hasConstant(QString n) const
{
	return valueExists(d->consts,n);
}

bool Engine::hasVariable(QString n) const
{
	return valueExists(d->vars,n);
}

bool Engine::hasValue(QString n) const
{
	return hasVariable(n) || hasConstant(n);
}

void Engine::removeConstant(QString n)
{
	d->consts.remove(n);
}

void Engine::removeVariable(QString n)
{
	d->vars.remove(n);
}

void Engine::removeValue(QString n)
{
	removeConstant(n);
	removeVariable(n);
}

bool Engine::setConstant(QString n, QVariant v)
{
	if(!d->cclass.isName(n))return false;
	if(d->funcs.contains(n))return false;
	removeVariable(n);
	d->consts.insert(n,autoCast(v));
	return true;
}

bool Engine::setVariable(QString n, QVariant v)
{
	if(!d->cclass.isName(n))return false;
	if(hasConstant(n))return false;
	if(d->funcs.contains(n))return false;
	d->vars.insert(n,autoCast(v));
	return true;
}

bool Engine::hasFunction(QString n) const
{
	return d->funcs.contains(n);
}

Function Engine::getFunction(QString n) const
{
	if(d->funcs.contains(n))return d->funcs[n];
	return 0;
}

bool Engine::setFunction(QString n, ELAM::Function p)
{
	if(p==0)return false;
	if(!d->cclass.isName(n))return false;
	if(hasConstant(n))return false;
	if(hasVariable(n))return false;
	d->funcs.insert(n,p);
	return true;
}

void Engine::removeFunction(QString n)
{
	d->funcs.remove(n);
}

int Engine::setLiteralParser ( ELAM::LiteralParser parser, QString startchars, int prio )
{
	//base check
	if(parser==nullptr || startchars.isEmpty() || prio<0 || prio>100)
		return 0;
	//search for existing entry
/*	for(int i=0;i<d->parsers.size();i++){
		if(d->parsers[i].parser==parser){
			d->parsers[i].start=startchars;
			d->parsers[i].prio=prio;
			return true;
		}
	}*/
	//none found: create new entry
	Private::LiteralParser_s s;
	s.parser=parser;
	s.prio=prio;
	s.start=startchars;
	s.parserid=d->nextparserid++;
	d->parsers<<s;
	return s.parserid;
}

void Engine::removeLiteralParser ( int parser )
{
	for(int i=0;i<d->parsers.size();i++)
		if(d->parsers[i].parserid==parser){
			d->parsers.removeAt(i);
			return;
		}
}

QPair< QString, QVariant > Engine::parseLiteral ( QString ex, int start)
{
	QChar sc=ex[start];
	//find any parser that matches the start char
	QList<Private::LiteralParser_s>cand;
	for(int i=0;i<d->parsers.size();i++)
		if(d->parsers[i].start.contains(sc))
			cand<<d->parsers[i];
	if(cand.size()<1)
		return QPair<QString,QVariant>();
	//sort them (highest prio first)
	std::sort(cand.begin(),cand.end());
	//execute
	for(int i=0;i<cand.size();i++){
		QPair< QString, QVariant >r=cand[i].parser(ex,*this,start);
		if(r.first.size()>0)
			return r;
	}
	//failure
	return QPair< QString, QVariant >();
}

BinaryOperator Engine::binaryOperator ( QString name,int prio,PriorityMatch match )
{
	//correct prio
	if(prio<1)prio=1;
	if(prio>99)prio=99;
	//if new: declare success
	if(!d->binary.contains(name)){
		d->binary.insert(name,Private::BinaryOperator_s(prio));
		return d->binary[name].op;
	}
	//check whether prio matches
	if(d->binary[name].prio==prio)
		return d->binary[name].op;
	//override if the op is empty
	if(d->binary[name].op.isNull())
		match=OverridePrio;
	//check matching mode
	switch(match){
		case OverridePrio:
			//change prio
			d->binary[name].prio=prio;
			//fall through
		case IgnoreMismatch:
			//return original op
			return d->binary[name].op;
			break;
		default://FailIfMismatch
			//return fake
			return BinaryOperator();
			break;
	}
}

int Engine::binaryOperatorPrio(QString name)
{
	if(d->binary.contains(name))
		return d->binary[name].prio;
	return -1;
}

void Engine::setBinaryOperatorPrio(QString name, int prio)
{
	binaryOperator(name,prio,OverridePrio);
}


UnaryOperator Engine::unaryOperator ( QString name )
{
	if(!d->unary.contains(name))
		d->unary.insert(name,UnaryOperator());
	return d->unary[name];
}

bool Engine::isAssignment(QString name) const
{
	//is it composed of operator chars?
	if(!d->cclass.isOperator(name))return false;
	//is there an overriding operator?
	if(d->binary.contains(name))return false;
	//return cclasses view of things
	return d->cclass.isAssignment(name);
}

bool Engine::isPipe ( QString name ) const
{
	if(name.isEmpty())return false;
	//is it composed of operator chars?
	if(!d->cclass.isOperator(name))return false;
	//is it pipe?
	return name==d->pipe;
}

bool Engine::hasBinaryOperator(QString name) const
{
	if(!d->binary.contains(name))return false;
	return ! d->binary[name].op.isNull();
}

bool Engine::hasUnaryOperator(QString name) const
{
	if(!d->unary.contains(name))return false;
	return ! d->unary[name].isNull();
}

void Engine::registerType(int typeId)
{
	if(!d->primtypes.contains(typeId))
		d->primtypes.append(typeId);
}

void Engine::setAutoCast(int target, QList< int > origin, TypeCast castfunc, int prio)
{
	if(castfunc==0)return;
	if(prio<0)return;
	registerType(target);
	Private::CastFunc_s cf(prio);
	cf.cfunc=castfunc;
	cf.origin=origin;
	cf.target=target;
	d->casts.append(cf);
}

QVariant Engine::autoCast(const QVariant& v)const
{
	//check for primary
	int vid=v.userType();
	if(d->primtypes.contains(vid))
		return v;
	//find matching cast
	int prio=-1;int pos=-1;
	for(int i=0;i<d->casts.size();i++){
		if(!d->casts[i].origin.contains(vid))continue;
		if(prio<d->casts[i].prio){
			pos=i;
			prio=d->casts[i].prio;
		}
	}
	//found it?
	if(pos<0)return v;
	else return d->casts[pos].cfunc(v,*this);
}

QStringList Engine::binaryOperatorNames() const
{
	return d->binary.keys();
}

QStringList Engine::constantNames() const
{
	return d->consts.keys();
}

QStringList Engine::functionNames() const
{
	return d->funcs.keys();
}

QStringList Engine::unaryOperatorNames() const
{
	return d->unary.keys();
}

QStringList Engine::variableNames() const
{
	return d->vars.keys();
}

QString Engine::pipeOperator() const
{
	return d->pipe;
}

void Engine::setPipeOperator ( QString pipe )
{
	d->pipe=pipe;
}

int Engine::pipePrio() const
{
	return d->pipeprio;
}

void Engine::setPipePrio ( int p)
{
	d->pipeprio=p;
}

bool Engine::namedOperatorsAllowed() const
{
	return d->allownamedoperators;
}

void Engine::setNamedOperatorsAllowed ( bool b )
{
	d->allownamedoperators=b;
}

static QVariant reflectTypeId(const QList<QVariant>&args,Engine&)
{
	if(args.size()!=1)
		return QVariant::fromValue(Exception(Exception::ArgumentListError,"expecting one argument"));
	return (int)args[0].userType();
}

static QVariant reflectTypeName(const QList<QVariant>&args,Engine&)
{
	if(args.size()!=1)
		return QVariant::fromValue(Exception(Exception::ArgumentListError,"expecting one argument"));
	return QString(args[0].typeName());
}

static QVariant reflectException(const QList<QVariant>&args,Engine&)
{
	QString ret;
	for(auto arg:args){
		if(ret.size())ret+="; ";
		if(arg.userType()==Exception::metaTypeId()){
			Exception e=arg.value<Exception>();
			ret+=QString("Exception(type=%1, position=%2, message='%3')")
				.arg(e.errorTypeName())
				.arg((QString)e.errorPos())
				.arg(e.errorText());
		}
	}
	return ret;
}

void Engine::configureReflection(Engine& e)
{
	e.setFunction("typeid",reflectTypeId);
	e.setFunction("typename",reflectTypeName);
	e.setFunction("showexception",reflectException);
}


//end of namespace
};
