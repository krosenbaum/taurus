//  engine definition implementation
//
// (c) Konrad Rosenbaum, 2010
// protected under the GNU LGPL v3 or at your option any newer

#include "elamengine.h"
#include "elamvalue.h"

#include "metatypeid.h"

#include <QDebug>
#include <SharedDPtr>

namespace ELAM {

/////////////////////////////////////////////////////////////////
// Unary Operator

class DPTR_CLASS_NAME(UnaryOperator):public SharedDPtr
{
    public:
        QMap<int,UnaryOperatorCall>callmap;
};
DEFINE_SHARED_DPTR(UnaryOperator)


UnaryOperator::UnaryOperator(const UnaryOperator& op)
    :d(op.d)
{
}

UnaryOperator& UnaryOperator::UnaryOperator::operator=(const ELAM::UnaryOperator& op)
{
    d=op.d;
    return *this;
}


UnaryOperator::UnaryOperator()
{
}

UnaryOperator::~UnaryOperator()
{
}


QVariant UnaryOperator::execute(const QVariant& op,Engine&eng) const
{
    if(d->callmap.size()==0)
        return Exception(Exception::UnknownOperatorError);
    //search for direct match
    if(d->callmap.contains(op.userType()))
        return d->callmap[op.userType()](op,eng);
    //search for fallback
    int any=AnyType::metaTypeId();
    if(d->callmap.contains(any))
        return d->callmap[any](op,eng);
    return Exception(Exception::TypeMismatchError);
}

QList< int > UnaryOperator::getTypeIds() const
{
    return d->callmap.keys();
}

QStringList UnaryOperator::getTypeNames() const
{
    QStringList ret;
    QList<int>ti=d->callmap.keys();
    for(int i=0;i<ti.size();i++)
        ret<<typeIdToName(ti[i]);
    return ret;
}

UnaryOperatorCall UnaryOperator::getCallback(QString type) const
{
    return getCallback(typeNameToId(type));
}

UnaryOperatorCall UnaryOperator::getCallback(int type) const
{
    if(d->callmap.contains(type))
        return d->callmap[type];
    if(d->callmap.contains(AnyType::metaTypeId()))
        return d->callmap[AnyType::metaTypeId()];
    return 0;
}

void UnaryOperator::setCallback(UnaryOperatorCall callback, int type)
{
    if(type==QMetaType::UnknownType)return;
    if(!(bool)callback){
        d->callmap.remove(type);
        return;
    }
    d->callmap.insert(type,callback);
}

void UnaryOperator::setCallback(UnaryOperatorCall callback, QString type)
{
    setCallback(callback,typeNameToId(type));
}

void UnaryOperator::removeCallback(QString t)
{
    removeCallback(typeNameToId(t));
}

void UnaryOperator::removeCallback(int t)
{
    d->callmap.remove(t);
}

bool UnaryOperator::isNull() const
{
    return d->callmap.isEmpty();
}


//end of namespace
};
