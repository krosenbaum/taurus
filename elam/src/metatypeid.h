// Wrapper functions to convert meta type IDs while Qt5 is still supported

#pragma once

#include <QMetaType>

static inline QString typeIdToName(int id)
{
    return QMetaType(id).name();
}

static inline int typeNameToId(QString n)
{
#if QT_VERSION > QT_VERSION_CHECK(6,0,0)
    return QMetaType::fromName(n.toLatin1()).id();
#else
    return QMetaType::type(n.toLatin1());
#endif
}

static inline bool canConvert(const QVariant&var,QString n)
{
#if QT_VERSION > QT_VERSION_CHECK(6,0,0)
    return var.canConvert(QMetaType::fromName(n.toLatin1()));
#else
    return var.canConvert(QMetaType::type(n.toLatin1()));
#endif
}
