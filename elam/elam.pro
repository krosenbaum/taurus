TEMPLATE = lib
TARGET = elam
CONFIG += dll create_prl hide_symbols separate_debug_info c++17
QT -= gui
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp
DEFINES += ELAM_LIBRARY_BUILD
VERSION =
DESTDIR = $$OUT_PWD/../lib

HEADERS += \
	include/elam.h \
	include/elamunary.h \
	include/elambinary.h \
	include/elamcharclass.h \
	include/elamengine.h \
	include/elamexpression.h \
	include/elamvalue.h \
	include/elamintengine.h \
	include/elamfloatengine.h \
	include/elamboolengine.h \
	include/elamstringengine.h \
	include/elamarrayengine.h

SOURCES += \
	src/elamvalue.cpp \
	src/elamunary.cpp \
	src/elambinary.cpp \
	src/elamcharclass.cpp \
	src/elamengine.cpp \
	src/elamintengine.cpp \
	src/elamexpression.cpp \
	src/elamfloatengine.cpp \
	src/elamboolengine.cpp \
	src/elamstringengine.cpp \
	src/elamarrayengine.cpp

INCLUDEPATH += . src include ../include/elam ../include/chester
DEPENDPATH += $$INCLUDEPATH
