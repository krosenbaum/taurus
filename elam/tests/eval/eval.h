#include <QObject>

#pragma once

class ElamTest:public QObject
{
    Q_OBJECT
    private slots:
        void emptytest();
        void evaltest();
        void excepttest();
        void counttest();
        void functest();
        void pipetest();
        void dottest();
        void dotgadgettest();
        void dotfunctest();
        void dotstructtest();
        void namedOperators();
        void numberedArray();
        void assocArray();
        void recursiveArray();
        void priority();
        void complexStuff();
};

class DotGadget
{
    Q_GADGET
public:
    DotGadget()=default;
    DotGadget(const DotGadget&)=default;

    DotGadget& operator=(const DotGadget&)=default;

    Q_PROPERTY(QString tprop READ tprop CONSTANT);

    QString tprop(){return "testprop";}
};

Q_DECLARE_METATYPE(DotGadget);
