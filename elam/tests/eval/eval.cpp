#include "elam.h"

#include "eval.h"

#include <QtCore>
#include <QtTest>
#include <QDebug>

using namespace ELAM;

static int gadgetTypeId=qRegisterMetaType<DotGadget>();


void ElamTest::emptytest()
{
	QVariant v=Engine().evaluate("");
	QCOMPARE(v.userType(),Exception::metaTypeId());
}

void ElamTest::evaltest()
{
	IntEngine ie;
	FloatEngine::configureFloatEngine(ie);
	ie.setVariable("b",(qlonglong)0);
	QString exs="a=b+=345*int(3.15)+ - -(+65/(5))";
	Expression ex=ie.expression(exs);
// 	qDebug()<<"expression:\n"<<ex;
	QVariant v=ie.evaluate(ex);
	QVariant xv=Exception();
// 	if(v.userType()==Exception::metaTypeId())qDebug()<<"exception:"<<Exception(v);
	QVariant cmp=(qlonglong)(345*3+65/5);
	QCOMPARE(v,cmp);
	QCOMPARE(ie.getVariable("b"),cmp);
	QCOMPARE(ie.getVariable("a"),cmp);
	QCOMPARE(ie.evaluate("-2"),QVariant((qlonglong)-2));
	ie.setVariable("v",(qlonglong)2);
	ie.setConstant("c",(qlonglong)-3);
	QCOMPARE(ie.evaluate("-v"),QVariant((qlonglong)-2));
	QCOMPARE(ie.evaluate("-c"),QVariant((qlonglong)3));
}

void ElamTest::dottest()
{
	StringEngine ie;
	ie.characterClasses().setDotChar('.');
	QObject obj;
	obj.setObjectName("hello");
	obj.setProperty("dynamic","juhoo");
	ie.setVariable("b",QVariant::fromValue(&obj));
	QVERIFY(ie.hasVariable("b.objectName"));
	QCOMPARE(ie.getVariable("b.objectName"),QVariant("hello"));
	QVERIFY(ie.hasVariable("b.dynamic"));
	QCOMPARE(ie.getVariable("b.dynamic"),QVariant("juhoo"));
	QString exs="a=b.objectName+' world '+b.dynamic";
	Expression ex=ie.expression(exs);
// 	qDebug()<<"expression:\n"<<ex;
	QVariant v=ie.evaluate(ex);
	QCOMPARE(v,QVariant("hello world juhoo"));
	QCOMPARE(ie.getVariable("a"),QVariant("hello world juhoo"));
}

void ElamTest::dotfunctest()
{
    StringEngine ie;
    //register function returning structure
    ie.setFunction("f",[](const QList<QVariant>&,Engine&)->QVariant{return QVariant::fromValue(DotGadget());});
    //register operator returning structure
    ie.binaryOperator("+").setCallback([](const QVariant&r,const QVariant&,Engine&)->QVariant{return r;},gadgetTypeId,gadgetTypeId);
    //test assignment from function
    QVERIFY(ie.evaluate("df=f()").canConvert<Exception>()==false);
    QCOMPARE(ie.getVariable("df.tprop"),QVariant("testprop"));
    //test assignment from operator
    QVERIFY(ie.evaluate("do=df+f()").canConvert<Exception>()==false);
    QCOMPARE(ie.getVariable("do.tprop"),QVariant("testprop"));
    //test chaining function and dot
    QCOMPARE(ie.evaluate("f().tprop"),QVariant("testprop"));
    //test chaining operator and dot
    QCOMPARE(ie.evaluate("(df+do).tprop"),QVariant("testprop"));
}

void ElamTest::dotstructtest()
{
    StringEngine ie;
    Structure st;
    st.insert("first",1);
    st.insert("second","2");
    //check
    ie.setVariable("s",st);
    QVERIFY(ie.evaluate("d=s").canConvert<Exception>()==false);
    QCOMPARE(ie.getVariable("s"),st);
    QCOMPARE(ie.getVariable("s.first"),QVariant(1));
    QCOMPARE(ie.getVariable("s.second"),QVariant("2"));
}

void ElamTest::dotgadgettest()
{
    StringEngine ie;
    DotGadget dg;
    ie.setVariable("d",QVariant::fromValue(dg));
    QVERIFY(ie.hasVariable("d.tprop"));
    QCOMPARE(ie.getVariable("d.tprop"),QVariant("testprop"));
}

static QVariant mycount(const QList<QVariant>&args,Engine&)
{
	return (qlonglong)args.size();
}
static QVariant mysum(const QList<QVariant>&args,Engine&)
{
	qlonglong r=0;
	for(int i=0;i<args.size();i++)
		r+=args[i].toLongLong();
	return r;
}

void ElamTest::excepttest()
{
	IntEngine ie;
	QVariant v=ie.evaluate("-novar");
	QCOMPARE(v.userType(),Exception::metaTypeId());
	Exception ex=v;
// 	qDebug()<<ex;
	QCOMPARE(ex.errorType(),Exception::UnknownValueError);
	ex=ie.evaluate("9/0");
	QCOMPARE(ex.errorType(),Exception::OperationError);
	ex=ie.evaluate("(novar)");
	QCOMPARE(ex.errorType(),Exception::UnknownValueError);
}

void ElamTest::counttest()
{
	IntEngine ie;
	ie.setFunction("count",mycount);
	QCOMPARE(ie.evaluate("count(1,2,3,count(4,5,6))"),QVariant((qlonglong)4));
}

void ElamTest::functest()
{
	IntEngine ie;
	ie.setFunction("count",mycount);
	ie.setFunction("sum",mysum);
	QCOMPARE(ie.evaluate("sum(1,2,3,count(1,x,n))"),QVariant((qlonglong)9));
}

void ElamTest::namedOperators()
{
	IntEngine ie;
	Exception ex=ie.evaluate("1 or 2");
	QCOMPARE(ex.errorType(),Exception::ParserError);
	ex=ie.evaluate("not 1");
	QCOMPARE(ex.errorType(),Exception::ParserError);
	ie.setNamedOperatorsAllowed(true);
	QCOMPARE(ie.evaluate("1 or 2"),QVariant(3));
	QCOMPARE(ie.evaluate("~ 1"),QVariant(-2));
	QCOMPARE(ie.evaluate("2 + ~ 1"),QVariant(0));
	QCOMPARE(ie.evaluate("not 1"),QVariant(-2));
	QCOMPARE(ie.evaluate("2 + not 1"),QVariant(0));
}

void ElamTest::numberedArray()
{
    List ml;
    ml.append("first");
    ml.append(2);
    ml.append(true);
    IntEngine ie;
    ie.setVariable("a",ml);
    //check values
    QCOMPARE(ie.evaluate("a"),ml);
    QCOMPARE(ie.evaluate("a[0]"),"first");
    QCOMPARE(ie.evaluate("a[1]"),2);
    QCOMPARE(ie.evaluate("a[2]"),true);
    QVERIFY(ie.evaluate("a[13]").canConvert<Exception>());//?
}

void ElamTest::assocArray()
{
    Structure s;
    s.insert("1","one");
    s.insert("two","shoe");
    IntEngine ie;
    StringEngine::configureStringEngine(ie);
    ie.setVariable("a",s);
    QCOMPARE(ie.evaluate("a"),s);
    QCOMPARE(ie.evaluate("a[1]"),"one");
    QCOMPARE(ie.evaluate("a['1']"),"one");
    QCOMPARE(ie.evaluate("a['two']"),"shoe");
    QCOMPARE(ie.evaluate("a.two"),"shoe");
}

void ElamTest::recursiveArray()
{
    Structure s,s2;
    List l;
    s.insert("1","one");
    s.insert("two","shoe");
    l.append(1);
    l.append(s);
    s2.insert("simple",1);
    s2.insert("list",l);
    s2.insert("st",s);
    IntEngine ie;
    StringEngine::configureStringEngine(ie);
    ie.setVariable("a",s2);
    QCOMPARE(ie.evaluate("a"),s2);
    QCOMPARE(ie.evaluate("a['simple']"),1);
    QCOMPARE(ie.evaluate("a['list']"),l);
    QCOMPARE(ie.evaluate("a['st']"),s);
    QCOMPARE(ie.evaluate("a.simple"),1);
    QCOMPARE(ie.evaluate("a.list"),l);
    QCOMPARE(ie.evaluate("a.st"),s);
    QCOMPARE(ie.evaluate("a.st.two"),"shoe");
    QCOMPARE(ie.evaluate("a.st['two']"),"shoe");
    QCOMPARE(ie.evaluate("a['st'].two"),"shoe");
    QCOMPARE(ie.evaluate("a['st']['two']"),"shoe");
    QCOMPARE(ie.evaluate("a.list[0]"),1);
    QCOMPARE(ie.evaluate("a['list'][0]"),1);
    QCOMPARE(ie.evaluate("a['list'][1]"),s);
    QCOMPARE(ie.evaluate("a.list[1]"),s);
    QCOMPARE(ie.evaluate("a['list'][1][1]"),"one");
    QCOMPARE(ie.evaluate("a['list'][1]['two']"),"shoe");
    QCOMPARE(ie.evaluate("a['list'][1].two"),"shoe");
}

void ElamTest::pipetest()
{
    IntEngine ie;
    ie.setFunction("f",[](const QList<QVariant>&o,Engine&)->QVariant
    {
        int r=0;
        bool b;
        for(const auto&v:o){
            r+=v.toInt(&b);
            if(!b)return Exception(Exception::ArgumentListError,"expecting int");
        }
        return r;
    });
    ie.setPipeOperator("$");
    QCOMPARE(ie.evaluate("f(1,2)"),3);
    QCOMPARE(ie.evaluate("f(1,2,3)"),6);
    QCOMPARE(ie.evaluate("1$f"),1);
    QCOMPARE(ie.evaluate("1$f(2)"),3);
    QCOMPARE(ie.evaluate("f(1,2)$f"),3);
    QCOMPARE(ie.evaluate("f(1,2)$f(4)"),7);
    QCOMPARE(ie.evaluate("f(1,2)$f(4)$f(5)"),12);
    QVERIFY(ie.evaluate("1$4").canConvert<Exception>());
}

void ElamTest::priority()
{
    IntEngine ie;
    ie.setFunction("func",[](const QList<QVariant>&p,Engine&e)->QVariant
        {
            Q_UNUSED(e);
            return p.size();
        });
    QCOMPARE(ie.evaluate("1+2*3"),7);
    QCOMPARE(ie.evaluate("2*3+1"),7);
    QCOMPARE(ie.evaluate("2*3+(1)"),7);
    QCOMPARE(ie.evaluate("(1+2)*3"),9);
    QCOMPARE(ie.evaluate("((1+2)*3)"),9);
    QCOMPARE(ie.evaluate("1+func(3,(4+4)*2,5,6)+2"),7);
}

void ElamTest::complexStuff()
{
    Structure s1,s2;
    s1.insert("abc",1);
    s2.insert("idx","def");
    s2.insert("1",2);
    s2.insert("def",s1);
    IntEngine ie;
    StringEngine::configureStringEngine(ie);
    ie.setVariable("x",s2);
    ie.setFunction("func",[](const QList<QVariant>&p,Engine&)->QVariant{return p.value(0).toInt()*2;});
    ie.setFunction("echo",[](const QList<QVariant>&p,Engine&)->QVariant{return p.value(0);});
    QCOMPARE(ie.evaluate("1+x.def.abc+2"),4);
    QCOMPARE(ie.evaluate("1+func(x.def.abc)+2"),5);
    QCOMPARE(ie.evaluate("1+func(x.def.abc+3)+2"),11);
    QCOMPARE(ie.evaluate("echo(x).def.abc"),1);
    QCOMPARE(ie.evaluate("echo(x.def).abc"),1);
    QCOMPARE(ie.evaluate("echo(x)['def'].abc"),1);
    QCOMPARE(ie.evaluate("2+echo(x).def.abc"),3);
    QCOMPARE(ie.evaluate("x[x.idx].abc"),1);
    QCOMPARE(ie.evaluate("x[(3-2)+0]"),2);
}


QTEST_MAIN(ElamTest)
