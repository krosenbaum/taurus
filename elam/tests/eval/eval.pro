TEMPLATE = app
TARGET = evaltest
QT -= gui
CONFIG += testlib debug link_prl c++11
QT += testlib
INCLUDEPATH += . ../../include ../../../include/chester
DEPENDPATH += $$INCLUDEPATH ../.. ../../src
LIBS += -L../../../lib -lelam

SOURCES += eval.cpp
HEADERS += eval.h
