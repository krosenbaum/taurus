//ELAM global definitions header
//
// (c) Konrad Rosenbaum, 2012
// protected under the GNU LGPL v3 or at your option any newer

#pragma once

//export for DLL
#include <QtCore/QtGlobal>

#if defined(ELAM_LIBRARY_BUILD)
#  define ELAM_EXPORT Q_DECL_EXPORT
#else
#  define ELAM_EXPORT Q_DECL_IMPORT
#endif
