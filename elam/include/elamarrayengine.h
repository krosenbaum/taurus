//ELAM array/structure engine header
//
// (c) Konrad Rosenbaum, 2021
// protected under the GNU LGPL v3 or at your option any newer

#pragma once

#include "elamengine.h"

namespace ELAM {

/**an engine that has some functions and operators for arrays and structures implemented*/
class ELAM_EXPORT ArrayEngine:public Engine
{
	public:
		///instantiates a pre-initialized engine
		ArrayEngine();

		///configures any engine to support float
		static void configureArrayEngine(Engine&);

        ///callback type for iteration:
        /// - first parameter: key for the iteration
        /// - second parameter: value for the iteration
        /// - return value: true=continue, false=stop iterating
        typedef std::function<bool(const QVariant&,const QVariant&)> PairCallback;

        ///iterate over an array
        ///\param array - the array variable to iterate over; if it is a non-array: no iteration
        ///\param callback - function that is called for every iteration see PairCallback
        ///\return true if the iteration was successful, false if one of the callbacks returned false
        static bool iterate(const QVariant&array,PairCallback callback);

        ///returns true if this is one of the recognized array types
        static bool isArray(const QVariant&);

        ///returns the number of elements in the array
        static int countArray(const QVariant&);

        ///return all keys of the array
        static QVariantList arrayKeys(const QVariant&);

        ///return all keys of the array
        static bool arrayHasKey(const QVariant&array,const QVariant&key);

        ///return all keys of the array
        static QVariantList arrayValues(const QVariant&);

        ///return the array in the form of a map/structure
        static Structure arrayStructure(const QVariant&);
};

//end of namespace
};

