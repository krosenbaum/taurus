TEMPLATE = lib
TARGET = domext
CONFIG += dll create_prl hide_symbols separate_debug_info c++11
QT -= gui
QT += xml
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp
DEFINES += DOMEXT_LIBRARY_BUILD
VERSION =
DESTDIR = $$OUT_PWD/../lib

HEADERS += \
	domiterator.h \
	domlist.h \
	domquery.h \
	domxquery.h

SOURCES += \
	domquery.cpp \
	domxquery.cpp

INCLUDEPATH += . ../include/domext
DEPENDPATH += $$INCLUDEPATH
