// Qt DOM Extensions
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2020
// protected under the GNU GPL v.3 or at your option any newer

#include "domxquery.h"

bool DomXQuery::setQuery(QString q,bool useNS)
{
    mquery.clear();
    merror.clear();
    museNS=useNS;
    //determine base point
    if(q.startsWith("//")){mStartPoint=StartPoint::AnyWhere;q=q.mid(2);}
    else if(q.startsWith("/")){mStartPoint=StartPoint::Document;q=q.mid(1);}
    else mStartPoint=StartPoint::Base;
    //TODO: parse, per item syntax: @{namespace}prefix:name(filter/expression=value)
    enum class PMode{Start,NS,Name,Filter};

    return false;
}

DomNodeList DomXQuery::execute(const QDomElement&)
{
    return DomNodeList();
}

DomNodeList DomXQuery::QueryElement::filter(const DomNodeList&in)
{
    //TODO
    return in;
}

DomNodeList DomXQuery::ComparableQueryElement::filter(const DomNodeList&in)
{
    DomNodeList out=QueryElement::filter(in);
    if(!hasCompare)return out;
    //TODO: comparison
    return out;
}

DomXQuery::QueryElement::~QueryElement()
{
    //no content, exists just for virtual
}
