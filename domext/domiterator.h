//
// C++ Implementation: DOM node iterator for range based for loops
//
//
// Author: Konrad Rosenbaum <konrad@silmor.de>, (C) 2013-2021
//
// Copyright: See README/COPYING.GPL files that come with this distribution
//
// originally introduced at http://silmor.de/qtstuff.domfor.php

#pragma once

#include <QDomNodeList>

///This class enables the use of range based for loops with Qt's QDomNodeList.
///
/// By simple including this header, range based for becomes possible:
/// \code
/// #include <DomNodeIterator>
///
/// QDomDocument doc;
/// //...
/// for(auto node:doc.elementsByTagName("MyTag"))
///   handleMyTag(node.toElement());
/// \endcode
///
/// Since this class is completely inline you can use it without linking the DOM extension library.
class DomNodeIterator
{
        int pos;
        const QDomNodeList *container;//must not be ref, for operator=(..) ; pointer is more efficient than a copy
public:
        //constructors and assignments
        inline DomNodeIterator(const QDomNodeList&l,int p):pos(p),container(&l){}
        inline DomNodeIterator(const DomNodeIterator&)=default;
        inline DomNodeIterator(DomNodeIterator&&)=default;
        inline DomNodeIterator& operator=(const DomNodeIterator&)=default;
        inline DomNodeIterator& operator=(DomNodeIterator&&)=default;

        //increment
        inline DomNodeIterator& operator++(){if(pos<container->size())pos++;return *this;}
        inline DomNodeIterator operator++(int){DomNodeIterator r(*this);if(pos<container->size())pos++;return r;}
        //decrement
        inline DomNodeIterator& operator--(){if(pos>0)pos--;return *this;}
        inline DomNodeIterator operator--(int){DomNodeIterator r(*this);if(pos>0)pos--;return r;}

        //comparison
        inline bool operator==(const DomNodeIterator&o)const{return pos==o.pos && container==o.container;}
        inline bool operator!=(const DomNodeIterator&o)const{return pos!=o.pos || container!=o.container;}

        //indirection
        inline QDomNode operator*(){return container->at(pos);}
};

//begin and end
/// returns an iterator to the beginning of a QDomNodeList
/// \related DomNodeIterator
inline DomNodeIterator begin(const QDomNodeList&l){return DomNodeIterator(l,0);}
/// returns an iterator behind the end of a QDomNodeList
/// \related DomNodeIterator
inline DomNodeIterator end(const QDomNodeList&l){return DomNodeIterator(l,l.size());}

