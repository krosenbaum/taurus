#include <QtTest>

#include <QtXml>

#include "../domiterator.h"
#include "../domlist.h"
#include "../domquery.h"

class Tests:public QObject
{
    Q_OBJECT
private slots:
    void initTestCase();
    void testIter();
    void testList();
    void testQueryPath();
    void testQueryRange();
    void testQueryDescend();
    void testQueryFilterBuiltin();
    void testQueryFilterPrefab();
    void testQueryFilterCustom();
    void testQuerySort();
public:
    Tests(){}
private:
    QDomDocument simple,noparseNS,parsedNS;
};

const QString simpleTxt="<myxml><A/><b/><c><d>text</d></c></myxml>";
const QString nsTxt=
"<myxml xmlns=\"http://hello\" xmlns:n=\"uri:world\" xmlns:o=\"uri:omm\">"
    "<A/>"
    "<n:b at1=\"one\" n:at2=\"2 two\" o:at3=\"3 three\"/>"
    "<c cat1='kitty' cat2='angus'>"
        "<n:d dat1=\"d1\">text</n:d>"
        "<e-f_g xmlns=\"uri:other\" eat1=\"yeah\"/>"
    "</c>"
"</myxml>";

void Tests::initTestCase()
{
    QVERIFY(simple.setContent(simpleTxt));
    QVERIFY(noparseNS.setContent(nsTxt,false));
    QVERIFY(parsedNS.setContent(nsTxt,true));
}

void Tests::testIter()
{
    int cnt=0;
    for(auto n:simple.documentElement().childNodes())
        if(n.isElement())cnt++;
    QCOMPARE(3,cnt);
}

void Tests::testList()
{
    //node list
    DomNodeList nl1=parsedNS.elementsByTagName("c");
    QCOMPARE(nl1.size(),1);
    DomNodeList nl2=nl1[0].childNodes();
    QCOMPARE(nl2.size(),2);
    nl2+=nl1[0].attributes();
    QVERIFY(nl1[0].hasAttributes());
    QCOMPARE(nl2.size(),4);
    //convert to element list
    DomElementList el=nl2;
    QCOMPARE(el.size(),2);
    QCOMPARE(nl2.toElementList().size(),2);
    //convert to attr list
    DomAttributeList al=nl2;
    QCOMPARE(al.size(),2);
    QCOMPARE(nl2.toAttributeList().size(),2);
    //append
    nl1.append(nl2);
    QCOMPARE(nl1.size(),5);
    nl1.append(el);
    QCOMPARE(nl1.size(),7);
    nl1.append(al);
    QCOMPARE(nl1.size(),9);
}

void Tests::testQueryPath()
{
    //init
    //path
}

void Tests::testQueryRange()
{
    //first
    //last
    //range
}

void Tests::testQueryDescend()
{
    //child
    //descend
}

void Tests::testQueryFilterBuiltin()
{
    //filter methods
}

void Tests::testQueryFilterPrefab()
{
    //prebuilt filter types
}

void Tests::testQueryFilterCustom()
{
    //custom filter
}

void Tests::testQuerySort()
{
    //sort
    //unique
}


QTEST_MAIN(Tests)

#include "moc_domtest.cpp"
