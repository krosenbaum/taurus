TEMPLATE = app
TARGET = domexttest
CONFIG += testlib separate_debug_info c++11
QT -= gui
QT += xml testlib
VERSION = 0.1.0

HEADERS += \
	domtest.cpp

SOURCES += \
	domtest.cpp

INCLUDEPATH += . ..
DEPENDPATH += $$INCLUDEPATH
