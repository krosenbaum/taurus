// Qt DOM Extensions
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2009-2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDomNode>
#include <QDomElement>
#include <QList>
#include <QtGlobal>
#include <QRegularExpression>

#include "domlist.h"
#include "domiterator.h"

#include <functional>
#include <initializer_list>
#include <vector>
#include <algorithm>

///This class helps navigate through XML documents similar to XPath/XQuery, but with C++ syntax based on DOM.
class DomQuery
{
public:
    DomQuery()=default;
    DomQuery(const DomQuery&)=default;
    DomQuery(DomQuery&&)=default;
    DomQuery(const QDomNode&e){mcurrent<<e;}
    DomQuery(const DomNodeList&l):mcurrent(l){}
    DomQuery(const DomElementList&l):mcurrent(l){}
    DomQuery(const DomAttributeList&l):mcurrent(l){}
    DomQuery(const QDomNodeList&l):mcurrent(l){}

    DomQuery& operator=(const DomQuery&)=default;
    DomQuery& operator=(DomQuery&&)=default;

    inline bool isValid()const{return mcurrent.size()>0;}

    DomQuery clone()const{return *this;}

    typedef std::function<bool(const QDomNode&)> FilterFunc;

    DomQuery& path(const QStringList&);
    DomQuery& path(QString p){return path(p.split('/',Qt::SkipEmptyParts));}

    ///returns all direct children of the current selection
    DomQuery& allChildren();
    ///returns all direct child elements of the current selection, if a name is given: filtered by name
    DomQuery& allChildElements(QString name=QString());
    ///returns all direct child attributes of the current selection, if a name is given: filtered by name
    DomQuery& allChildAttributes(QString name=QString());
    ///returns all descendant nodes of the current selection recusively (excluding the current selection)
    DomQuery& allDescendants();
    ///returns all descendant elements of the current selection recusively (excluding the current selection)
    DomQuery& allDescendantElements(QString name=QString());
    ///returns all descendant attributes of the current selection recusively (excluding the current selection)
    DomQuery& allDescendantAttributes(QString name=QString());

    ///returns the child nodes of the current selection filtered by the given function
    DomQuery& children(FilterFunc);
    ///returns the descendants of the current selection filtered by the given function
    DomQuery& descendants(FilterFunc);
    ///filters the current set with the given function
    DomQuery& filter(FilterFunc);

    typedef std::function<bool(const QDomNode&,const QDomNode&)> CompareFunc;
    DomQuery& sort(CompareFunc lessThan);

    DomQuery& unique();

    DomQuery& reverse();

    DomQuery& first(int num=1){mcurrent=mcurrent.mid(0,num);return *this;}
    DomQuery& last(int num=1){mcurrent=mcurrent.mid(mcurrent.size()-num);return *this;}
    DomQuery& range(int first,int num){mcurrent=mcurrent.mid(first,num);return *this;}

    DomQuery& append(const DomQuery&o){mcurrent.append(o.mcurrent);return *this;}
    DomQuery& append(const DomNodeList&o){mcurrent.append(o);return *this;}
    DomQuery& append(const DomElementList&o){mcurrent.append(o.toNodeList());return *this;}
    DomQuery& append(const DomAttributeList&o){mcurrent.append(o.toNodeList());return *this;}
    DomQuery& append(const QDomNodeList&o){mcurrent.append(o);return *this;}

    DomQuery& operator|=(const DomQuery&o){mcurrent.append(o.mcurrent);return *this;}
    DomQuery& operator|=(const DomNodeList&o){mcurrent.append(o);return *this;}
    DomQuery& operator|=(const DomElementList&o){mcurrent.append(o.toNodeList());return *this;}
    DomQuery& operator|=(const DomAttributeList&o){mcurrent.append(o.toNodeList());return *this;}
    DomQuery& operator|=(const QDomNodeList&o){mcurrent.append(o);return *this;}

    ///returns the current set of nodes
    DomNodeList result()const{return mcurrent;}
    QStringList resultStrings()const{QStringList r;for(QDomNode n:mcurrent)r.append(n.nodeValue());return r;}

    int resultSize()const{return mcurrent.size();}

private:
    DomNodeList mcurrent;
    static DomNodeList recurFilter(DomQuery::FilterFunc f,const QDomNodeList&l);
};

inline DomQuery operator|(const DomQuery&q1,const DomQuery&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomQuery&q1,const DomNodeList&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomQuery&q1,const DomElementList&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomQuery&q1,const DomAttributeList&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomQuery&q1,const QDomNodeList&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomNodeList&q1,const DomQuery&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomElementList&q1,const DomQuery&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const DomAttributeList&q1,const DomQuery&q2){DomQuery rq(q1);rq|=q2;return rq;}
inline DomQuery operator|(const QDomNodeList&q1,const DomQuery&q2){DomQuery rq(q1);rq|=q2;return rq;}

namespace DomFilter {

    class Or
    {
    public:
        Or(std::initializer_list<DomQuery::FilterFunc>func):mfuncs(func){}

        bool operator()(const QDomNode&n)
        {
            for(auto f:mfuncs)
                if(f(n))return true;
            return false;
        }

        Or& append(DomQuery::FilterFunc f)
        {
            mfuncs.push_back(f);
            return *this;
        }
        Or& prepend(DomQuery::FilterFunc f)
        {
            mfuncs.insert(mfuncs.begin(),f);
            return *this;
        }
    private:
        std::vector<DomQuery::FilterFunc>mfuncs;
    };

    class And
    {
    public:
        And(std::initializer_list<DomQuery::FilterFunc>func):mfuncs(func){}

        bool operator()(const QDomNode&n)
        {
            for(auto f:mfuncs)
                if(!f(n))return false;
            return true;
        }

        And& append(DomQuery::FilterFunc f)
        {
            mfuncs.push_back(f);
            return *this;
        }
        And& prepend(DomQuery::FilterFunc f)
        {
            mfuncs.insert(mfuncs.begin(),f);
            return *this;
        }
    private:
        std::vector<DomQuery::FilterFunc>mfuncs;
    };

    class Not
    {
    public:
        Not(DomQuery::FilterFunc f):mfunc(f){}

        bool operator()(const QDomNode&n){return !mfunc(n);}
    private:
        DomQuery::FilterFunc mfunc;
    };

    const DomQuery::FilterFunc IsElement=[](const QDomNode&n){return n.isElement();};
    const DomQuery::FilterFunc IsAttr=[](const QDomNode&n){return n.isAttr();};
    const DomQuery::FilterFunc AllNodes=[](const QDomNode&){return true;};

    const DomQuery::FilterFunc IsCDATASection=[](const QDomNode&n){return n.isCDATASection();};
    const DomQuery::FilterFunc IsCharacterData=[](const QDomNode&n){return n.isCharacterData();};
    const DomQuery::FilterFunc IsComment=[](const QDomNode&n){return n.isComment();};
    const DomQuery::FilterFunc IsDocument=[](const QDomNode&n){return n.isDocument();};
    const DomQuery::FilterFunc IsDocumentFragment=[](const QDomNode&n){return n.isDocumentFragment();};
    const DomQuery::FilterFunc IsDocumentType=[](const QDomNode&n){return n.isDocumentType();};
    const DomQuery::FilterFunc IsEntity=[](const QDomNode&n){return n.isEntity();};
    const DomQuery::FilterFunc IsEntityReference=[](const QDomNode&n){return n.isEntityReference();};
    const DomQuery::FilterFunc IsNotation=[](const QDomNode&n){return n.isNotation();};
    const DomQuery::FilterFunc IsNull=[](const QDomNode&n){return n.isNull();};
    const DomQuery::FilterFunc IsNotNull=[](const QDomNode&n){return !n.isNull();};
    const DomQuery::FilterFunc IsProcessingInstruction=[](const QDomNode&n){return n.isProcessingInstruction();};
    const DomQuery::FilterFunc IsText=[](const QDomNode&n){return n.isText();};


    class ByName
    {
        QString mname;
    public:
        ByName(QString name):mname(name){}
        bool operator()(const QDomNode&n){return n.nodeName()==mname;}
    };
    class ByPrefix
    {
        QString mpref;
    public:
        ByPrefix(QString p):mpref(p){}
        bool operator()(const QDomNode&n){return n.prefix()==mpref;}
    };
    class ByNsUri
    {
        QString muri;
    public:
        ByNsUri(QString ns):muri(ns){}
        bool operator()(const QDomNode&n){return n.namespaceURI()==muri;}
    };
    class ByValue
    {
        QString mval;
    public:
        ByValue(QString v):mval(v){}
        bool operator()(const QDomNode&n){return n.nodeValue()==mval;}
    };
    class ValueContains
    {
        QString mval;
    public:
        ValueContains(QString v):mval(v){}
        bool operator()(const QDomNode&n){return n.nodeValue().contains(mval);}
    };
    class ValueMatches
    {
        QRegularExpression mval;
        QString mvalfix;
        Qt::CaseSensitivity mcase=Qt::CaseSensitive;
    public:
        enum class PatternSyntax {FixedString, Wildcard, RegExp, RegularExpression=RegExp};
        ValueMatches(QString v,Qt::CaseSensitivity cs=Qt::CaseSensitive,PatternSyntax syn=PatternSyntax::Wildcard);
        ValueMatches(QString v,PatternSyntax syn):ValueMatches(v,Qt::CaseSensitive,syn){}
        ValueMatches(QRegularExpression v):mval(v){}
        bool operator()(const QDomNode&n);
    };

    inline Or operator|(DomQuery::FilterFunc f1,DomQuery::FilterFunc f2){return Or{f1,f2};}
    inline Or operator|(Or o,DomQuery::FilterFunc f){return o.append(f);}
    inline Or operator|(DomQuery::FilterFunc f,Or o){return o.prepend(f);}
    inline And operator&(DomQuery::FilterFunc f1,DomQuery::FilterFunc f2){return And{f1,f2};}
    inline And operator&(And o,DomQuery::FilterFunc f){return o.append(f);}
    inline And operator&(DomQuery::FilterFunc f,And o){return o.prepend(f);}

    inline Or operator||(DomQuery::FilterFunc f1,DomQuery::FilterFunc f2){return Or{f1,f2};}
    inline Or operator||(Or o,DomQuery::FilterFunc f){return o.append(f);}
    inline Or operator||(DomQuery::FilterFunc f,Or o){return o.prepend(f);}
    inline And operator&&(DomQuery::FilterFunc f1,DomQuery::FilterFunc f2){return And{f1,f2};}
    inline And operator&&(And o,DomQuery::FilterFunc f){return o.append(f);}
    inline And operator&&(DomQuery::FilterFunc f,And o){return o.prepend(f);}

    inline Not operator!(DomQuery::FilterFunc f){return Not(f);}
    inline Not operator~(DomQuery::FilterFunc f){return Not(f);}
};
