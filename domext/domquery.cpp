// Qt DOM Extensions
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2020
// protected under the GNU GPL v.3 or at your option any newer

#include "domquery.h"
#include "domiterator.h"

#include <algorithm>

using namespace DomFilter;

DomQuery & DomQuery::allChildren()
{
    DomNodeList ret;
    for(QDomNode cur:mcurrent)
        ret.append(cur.childNodes());
    mcurrent=ret;
    return *this;
}

DomQuery & DomQuery::allDescendants()
{
    return descendants(AllNodes);
}

DomQuery & DomQuery::allChildElements(QString name)
{
    if(name.isEmpty())
        return children(IsElement);
    else
        return children(Or{IsElement,ByName(name)});
}

DomQuery & DomQuery::allChildAttributes(QString name)
{
    if(name.isEmpty())
        return children(IsAttr);
    else
        return children(IsAttr|ByName(name));
}

DomQuery & DomQuery::allDescendantElements(QString name)
{
    if(name.isEmpty())
        return descendants(IsElement);
    else
        return descendants(IsElement|ByName(name));
}

DomQuery & DomQuery::allDescendantAttributes(QString name)
{
    if(name.isEmpty())
        return descendants(IsAttr);
    else
        return descendants(IsAttr|ByName(name));
}

DomQuery & DomQuery::children(DomQuery::FilterFunc f)
{
    DomNodeList ret;
    for(QDomNode node:mcurrent)
        for(QDomNode sub:node.childNodes())
            if(f(sub))ret.append(sub);
    mcurrent=ret;
    return *this;
}

//static
DomNodeList DomQuery::recurFilter(DomQuery::FilterFunc f,const QDomNodeList&l)
{
    DomNodeList ret;
    for(int i=0;i<l.size();i++){
        if(f(l.at(i))) ret.append(l.at(i));
        ret.append(recurFilter(f,l.at(i).childNodes()));
    }
    return ret;
}

DomQuery & DomQuery::descendants(DomQuery::FilterFunc f)
{
    DomNodeList ret;
    for(QDomNode node:mcurrent)
        ret.append(recurFilter(f,node.childNodes()));
    mcurrent=ret;
    return *this;
}

DomQuery & DomQuery::filter(DomQuery::FilterFunc f)
{
    DomNodeList ret;
    for(QDomNode node:mcurrent)
        if(f(node))ret.append(node);
    mcurrent=ret;
    return *this;
}

DomQuery & DomQuery::sort(DomQuery::CompareFunc lessThan)
{
    std::sort(mcurrent.begin(),mcurrent.end(),lessThan);
    return *this;
}

DomQuery & DomQuery::unique()
{
    DomNodeList ret;
    for(int i=mcurrent.size()-1;i>=0;i--){
        bool found=false;
        for(int j=0;j<i;j++){
            if(mcurrent[i]==mcurrent[j]){
                found=true;
                break;
            }
        }
        if(!found)ret.prepend(mcurrent[i]);
    }
    mcurrent=ret;
    return *this;
}

DomQuery & DomQuery::reverse()
{
    std::reverse(mcurrent.begin(),mcurrent.end());
    return *this;
}

DomQuery & DomQuery::path(const QStringList&p)
{
    for(QString elem:p)
        if(p.startsWith("@"))allChildAttributes(elem.mid(1));
        else allChildElements(elem);
    return *this;
}

ValueMatches::ValueMatches(QString v,Qt::CaseSensitivity cs,PatternSyntax syn)
:mcase(cs)
{
    switch(syn){
        case PatternSyntax::RegExp:
            mval=QRegularExpression(v,cs==Qt::CaseInsensitive?QRegularExpression::CaseInsensitiveOption:QRegularExpression::NoPatternOption);
            break;
        case PatternSyntax::Wildcard:
#if (QT_VERSION >= QT_VERSION_CHECK(6,0,0))
            mval=QRegularExpression::fromWildcard(v,cs);
#else
            mval=QRegularExpression(QRegularExpression::wildcardToRegularExpression(v),cs==Qt::CaseInsensitive?QRegularExpression::CaseInsensitiveOption:QRegularExpression::NoPatternOption);
#endif
            break;
        default:
            mvalfix=v;
            break;
    }
}

bool ValueMatches::operator()(const QDomNode&n)
{
    if(!mvalfix.isEmpty())
        return mvalfix.compare(n.nodeValue(),mcase)==0;
    else
        return mval.match(n.nodeValue()).hasMatch();

}
