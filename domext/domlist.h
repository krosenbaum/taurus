// ArduOS Browser/Configurator
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2009-2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDomNode>
#include <QDomElement>
#include <QDomAttr>
#include <QList>

class DomElementList;
class DomAttributeList;
class DomNodeList:public QList<QDomNode>
{
public:
    inline DomNodeList()=default;
    inline DomNodeList(const DomNodeList&)=default;
    inline DomNodeList(DomNodeList&&)=default;
    inline DomNodeList(const QDomNodeList&l){for(int i=0;i<l.size();i++)append(l.at(i));}
    inline DomNodeList(const QList<QDomAttr>&l){for(QDomNode n:l)append(n);}
    inline DomNodeList(const QList<QDomElement>&l){for(QDomNode n:l)append(n);}
    inline DomNodeList(const QDomNamedNodeMap&nm){for(int i=0;i<nm.size();i++)append(nm.item(i));}

    inline DomNodeList& operator=(const DomNodeList&)=default;
    inline DomNodeList& operator=(DomNodeList&&)=default;
    inline DomNodeList& operator=(const QList<QDomNode>&l){QList<QDomNode>::operator=(l);return *this;}
    inline DomNodeList& operator=(const QList<QDomElement>&l){clear();for(QDomNode n:l)append(n);return *this;}
    inline DomNodeList& operator=(const QList<QDomAttr>&l){clear();for(QDomNode n:l)append(n);return *this;}
    inline DomNodeList& operator=(const QDomNamedNodeMap&nm){clear();for(int i=0;i<nm.size();i++)append(nm.item(i));return *this;}

    using QList<QDomNode>::append;
    inline void append(const QDomNodeList&l){for(int i=0;i<l.size();i++)append(l.at(i));}
    inline void append(const QList<QDomAttr>&l){for(int i=0;i<l.size();i++)append(l.at(i));}
    inline void append(const QList<QDomElement>&l){for(int i=0;i<l.size();i++)append(l.at(i));}
    inline void append(const QDomNamedNodeMap&nm){for(int i=0;i<nm.size();i++)append(nm.item(i));}

    inline DomNodeList& operator+=(const QDomNodeList&l){append(l);return *this;}
    inline DomNodeList& operator+=(const QList<QDomAttr>&l){append(l);return *this;}
    inline DomNodeList& operator+=(const QList<QDomElement>&l){append(l);return *this;}
    inline DomNodeList& operator+=(const QList<QDomNode>&l){append(l);return *this;}
    inline DomNodeList& operator+=(const QDomNode&n){append(n);return *this;}
    inline DomNodeList& operator+=(const QDomNamedNodeMap&nm){append(nm);return *this;}

    DomElementList toElementList()const;
    DomAttributeList toAttributeList()const;
};

class DomElementList:public QList<QDomElement>
{
public:
    inline DomElementList()=default;
    inline DomElementList(const DomElementList&)=default;
    inline DomElementList(DomElementList&&)=default;
    inline DomElementList(const DomNodeList&l)
    {
        for(int i=0;i<l.size();i++)if(l[i].isElement())append(l[i].toElement());
    }
    inline DomElementList(const QDomNodeList&l)
    {
        for(int i=0;i<l.size();i++)if(l.at(i).isElement())append(l.at(i).toElement());
    }

    DomElementList& operator=(const DomElementList&)=default;
    DomElementList& operator=(DomElementList&&)=default;
    DomElementList& operator=(const QList<QDomNode>&l)
    {
        clear();
        for(int i=0;i<l.size();i++)if(l[i].isElement())append(l[i].toElement());
        return *this;
    }
    DomElementList& operator=(const QDomNodeList&l)
    {
        clear();
        for(int i=0;i<l.size();i++)if(l.at(i).isElement())append(l.at(i).toElement());
        return *this;
    }
    DomElementList& operator=(const QList<QDomElement>&l){QList<QDomElement>::operator=(l);return *this;}

    //TODO: append, +=

    DomNodeList toNodeList()const
    {
        return DomNodeList(*this);
    }
};

class DomAttributeList:public QList<QDomAttr>
{
public:
    inline DomAttributeList()=default;
    inline DomAttributeList(const DomAttributeList&)=default;
    inline DomAttributeList(DomAttributeList&&)=default;
    inline DomAttributeList(const DomNodeList&l)
    {
        for(int i=0;i<l.size();i++)if(l[i].isAttr())append(l[i].toAttr());
    }
    inline DomAttributeList(const QDomNodeList&l)
    {
        for(int i=0;i<l.size();i++)if(l.at(i).isAttr())append(l.at(i).toAttr());
    }
    inline DomAttributeList(const QDomNamedNodeMap&nm)
    {
        for(int i=0;i<nm.size();i++)if(nm.item(i).isAttr())append(nm.item(i).toAttr());
    }

    DomAttributeList& operator=(const DomAttributeList&)=default;
    DomAttributeList& operator=(DomAttributeList&&)=default;
    DomAttributeList& operator=(const DomNodeList&l)
    {
        clear();
        for(int i=0;i<l.size();i++)if(l[i].isAttr())append(l[i].toAttr());
        return *this;
    }
    DomAttributeList& operator=(const QDomNodeList&l)
    {
        clear();
        for(int i=0;i<l.size();i++)if(l.at(i).isAttr())append(l.at(i).toAttr());
        return *this;
    }

    DomNodeList toNodeList()const
    {
        return DomNodeList(*this);
    }
};

typedef DomAttributeList DomAttrList;

inline DomElementList DomNodeList::toElementList()const
{
    return DomElementList(*this);
}

inline DomAttributeList DomNodeList::toAttributeList() const
{
    return DomAttributeList(*this);
}
