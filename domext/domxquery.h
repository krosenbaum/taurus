// Qt DOM Extensions
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2010-2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QDomNode>
#include <QDomElement>
#include <QList>
#include <QtGlobal>

#include "domlist.h"

#ifdef DOMEXT_LIBRARY_BUILD
#define DOMEXT_EXPORT Q_DECL_EXPORT
#else
#define DOMEXT_EXPORT Q_DECL_IMPORT
#endif

class DOMEXT_EXPORT DomXQuery
{
public:
    DomXQuery()=default;
    DomXQuery(const DomXQuery&)=default;
    DomXQuery(DomXQuery&&)=default;
    DomXQuery(QString q,bool useNS=false){setQuery(q,useNS);}

    DomXQuery& operator=(const DomXQuery&)=default;
    DomXQuery& operator=(DomXQuery&&)=default;

    bool setQuery(QString,bool useNS=false);
    inline bool isValid()const{return mquery.size()>0 && merror.isEmpty();}
    inline bool hasError()const{return !merror.isEmpty();}
    inline QString error()const{return merror;}

    inline DomNodeList execute(const QDomDocument&doc){return execute(doc.documentElement());}
    DomNodeList execute(const QDomElement&);

private:
    enum class StartPoint{Base,Document,AnyWhere};
    StartPoint mStartPoint=StartPoint::Base;
    enum class TargetType {Element,Attribute};
    enum class CompareType {Exist,Equal,NotEqual,Less,LessOrEqual,Greater,GreaterOrEqual};
    struct QueryElement
    {
        QueryElement()=default;
        QueryElement(const QueryElement&)=default;
        QueryElement& operator=(const QueryElement&)=default;
        virtual ~QueryElement();

        QString targetName,nameSpace,prefix;
        TargetType targetType=TargetType::Element;
        bool hasNS=false;
        bool hasPrefix=false;

        virtual DomNodeList filter(const DomNodeList&);
    };
    class ComparableQueryElement:QueryElement
    {
        ComparableQueryElement()=default;
        ComparableQueryElement(const ComparableQueryElement&)=default;
        ComparableQueryElement& operator=(const ComparableQueryElement&)=default;

        bool hasCompare=false;
        QList<QueryElement>compareSearch;
        QString compareValue;

        virtual DomNodeList filter(const DomNodeList&);
    };
    QList<ComparableQueryElement>mquery;
    bool museNS=false;

    QString merror;
};
