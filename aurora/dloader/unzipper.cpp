// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#include "unzipper.h"
#include "fhelp.h"

#include <unistd.h>

#include <QDebug>
#include <QtXml>

constexpr char UnZipper::defaultOldFileExtension[];

UnZipper::UnZipper(QString filename,QString ext)
        :QObject(),
         mtgt(filename),
         moldFileExt(ext)
{
        if(mtgt.open(QIODevice::ReadOnly)){
                mzip.open(&mtgt);
                readMetaFile();
        }else
                qWarning()<<"Unable to read ZIP file"<<filename;
}

UnZipper::~UnZipper()
{
        close();
}

void UnZipper::close()
{
        if(mzip.isOpen()){
                mzip.close();
        }
        if(mtgt.isOpen())
                mtgt.close();
}



bool UnZipper::unpackAll(const QString&path)
{
        if(!mzip.isOpen()){
		qWarning()<<"Zip file is not open while trying to unpack files!";
		return false;
	}
        //go to start
        if(!mzip.firstFile().isValid()){
                qWarning()<<"Zip file does not seem to contain files.";
                return false;
        }
        //go through files
        do{
                ZipFileInfo inf=mzip.currentFile();
                if(inf.fileName()==".aurorameta")continue;
                //move old file out of the way
                const QString name=path+"/"+inf.fileName();
		if(!moldFileExt.isEmpty() && QFile(name).exists()){
			QFile(name+moldFileExt).remove();
			if(!QFile(name).rename(name+moldFileExt))
				qDebug()<<"Cannot rename old file"<<name;
		}
                //create new file
                QDir().mkpath(QFileInfo(name).absolutePath());
                QFile fd(name);
                if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
                        qDebug()<<"Unable to create file"<<name;
                        return false;
                }
                mzip.currentFile(fd);
                fd.close();
        }while(mzip.nextFile().isValid());
        //go through symlinks
        for(const QString&n:msymlinks.keys()){
                //move old file out of the way
                const QString name=path+"/"+n;
		if(!moldFileExt.isEmpty() && QFile(name).exists()){
			QFile(name+moldFileExt).remove();
			if(!QFile(name).rename(name+moldFileExt))
				qDebug()<<"Cannot rename old file/link"<<name;
		}
                //create new file
                if(!QFile(msymlinks[n]).link(name))
			qDebug()<<"Unable to create symlink"<<name;
        }
        //go through permissions
        for(const QString&n:mperms.keys()){
                QFile(path+"/"+n).setPermissions(mperms[n]);
        }
        //done
        return true;
}

void UnZipper::readMetaFile()
{
        //only if we have a ZIP
        if(!mzip.isOpen()){
		qWarning()<<"Zip file is not open while searching meta data.";
		return;
	}
        //find meta data
        if(!mzip.locateFile(".aurorameta").isValid()){
                return;
        }
        //create XML
        QDomDocument doc;
        if(!doc.setContent(mzip.currentFileContent())){
                qDebug()<<"Aurora Unzip: Meta Data File is invalid.";
                return;
        }
        QDomElement root=doc.documentElement();
        QDomNodeList nl=root.elementsByTagName("SymLink");
        for(int i=0;i<nl.size();i++){
                QDomElement sym=nl.at(i).toElement();
                if(sym.isNull())continue;
                msymlinks.insert(sym.attribute("file"),sym.attribute("target"));
        }
        nl=root.elementsByTagName("Perm");
        for(int i=0;i<nl.size();i++){
                QDomElement prm=nl.at(i).toElement();
                if(prm.isNull())continue;
                mperms.insert(prm.attribute("file"),(QFile::Permissions)prm.attribute("perm").toInt());
        }
}
