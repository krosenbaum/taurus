// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_SUBTASK_H
#define AURORA_SUBTASK_H

#include <QString>
#include <QObject>
#include <QUrl>
#include <QMap>
#include <QPointer>
#include <QTemporaryFile>
#include <QNetworkReply>

class QNetworkAccessManager;
namespace AuroraUpdater {

/// \internal helper class for the SubTask - it tracks a single connection/url
struct SubTasklet {
        ///the open reply object
        QPointer<QNetworkReply>reply;
        ///(temporary) file to hold the data
        QSharedPointer<QFile> file;
        ///user defined task ID, does not have to be unique
        int taskid=-1;
        
        ///used by QMap only, invalid instance
        SubTasklet(){}
        ///normal way to instantiate it: from the fresh reply object, 
        ///if the path is empty it auto-creates a temporary file otherwise a normal file
        SubTasklet(QNetworkReply*request,int taskid,const QString&path);
        ///copy constructor
        SubTasklet(const SubTasklet&)=default;
        ///copy operator
        SubTasklet& operator=(const SubTasklet&)=default;
        ///used by SubTask to trigger copying data from the reply to the temp file
        void copyNet2Local();
};

///Instances of this class track the retrieval of one or more URLs and
///signal finished() when all of them downloaded successfully or error()
///if any of them failed. In case of an error all running downloads are
///aborted.
///
///Per default this class stores downloaded files as temporary files. Those
///files are automatically deleted when this object is deleted. This setting
///can be changed by setting an explicit download path.
class SubTask:public QObject {
        Q_OBJECT
        public:
                ///instantiates a SubTask with no running downloads, use add to start downloading
                SubTask(QObject*parent=nullptr):SubTask(QList<QUrl>(),parent){}
                ///instantiates a SubTask and starts downloading with one initial URL
                SubTask(QUrl u,QObject*parent=nullptr):SubTask(u,-1,parent){}
                ///instantiates a SubTask and starts downloading with one initial URL
                SubTask(QUrl u,int taskid,QObject*parent=nullptr) :SubTask(QList<QUrl>()<<u,taskid,parent){}
                ///instantiates a SubTask and starts downloading with a list of initial URLs
                SubTask(QList<QUrl>u,QObject*parent=nullptr):SubTask(u,-1,parent){}
                ///instantiates a SubTask and starts downloading with a list of initial URLs
                SubTask(QList<QUrl>u,int taskid,QObject*parent=nullptr):QObject(parent){add(u,taskid);}
                
                ///returns the current target directory for downloads,
                ///returns an empty string if the SubTask is in temporary file mode
                QString targetDirectory()const{return mtargetdir;}
                
                ///sets the directory in which files are to be stored,
                ///set to an empty string if you want it to use temporary files
                void setTargetDirectory(const QString&s){mtargetdir=s;}
                
                ///adds a single URL to its downloads,
                ///this function refuses to add downloads after the SubTask has already finished
                ///\param url the URL to download
                ///\param taskid if >=0 the URL is assigned this task ID under which it can be found again
                ///\returns true if the URL was successfully added, false if the URL was not valid
                bool add(QUrl url,int taskid=-1){return add(QList<QUrl>()<<url, taskid);}
                ///adds a several URLs to its downloads, in case any of the URLs is not valid it does
                ///not start downloading any of them,
                ///this function refuses to add downloads after the SubTask has already finished
                ///\param urls the URLs to download
                ///\param firstTaskId if >=0: the task ID of the first URL, the second one gets firstTaskId+1, etc.
                ///\returns true if the URLs were successfully added, false if any of the URLs was not valid
                bool add(QList<QUrl>urls,int firstTaskId=-1);
                
                ///returns all URLs that this SubTask tries to download or has already downloaded
                QList<QUrl> urls()const{return mreq.keys();}
                ///returns the temporary file corresponding to the given URL,
                ///after finished() has been triggered this file contains the completely
                ///downloaded data
                QFile& file(QUrl u){return *mreq[u].file;}
                ///returns the temporary file corresponding to a task ID, if the task ID has
                ///several URLs associated it is undefined which one is returned,
                ///after finished() has been triggered this file contains the completely
                ///downloaded data
                QFile& file(int taskid);
                
                ///returns true if this task is tracking this URL
                bool hasUrl(QUrl u)const{return mreq.contains(u);}
                ///returns true if this task knows this task ID
                bool hasTaskId(int id)const;
                
                ///represents the internal state of the object
                enum State{
                        ///the object has not started or is in the process of downloading
                        Working,
                        ///the SubTask has finished with an error
                        Error,
                        ///the SubTask has successfully finished
                        Success
                };
                
                ///returns the current state of the instance
                State state()const{return mstate;}
                
        private slots:
                /// \internal reacts to finished downloads
                void finishedReq();
                /// \internal reacts to network errors
                void errorReq();
                /// \internal reads data from open connections
                void readFromNet();
                
        signals:
                ///emitted if/when a network error occurs, all downloads are aborted
                void error();
                ///emitted when all downloads have finished successfully
                void finished();
        private:
                //network handling
                QNetworkAccessManager*mnam=nullptr;
                //all added URLs and corresponding requests and temp files
                QMap<QUrl,SubTasklet>mreq;
                //current internal state
                State mstate=Working;
                //directory to store files to
                QString mtargetdir;
};

//end of namespace
}

#endif