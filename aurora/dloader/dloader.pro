# QMake file for the Aurora Library

TEMPLATE = lib
TARGET = Aurora
VERSION =

# sources...
SOURCES += \
        aurora.cpp \
        subtask.cpp \
        gpgwrap.cpp \
        unzipper.cpp \
        ../common/metafile.cpp

HEADERS += \
        aurora.h \
        subtask.h \
        gpgwrap.h \
        unzipper.h \
        ../common/metafile.h

#store with all other Taurus libs
DESTDIR = $$PWD/../../lib

#Qt: need networking and XML files
QT -= gui
QT += network xml
CONFIG += dll create_prl hide_symbols separate_debug_info

#FIXME:
equals(QT_MAJOR_VERSION, 6): QT += core5compat

#temp files
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

# include common files
INCLUDEPATH += ../common

# tell compiler we are building the lib itself
DEFINES += AURORA_LIBRARY_BUILD=1

# include ZIP stuff
include (../../zip.pri)

# GPG dependencies for signature checking
#INCLUDEPATH += gpg/include
#LIBS += -L../gpg/lib -lgpgme-pthread -lassuan -lgpg-error

# use C++11 features
gcc {
  QMAKE_CXXFLAGS += -std=c++11
}

#hack for proper recompiles during development
DEPENDPATH += $$INCLUDEPATH

#protect the stack
QMAKE_CFLAGS += -fstack-protector-all -Wstack-protector
QMAKE_CXXFLAGS += -fstack-protector-all -Wstack-protector
win32 {
  QMAKE_LFLAGS +=  -Wl,--nxcompat -Wl,--dynamicbase
  LIBS += -lssp
}
