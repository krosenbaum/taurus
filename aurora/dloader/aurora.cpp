// Copyright (C) 2012-2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "aurora.h"
#include "subtask.h"
#include "gpgwrap.h"
#include "unzipper.h"
#include "metafile.h"
#include "fhelp.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDomDocument>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSettings>
#include <QTimer>
#include <QDateTime>
#include <QDir>

struct AuroraUpdater::AuroraPrivate
{
        AuroraPrivate(const QString&);
        
        Aurora::State state=Aurora::Initializing;
        QString indexfile,localbasepath;
        int pollint=0;
        QTimer*polltmr=nullptr;
        QPointer<SubTask>task;
        MetaFile metafilenew,metafilecur;
        
        bool isBlocking()const{return state!=Aurora::Initializing && state!=Aurora::Polling;}
        
        QString downloadDir()const{return localbasepath+"/aurora_downloads";}
        QString oldFileExtension=UnZipper::defaultOldFileExtension;
};

Aurora::Aurora(QObject* parent)
        :Aurora(qApp->applicationFilePath()+".aurora_xml",parent)
{
}

Aurora::Aurora(const QString&xmlfile,QObject* parent)
        :QObject(parent)
{
        d=new AuroraPrivate(xmlfile);
        //make sure the timer triggers
        startPoll();
}

Aurora::~Aurora()
{
        if(d)delete d;
        d=0;
}


static const QString AuroraGroup("aurora_updater");
static const QString AuroraBaseUrlAttr("baseurl");
static const QString AuroraIndexFileAttr("indexfile");
static const QString AuroraPollIntervalAttr("pollinterval");
static const QString AuroraBaseUrlKey(AuroraGroup+"/"+AuroraBaseUrlAttr);
static const QString AuroraIndexFileKey(AuroraGroup+"/"+AuroraIndexFileAttr);
static const QString AuroraPollIntervalKey(AuroraGroup+"/"+AuroraPollIntervalAttr);

//Task IDs for specific files
static const int AuroraTaskMainXml=100;
static const int AuroraTaskMainXmlSig=AuroraTaskMainXml+1;
static const int AuroraTaskFirstFile=1000;

AuroraPrivate::AuroraPrivate(const QString&xmlfile)
        :metafilecur(xmlfile)
{
        if(metafilecur.isValid())
                indexfile=QFileInfo(xmlfile).absoluteFilePath();
        //try to initialize from program settings
        pollint=QSettings().value(AuroraPollIntervalKey,-1).toInt();
        //try to initialize from XML file
        if(pollint<0){
                pollint=metafilecur.pollInterval();
        }
        //determine base directory
        localbasepath=QFileInfo(xmlfile).absolutePath();
        //output base data for debugging
        qDebug()<<"Aurora: Initializing Auto-Updater from"<<xmlfile; 
        qDebug()<<"         URL:"<<metafilecur.baseUrl();
        qDebug()<<"  Index File:"<<metafilecur.indexFile();
        qDebug()<<"    Interval:"<<pollint<<"s";
        qDebug()<<"Aurora current Version Info:";
        qDebug()<<"      built:"<<metafilecur.buildTime().toString();
        qDebug()<<"  installed:"<<metafilecur.installTime().toString();
        qDebug()<<"    version:"<<metafilecur.version()<<" (Rev."<<metafilecur.machineVersion()<<")";
        
        //init GPG; TODO: make this controllable from outside
        GpgWrapper::initPath(localbasepath+"/aurora-gpg",localbasepath);
}

QString Aurora::indexFileName() const
{
        return d->indexfile;
}

QUrl Aurora::baseUrl() const
{
        return d->metafilecur.baseUrl();
}

Aurora::State Aurora::state() const
{
        return d->state;
}

int Aurora::pollInterVall() const
{
        return d->pollint;
}

void Aurora::setPollIntervall(int seconds)
{
        if(seconds<0)seconds=0;
        QSettings c;
        c.setValue(AuroraPollIntervalKey,seconds);
        qDebug()<<"Aurora: Changing Interval to"<<seconds<<"s";
        startPoll(seconds);
}

void Aurora::startPoll(int intervalInSeconds)
{
        if(intervalInSeconds>=0){
                d->pollint=intervalInSeconds;
                QSettings().setValue(AuroraPollIntervalKey,d->pollint);
        }
        stopPoll();
        QFileInfo dir(d->localbasepath);
        if(!dir.exists() || !dir.isDir() || !dir.isWritable()){
                qDebug()<<"Aurora: install directory of this instance is not writable, no use in polling.";
                return;
        }
        if(!QUrl(d->metafilecur.baseUrl()).isValid() || d->indexfile.isEmpty()){
                qDebug()<<"Aurora: no valid URL and/or index file, cannot poll.";
                return;
        }
        if(!GpgWrapper().testGPG()){
                qDebug()<<"Aurora: there is no GPG or GPG is on strike, cannot verify downloads so there is no point in polling.";
                return;
        }
        if(d->pollint>0){
                d->polltmr=new QTimer(this);
                connect(d->polltmr,SIGNAL(timeout()), this,SLOT(pollnow()));
                d->polltmr->start(d->pollint*1000);
                if(!d->isBlocking())
                        d->state=Polling;
                qDebug()<<"Aurora: Starting Timer.";
                QTimer::singleShot(5,this,SLOT(pollnow()));
        }else
                qDebug()<<"Aurora: polling is deactivated, not starting.";
}

void Aurora::stopPoll()
{
        if(d->polltmr){
                delete d->polltmr;
                d->polltmr=0;
                if(!d->isBlocking())
                        d->state=Initializing;
                qDebug()<<"Aurora: Stopping Timer.";
        }
}

void Aurora::pollnow()
{
        if(d->state==PostInstall){
                qDebug()<<"Aurora: Post-Install mode, not polling.";
                return;
        }
        if(d->isBlocking()){
                qDebug()<<"Aurora: missing an update poll cycle, still working on another one...";
                return;
        }
        qDebug()<<"Aurora: Polling for updates...";
	//make sure download dir exists
        const QString targetdirname=d->downloadDir();
        QDir().mkpath(targetdirname);
        QDir targetdir(targetdirname);
        //start the web request
        if(!d->task.isNull())d->task->deleteLater();
        QUrl u2(d->metafilecur.baseUrl());
        const QString p=u2.path(QUrl::FullyEncoded);
        u2.setPath(p+"/"+d->metafilecur.indexFile(),QUrl::StrictMode);
        d->task=new SubTask();
	d->task->setTargetDirectory(targetdirname);
        d->task->add(u2,AuroraTaskMainXml);
	u2.setPath(p+"/"+d->metafilecur.indexFile()+".asc",QUrl::StrictMode);
        d->task->add(u2,AuroraTaskMainXmlSig);
        connect(d->task,SIGNAL(finished()),this,SLOT(checkMetaFile()));
        connect(d->task,SIGNAL(error()),this,SLOT(abortCycle()));
}


void Aurora::checkMetaFile()
{
        if(d->task.isNull()){
                qDebug()<<"Hmm. Aurora::checkMetaFile called for no good reason, returning to poll.";
                d->state=Polling;
                return;
        }
        //verify signature
        if(GpgWrapper().verifyFile(d->task->file(AuroraTaskMainXml), d->task->file(AuroraTaskMainXmlSig))){
                qDebug()<<"Aurora: successfully verified XML metafile.";
                //create meta file object
                d->metafilenew=MetaFile(d->task->file(AuroraTaskMainXml).fileName());
                d->task->deleteLater();
                if(d->metafilenew.isValid()){
                        if(d->metafilecur.machineVersion()<d->metafilenew.machineVersion()){
                                qDebug()<<"This is a new version, download?";
                                d->state=WaitForDownload;
                                emit newVersionAvailable(d->metafilenew.version());
                        }else{
                                qDebug()<<"Not a new version, skipping it.";
                                qDebug()<<"  current:"<<d->metafilecur.machineVersion()<<d->metafilecur.version();
                                qDebug()<<"      new:"<<d->metafilenew.machineVersion()<<d->metafilenew.version();
                                d->state=Polling;
                                emit notNewVersion();
                        }
                }else{
                        d->state=Polling;
                        qDebug()<<"Aurora: Metafile is corrupt. Waiting for next cycle.";
                        emit newVersionCorrupt();
                }
        }else{
                d->task->deleteLater();
                qDebug()<<"Aurora: metafile is not valid or bad signature, not continuing!";
                d->state=Polling;
                emit newVersionSignatureFailed();
        }
}

void Aurora::abortCycle()
{
        qDebug()<<"Aurora: Aborting current poll cycle.";
        if(!d->task.isNull())d->task->deleteLater();
        d->state=Polling;
}

void Aurora::startDownload()
{
        //make sure download dir exists
        const QString targetdirname=d->downloadDir();
        QDir().mkpath(targetdirname);
        QDir targetdir(targetdirname);
        //check the files we already have
        QList<QUrl> needarcs;
        for(const Archive&a:d->metafilenew.localPlatformArchives()){
                ArchiveFileInfo zinfo(targetdirname+"/"+a.name());
                if(!zinfo.isValid() || zinfo.sha256sum()!=a.sha256sum())
                        needarcs.append(d->metafilenew.baseUrl()+"/"+a.name());
        }
        //have everything?
        if(needarcs.size()==0){
                d->state=WaitForInstall;
                emit readyForInstallation();
                return;
        }
        //download
        d->state=Downloading;
        d->task=new SubTask(this);
        d->task->setTargetDirectory(targetdirname);
        connect(d->task,SIGNAL(finished()),this,SLOT(checkDownloads()));
        connect(d->task,SIGNAL(error()),this,SLOT(abortCycle()));
        connect(d->task,SIGNAL(error()),this,SIGNAL(downloadFailed()));
        d->task->add(needarcs,AuroraTaskFirstFile);
}

void Aurora::checkDownloads()
{
        if(d->task.isNull())return;
        //task can be closed, we have permanent files
        d->task->deleteLater();
        //check the checksums of all downloads
        qDebug()<<"Checking downloaded files.";
        const QList<Archive>arcs=d->metafilenew.localPlatformArchives();
        const QString targetdirname=d->downloadDir();
        for(const Archive&a:arcs){
                ArchiveFileInfo zinfo(targetdirname+"/"+a.name());
                if(!zinfo.isValid()){
                        qDebug()<<"Expected archive"<<a.name()<<"does not exist, cannot install.";
                        d->state=Polling;
                        emit downloadFailed();
                        return;
                }
                if(zinfo.sha256sum()!=a.sha256sum()){
                        qDebug()<<"Archive"<<a.name()<<"has an unexpected checksum.";
                        qDebug()<<"   expected:"<<a.sha256sum();
                        qDebug()<<"    but got:"<<zinfo.sha256sum();
                        d->state=Polling;
                        emit downloadFailed();
                        return;
                }
        }
        //done checking, ask for install
        d->state=WaitForInstall;
        emit readyForInstallation();
}

void Aurora::startInstallation()
{
        d->state=Installing;
        //clean up
        clearOldFiles(true);
        //go through archives and unzip
        for(const Archive&arc:d->metafilenew.localPlatformArchives()){
                if(!arc.isValid()){
                        qDebug()<<"Ooops. Archive"<<arc.name()<<"does not seem to be valid, skipping.";
                        continue;
                }
                const Archive oldarc=d->metafilecur.archive(arc.name());
                if(oldarc.isValid() && oldarc.sha256sum()==arc.sha256sum()){
                        qDebug()<<"Archive"<<arc.name()<<"is unchanged, skipping.";
                        continue;
                }
                UnZipper unzip(d->downloadDir()+"/"+arc.name(),d->oldFileExtension);
                if(unzip.unpackAll(d->localbasepath)==false){
                        qDebug()<<"Aurora: Error while unpacking"<<arc.name()<<"- rolling back!";
                        rollbackOldFiles();
                        //go to inactive state: we would likely fail again
                        d->state=Initializing;
                        emit installationFailed();
                        return;
                }else
                        qDebug()<<"Aurora: done unpacking"<<arc.name();
        }
        //write new meta file
        d->metafilenew.writeMetaFile(d->indexfile);
        qDebug()<<"Aurora: installation finished.";
        //done
        d->state=PostInstall;
        emit installationFinished();
}

static inline void clearPathOldies(QDir d,const QString&extension)
{
        for(const QFileInfo&fi:d.entryInfoList(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot))
                if(fi.isDir())clearPathOldies(QDir(fi.absoluteFilePath()),extension);
                else if(fi.fileName().endsWith(extension)){
                        qDebug()<<"Aurora cleanup: removing"<<fi.absoluteFilePath();
                        d.remove(fi.fileName());
                }
}

void Aurora::clearOldFiles(bool force)
{
        //TODO: if force==false, check that the file is at least x days old
        Q_UNUSED(force);
        //go through dirs...
        clearPathOldies(QDir(d->localbasepath),d->oldFileExtension);
}

static inline void revivePathOldies(QDir d,const QString&extension)
{
        for(const QFileInfo&fi:d.entryInfoList(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot))
                if(fi.isDir())revivePathOldies(QDir(fi.absoluteFilePath()),extension);
                else if(fi.fileName().endsWith(extension)){
                        const QString rest=fi.absoluteFilePath();
                        const QString rem=rest.left(rest.size()-extension.size());
                        qDebug()<<"Aurora rollback: moving"<<rest;
                        QFile(rem).remove();
                        QFile(rest).rename(rem);
                }
}

void Aurora::rollbackOldFiles()
{
        revivePathOldies(QDir(d->localbasepath),d->oldFileExtension);
}
