// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_UNZIPPER_H
#define AURORA_UNZIPPER_H

#include <QObject>
#include <QFile>
#include <QMap>
#include <QLatin1String>

#include <Unzip>

class UnZipper:public QObject
{
        Q_OBJECT
public:
	static constexpr char defaultOldFileExtension[]=".aurora_old";

	UnZipper(QString filename,QString oldFileExtension=defaultOldFileExtension);
        ~UnZipper();
public slots:
        void setOldFileExtension(const QString&e){moldFileExt=e;}
        
        bool unpackAll(const QString&path);
        
        void close();
private:
        QMap<QString,QString>msymlinks;
        QMap<QString,QFile::Permissions>mperms;
        QtZipHelper::Unzip mzip;
        QFile mtgt;
        QString moldFileExt;
        
        void readMetaFile();
};

#endif
