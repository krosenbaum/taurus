// Copyright (C) 2012 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_MAIN_H
#define AURORA_MAIN_H

#include <QString>
#include <QObject>
#include <QUrl>

#ifdef AURORA_LIBRARY_BUILD
#define AURORA_EXPORT Q_DECL_EXPORT
#else
#define AURORA_EXPORT Q_DECL_IMPORT
#endif

namespace AuroraUpdater {

class AuroraPrivate;
/** The Aurora class is the main interface for the automatic updater.
 * 
 * Normally you just instantiate it. You only need to set the base URL and the
 * polling interval once - next time the instance will remember the settings.
 * */
class AURORA_EXPORT Aurora:public QObject
{
        Q_OBJECT
        public:
                ///instantiates the updater object, normally you should only need one of them
                explicit Aurora(QObject*parent=0);
                ///instantiates the updater object, normally you should only need one of them
                explicit Aurora(const QString &xmlfile,QObject*parent=0);
                ///deletes the updater object (stopping any update that is in progress)
                virtual ~Aurora();
                
                ///returns the currently set base URL
                QUrl baseUrl()const;
                ///returns the name of the index file that contains meta data about releases,
                ///the name of the signature file is inferred to be the same plus ".asc"
                QString indexFileName()const;
                
                ///State of the Updater object
                enum State{
                        ///the instance is initializing or inactive
                        Initializing=0,
                        ///the instance is currently polling for an update on meta data
                        Polling=1,
                        ///the instance is currently waiting for the user to start downloading
                        WaitForDownload=5,
                        ///the instance is downloading a new release
                        Downloading=2,
                        ///the instance is currently waiting for the user to start installing
                        WaitForInstall=6,
                        ///the instance has verified ...
                        Installing=3,
                        ///installation is done, not doing anything anymore
                        PostInstall=4
                };
                
                ///returns the current state of the Aurora object
                State state()const;
                
                ///returns true if the object is waiting for the user to call startDownload()
                bool isReadyForDownload()const{return state()==WaitForDownload;}
                ///returns true if the object is waiting for the user to call startInstallation()
                bool isReadyForInstall()const{return state()==WaitForInstall;}
                ///returns true if the object is currently looking for updates
                bool isPolling()const{return state()==Polling;}
                ///returns true if the object is done after installation
                bool isDone()const{return state()==PostInstall;}
                ///returns true if the object is currently polling, downloading, or installing
                bool isActive()const{State s=state();return s!=Initializing && s!=PostInstall;}
                ///returns true if the object is currently initializing or is done installing
                bool isInactive()const{return !isActive();}
                ///returns true if the object is currently waiting for user interaction
                bool isWaiting()const{State s=state();return s==WaitForDownload || s==WaitForInstall;}
                
                ///returns the interval in seconds between update polls
                int pollInterVall()const;
                
        public slots:
                ///starts polling for updates
                ///\param intervalInSeconds if given: implicitly changes the poll interval
                void startPoll(int intervalInSeconds=-1);
                ///stops polling, effectively switching the object off
                void stopPoll();
                ///changes the polling interval
                void setPollIntervall(int seconds);
                ///starts the download while in WaitForDownload state
                void startDownload();
                ///starts installation while in WaitForInstall state
                void startInstallation();
                ///clears out old files from a former installation
                void clearOldFiles(bool force=false);
        signals:
                ///a new version is available for download, call startDownload() to start
                ///the object remains in the WaitForDownload state until the program exits
                void newVersionAvailable(const QString&);
                ///a new version has been detected, but the signature failed,
                ///the object returns to Polling state and tries again later
                void newVersionSignatureFailed();
                ///a new version has been detected, but the XML file is corrupted,
                ///the object returns to Polling state and tries again later, 
                ///maybe a better one is uploaded?
                void newVersionCorrupt();
                ///a polling cycle was successful, but no new version was found
                void notNewVersion();
                ///the download is complete and the object is ready for installation,
                ///call startInstallation() to begin
                void readyForInstallation();
                ///the download failed, or one of the files had a non-matching checksum,
                ///the object returns to Polling state an will try again later, you may
                ///switch it off by calling 
                void downloadFailed();
                ///reports progress during download: having downloaded bytesdone of bytesneeded
                void downloadProgress(int bytesdone,int bytesneeded);
                ///installation has finished, when starting next the new version will be used
                void installationFinished();
                ///installation has failed, the old version has been restored
                void installationFailed();
                
        private slots:
                ///\internal starts an update check cycle
                void pollnow();
                ///\internal aborts the running update cycle
                void abortCycle();
                ///\internal when the intial index file was downloaded: check its signature
                void checkMetaFile();
                ///\internal called when file downloads are done: check shasums
                void checkDownloads();
        private:
                AuroraPrivate *d;
                
                ///\internal used by installer routine: rolls back old files
                void rollbackOldFiles();
};

//end of namespace
}

using namespace AuroraUpdater;

#endif
