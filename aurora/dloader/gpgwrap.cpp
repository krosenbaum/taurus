// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "gpgwrap.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QProcess>

using namespace AuroraUpdater;

QString GpgWrapper::sHome,GpgWrapper::sExe;

static inline QString path2exe(const QString&p)
{
        static const QString exe=
#ifdef Q_OS_WIN
                "gpg.exe"
#else
                "gpg"
#endif
                ;

        if(p.isEmpty())return exe;
        else return p+"/"+exe;
}

static inline bool testGPG(const QString&path)
{
        QProcess prc;
        prc.start(path2exe(path),QStringList()<<"--batch"<<"--version");
        if(prc.waitForFinished(2000))
                return true;
        prc.terminate();
        return false;
}

static inline bool testHome(const QString&h)
{
        if(h.isEmpty())return true;
        QFileInfo fi(h);
        return fi.exists()&&fi.isDir();
}

bool GpgWrapper::initPath(const QString& homedir, const QString& exedir)
{
        if(!QFileInfo(homedir).exists())return false;
        if(::testGPG(exedir)&&testHome(homedir)){
                sExe=exedir;
                sHome=homedir;
                return true;
        }else return false;
}


GpgWrapper::GpgWrapper(const QString& homedir, const QString& exedir)
{
        if(homedir.isEmpty())mHome=sHome;
        else mHome=homedir;
        if(exedir.isEmpty())mExe=sExe;
        else mExe=exedir;
}

bool GpgWrapper::testGPG() const
{
        return testHome(mHome)&& ::testGPG(mExe);
}


bool GpgWrapper::verifyFile(QFile& dataFile, QFile& sigFile)const
{
	dataFile.close();sigFile.close();
        //Open process
        QProcess prc;
        QStringList args;
        args<<"--batch"<<"--no-sk-comment"<<"--lc-messages"<<"C"<<"--lc-ctype"<<"C";
        if(!mHome.isEmpty())
                args<<"--homedir"<<mHome;
        args<<"--status-fd"<<"1"<<"--no-tty"<<"--charset"<<"utf8";
        args<<"--verify"<<"--"<<QDir::toNativeSeparators(sigFile.fileName())<<QDir::toNativeSeparators(dataFile.fileName());
        const QString exe=path2exe(mExe);
        qDebug()<<"Executing"<<exe<<args;
        prc.start(exe,args);
        if(!prc.waitForFinished(10000)){
                qDebug()<<"Unable to execute GPG or GPG is blocked.";
                prc.terminate();
                qDebug()<<prc.readAllStandardError();
                return false;
        }
        const QString res=QString ::fromUtf8(prc.readAllStandardOutput());
        qDebug()<<"GPG result"<<prc.exitCode()<<"\n"<<res<<"\n---\n"<<prc.readAllStandardError();
        const QStringList resl=res.split("\n",Qt::SkipEmptyParts);
        //parse results
        bool result=false,valsig=false;
        for(QString line: resl){
                line=line.trimmed();
                if(!line.startsWith("[GNUPG:] "))continue;
                const QStringList tok=line.split(" ",Qt::SkipEmptyParts);
                if(tok.size()<2)continue;
                //we look for a good signature
                if(tok[1]=="GOODSIG" || tok[1]=="VALIDSIG")valsig=true;
                else if(tok[1].endsWith("SIG")) valsig=false;
                //and immediately after that: trust (full or ultimate)
                if(valsig && ( tok[1]=="TRUST_ULTIMATE" || tok[1]=="TRUST_FULLY"))
                        result=true;
        }

        //done
        return result;
}

