// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_GPGWRAP_H
#define AURORA_GPGWRAP_H

#include <QObject>

class QFile;
namespace AuroraUpdater {


class GpgWrapper
{
        public:
                static bool initPath(const QString&homedir,const QString&exedir=QString());
                
                GpgWrapper(const QString&homedir=QString(),const QString&exedir=QString());
                
                bool verifyFile(QFile& dataFile,QFile& sigFile)const;
                
                bool testGPG()const;
                
        private:
                static QString sExe,sHome;
                QString mExe,mHome;
};

//end of namespace
}


#endif
