// Copyright (C) 2012-2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "subtask.h"
#include <QDebug>
#include <QFileInfo>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

bool AuroraUpdater::SubTask::add(QList< QUrl > urls,int ftid)
{
        qDebug()<<"SubTask::add"<<urls;
        if(urls.size()==0)return true;//this is a fake true: we do nothing instantly
        if(mstate!=Working){
                qDebug()<<"SubTask: not working anymore, cannot add more URLs now";
                return false;
        }
        if(mnam==nullptr)
                mnam=new QNetworkAccessManager(this);
        //go through urls, check for validity
        for(auto url:urls)
                if(!url.isValid())return false;
        //go through urls, create network requests
        for(QUrl url:urls){
                //are we already downloading it?
                bool found=false;
                for(const auto &entry:mreq.keys())
                        if(entry==url){
                                found=true;
                                break;
                        }
                if(found)continue;
                //create appropriate name
                QString name;
                if(!mtargetdir.isEmpty())name=mtargetdir+"/"+QFileInfo(url.path()).fileName();
                //start downloading
                QNetworkReply*rep=mnam->get(QNetworkRequest(url));
                mreq.insert(url,SubTasklet(rep,ftid,name));
                connect(rep,SIGNAL(finished()),this,SLOT(finishedReq()));
                connect(rep,SIGNAL(error(QNetworkReply::NetworkError)), this,SLOT(errorReq()));
                connect(rep,SIGNAL(readyRead()),this,SLOT(readFromNet()));
                if(ftid>=0)ftid++;
        }
        return true;
}

void AuroraUpdater::SubTask::finishedReq()
{
//         qDebug()<<"SubTask::finishedReq"<<hex<<(quint64)sender();
        if(mstate!=Working){
                qDebug()<<"SubTask::finished: not working anymore, ignoring fake successs";
                return;
        }
        //find the sub-task
        QNetworkReply*rp=qobject_cast<QNetworkReply*>(sender());
        if(rp==nullptr)return;
        for(auto &rq:mreq)
                if(rp==rq.reply){
                        rq.copyNet2Local();
//                         qDebug()<<"SubTask::finishedReq: deleting connection";
                        rq.reply->deleteLater();
                        rq.reply=nullptr;
                }
        //is everything finished?
        for(const auto &rq:mreq){
                if(!rq.reply.isNull())return;
        }
        //flush all files
        for(const auto & rq:mreq)
                if(!rq.file.isNull() && rq.file->isOpen())
                        rq.file->flush();
        //done
        qDebug()<<"SubTask: I'm done, emitting finished()";
        mstate=Success;
        emit finished();
}

void AuroraUpdater::SubTask::errorReq()
{
//         qDebug()<<"SubTask::errorReq"<<hex<<(quint64)sender();
        if(mstate!=Working){
                qDebug()<<"SubTask::errorReq: not working anymore, ignoring follow-up error";
                return;
        }
        mstate=Error;
        if(mreq.size()==0)return;
        //abort all requests
        for(const auto &r:mreq){
                if(!r.reply.isNull()){
                        if(r.reply != sender()){
//                                 qDebug()<<"SubTask::errorReq: aborting connection";
                                r.reply->abort();
                        }
//                         qDebug()<<"SubTask::errorReq: deleting connection";
                        r.reply->deleteLater();
                }
        }
        mreq.clear();
        //signal my own error
        qDebug()<<"SubTask::errorReq: done, emitting error";
        emit error();
}

void AuroraUpdater::SubTask::readFromNet()
{
//         qDebug()<<"SubTask::readFromNet"<<hex<<(quint64)sender();
        QNetworkReply*r=qobject_cast<QNetworkReply*>(sender());
        if(r==nullptr)return;
        for(auto &rq:mreq)
                if(rq.reply == r)
                        rq.copyNet2Local();
}

void AuroraUpdater::SubTasklet::copyNet2Local()
{
//         qDebug()<<"SubTasklet::copyNet2Local, this="<<hex<<(quint64)this;
        if(reply.isNull())return;
        if(reply->bytesAvailable()<1)return;
        file->seek(file->size());
        file->write(reply->readAll());
}

QFile& AuroraUpdater::SubTask::file(int taskid)
{
        for(const SubTasklet&st:mreq)
                if(st.taskid==taskid)
                        return *st.file;
        static QFile f;
        qDebug()<<"Aurora SubTask: file for ID"<<taskid<<"cannot be found!";
        f.close();f.setFileName("");
        return f;
}

bool AuroraUpdater::SubTask::hasTaskId(int id) const
{
        for(const SubTasklet&st:mreq)
                if(st.taskid==id)
                        return true;
        return false;
}

AuroraUpdater::SubTasklet::SubTasklet(QNetworkReply* r, int t,const QString&p)
        :reply(r),file(p.isEmpty()?new QTemporaryFile:new QFile(p)),taskid(t)
{
        if(p.isEmpty())
                ((QTemporaryFile*)file.data())->open();
        else
                file->open(QIODevice::WriteOnly|QIODevice::Truncate);
}
