// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_META_H
#define AURORA_META_H

#include <QStringList>
#include <QDateTime>
#include <QRegExp>

//little hack to pacify KDevelop
#ifdef IN_KDEVELOP_PARSER
#define AURORA_INSTGEN_BUILD
#endif
//end of hack

class QDomElement;
namespace AuroraUpdater {

#ifdef AURORA_INSTGEN_BUILD

#ifdef Q_OS_WIN
#define CS Qt::CaseInsensitive
#else
#define CS Qt::CaseSensitive
#endif
///instgen only: represents a file pattern of an archive,
///refers to one pattern line of a <Files> tag
class ArchiveFile {
        QString mSource,mDest;
        QRegExp mpat,mexclude;
        
        friend class Archive;
        ///instantiates a pattern object
        ArchiveFile(QString src,QString dst,QString pat,QString excl)
         :mSource(src.replace('\\','/')),
          mDest(dst.replace('\\','/')),
          mpat(pat,CS,QRegExp::WildcardUnix),
          mexclude(excl,CS,QRegExp::WildcardUnix)
         {}
public:
        ///needed by QList: invalid pattern
        ArchiveFile(){}
        ///copies a pattern
        ArchiveFile(const ArchiveFile&)=default;
        
        ///returns the complete source path relative to the CWD
        QString sourcePath()const{return mSource;}
        ///returns the destination path inside the ZIP file
        QString destinationPath()const{return mDest;}
        ///returns a regular expression to represent the file pattern
        QRegExp pattern()const{return mpat;}
        ///returns the optional exclusion pattern: files that match the pattern(), 
        ///but are not to be included
        QRegExp excludePattern()const{return mexclude;}
        
        ///returns true if the given file name (relative to sourcePath()) matches
        ///the pattern
        bool fileMatches(QString filename)const;
        ///returns all matching files under sourcePath() (relative to sourcePath())
        QStringList getFiles()const;
};
#undef CS
#endif

///Description of a single Archive ZIP File
class Archive {
        QString mName,msha256,mos,mcpu;
        qint64 mzipsize=-1;
        QStringList mDestFiles;
        
        friend class MetaFile;
        ///used by library (and optionally from instgen): instantiate from <Archive> tag
        Archive(const QDomElement&);
#ifdef AURORA_INSTGEN_BUILD
        ///used by instgen: instantiate from <Platform> and <Files>
        Archive(const QDomElement&,const QString&,const QString&,const QString&);
        QString mBuildBase,mZipBase;
        bool misSource=false;
        QList<ArchiveFile>mFiles;
#endif
public:
        ///instantiate invalid archive object
        Archive(){}
        ///copy archive object
        Archive(const Archive&)=default;
        
        ///returns the archive's file name (always non-patterned, final version)
        QString name()const{return mName;}
        
        ///returns true if this is a valid archive (i.e. it has a name)
        bool isValid()const{return !mName.isEmpty();}
        
        ///returns the checksum of the file
        QString sha256sum()const{return msha256;}
        
        ///returns the operating system that is targeted by this archive, the
        ///special value "any" means a source spec can build any OS, but the built
        ///archive is OS specific, while "all" means it can build for any OS and the
        ///archive is OS independent
        QString operatingSystem()const{return mos;}
        ///returns the CPU that this archive targets, the
        ///special value "any" means a source spec can build any OS, but the built
        ///archive is OS specific, while "all" means it can build for any OS and the
        ///archive is OS independent
        QString cpu()const{return mcpu;}
        
        ///returns the size of the ZIP file
        qint64 zipSize()const{return mzipsize;}
        
        ///files actually contained in the ZIP file (including symlinks)
        QStringList destFiles()const{return mDestFiles;}
        
        ///add a file name to the list of files in the ZIP file
        void addDestFile(const QString&fname){if(!mDestFiles.contains(fname))mDestFiles.append(fname);}

#ifdef AURORA_INSTGEN_BUILD
        ///returns the directory where it starts searching for original files
        QString buildBaseDir()const{return mBuildBase;}
        ///returns the directory where to put those files
        QString zipBaseDir()const{return mZipBase;}
        ///returns true if this is a "source" archive spec, i.e. the specification
        ///how to build an archive; returns false if this represents a completed archive
        bool isSource()const{return misSource;}
        ///returns all patterns for matching files
        QList<ArchiveFile>filePatterns()const{return mFiles;}
        ///sets the file size of the archive, so it can be written to the output meta file
        void setZipSize(qint64 s){mzipsize=s;}
        ///sets the SHA256 sum so it can be written
        void setSha256sum(const QString&s){msha256=s;}
        ///resets the platform from "any" to the correct local one
        void resetPlatform();
#endif
        
};

///Encapsulates meta files: XML descriptions of how to generate and/or download packages.
class MetaFile
{
        QDateTime mBuilt,mInstalled;
        QString mVersionH,mVersionM,mBaseUrl,mIndexFile,mFullTargetDir;
        int mPollInterval=-1;
        QList<Archive>mArchives;
        bool mValid=false;
public:
        ///create empty meta file object, not recommended
        MetaFile(){}
        ///copies a meta file object
        MetaFile(const MetaFile&)=default;
        ///expects a file name and tries to read this file as an XML meta file
        MetaFile(const QString&);
        ///copies a meta file object
        MetaFile& operator=(const MetaFile&)=default;
        
        ///returns the time when the distribution described by this file was created
        QDateTime buildTime()const{return mBuilt;}
        ///returns the time when the distribution was installed
        QDateTime installTime()const{return mInstalled;}
        
        ///returns the human readable version
        QString version()const{return mVersionH;}
        ///returns a machine readable version number;
        ///an ASCII comparison of this "number" is used to determine 
        ///whether a newer version is available
        QString machineVersion()const{return mVersionM;}
        
        ///returns the HTTP/FTP URL where to find the index and distribution files
        QString baseUrl()const{return mBaseUrl;}
        ///returns the name of the file containing the index, relative to baseUrl()
        QString indexFile()const{return mIndexFile;}
        
        ///returns the interval in seconds that the program is supposed to poll for updates
        int pollInterval()const{return mPollInterval;}
        
        ///returns the archives that make up the distribution
        const QList<Archive>& archives()const{return mArchives;}
        ///returns the archives that make up the distribution (can be changed)
        QList<Archive>& archives(){return mArchives;}
        
        ///returns the archive matched by name
        Archive archive(const QString&name){
                for(const Archive&a:mArchives)
                        if(a.name()==name)return a;
                return Archive();
        }
        
        ///returns the names of archives that match the local platform
        QStringList localPlatformArchiveNames()const;
        ///returns the names of archives that match the local platform
        QList<Archive> localPlatformArchives()const;
        
        ///returns true if this object represents a valid Meta File
        bool isValid()const{return mValid;}
        
        ///returns the OS+CPU string for this platform
        static QString localPlatform();
        
        ///writes out a fresh meta file;
        ///instgen: writes the file that goes on the web server together with the archives;
        ///library: writes the file that is used to compare the installed version with a new one
        bool writeMetaFile(const QString&filename)const;

#ifdef AURORA_INSTGEN_BUILD
        ///returns a directory where a test installation is made during archive generation
        ///(used by instgen only)
        QString fullTargetDir()const;
#endif
};

//end of namespace
};

using namespace AuroraUpdater;

#endif
