// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_FHELP_H
#define AURORA_FHELP_H

#include <QStringList>
#include <QFileInfo>
#include <QFile>
#include <QCryptographicHash>
// #include <QDebug>

//remove . and .. as well as double-slashes from a path
inline QString normalizePath(QString path)
{
        const bool slash=path.startsWith('/');
        QStringList dl;
        for(const QString&dc:path.replace('\\','/').split("/",Qt::SkipEmptyParts)){
                if(dc==".")continue;
                if(dc==".."){
                        if(dl.size()==0)dl<<dc;
                        else if(dl[dl.size()-1]=="..")dl<<dc;
                        else dl.takeLast();
                }else dl<<dc;
        }
        if(slash)path="/";else path="";
        for(const QString&dc:dl){
                if(path.size()>0 && path!="/")path+="/";
                path+=dc;
        }
        if(path.size()==0)path=".";
        return path;
}

//return the concatenated and normalized form of two pathes
inline QString concatPath(QString p1,QString p2)
{
        if(p1.isEmpty())return normalizePath(p2);
        return normalizePath(p1+"/"+p2);
}

//reads the symlink and returns the relative path to the file it points to
inline QString relativeLinkPath(const QString&symlink)
{
        //the .absoluteFilePath is a work-around for a bug in Qt 5.1:
        // it reads links of relative files as relative to CWD instead of as
        // relative to the file's path
        // see https://bugreports.qt-project.org/browse/QTBUG-7157
        QFileInfo sfi(QFileInfo(symlink).absoluteFilePath());
        if(!sfi.isSymLink())return QString();
        QStringList sl=sfi.absolutePath().split("/",Qt::SkipEmptyParts);
        QStringList dl=sfi.symLinkTarget().split("/",Qt::SkipEmptyParts);
//         qDebug()<<"  --symlink"<<sl;
//         qDebug()<<"   --match-"<<dl;
        //eliminate common parts
        while(sl.size() && dl.size() && sl[0]==dl[0]){
                sl.takeFirst();
                dl.takeFirst();
        }
        //calculate steps down
        QString ret;
        for(int i=0;i<sl.size();i++){
                if(i)ret+="/";
                ret+="..";
        }
        //calculate steps up
        for(const QString&p:dl){
                if(ret.size())ret+="/";
                ret+=p;
        }
//         qDebug()<<"   --result"<<ret;
        return ret;
}

class ArchiveFileInfo
{
        QString msha,mname;
        qint64 msize=0;
public:
        ArchiveFileInfo(){}
        ArchiveFileInfo(const QString&fname)
                :mname(fname)
        {
                QCryptographicHash hash(QCryptographicHash::Sha256);
                QFile fd(fname);
                if(!fd.open(QIODevice::ReadOnly)){
                        return;
                }
                hash.addData(&fd);
                msha=hash.result().toHex().toLower();
                msize=fd.size();
                fd.close();
        }

        bool isValid()const{return !mname.isEmpty() && !msha.isEmpty();}
        QString name()const{return mname;}
        QString sha256sum()const{return msha;}
        qint64 size()const{return msize;}
};

#endif
