// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include "metafile.h"
#include "fhelp.h"

#include <QFile>
#include <QFileInfo>
#include <QProcess>
#include <QDir>

#include <QtXml>
#include <QtGlobal>

static const QString slocalOS =
#ifdef Q_OS_LINUX
        "linux"
#elif defined(Q_OS_WIN)
        "windows"
#else
#warning "Unknown OS, please extend me"
        "unknown"
#endif
;

static const QString slocalCPU =
#ifdef Q_PROCESSOR_X86_32
        "x86"
#elif defined(Q_PROCESSOR_X86_64)
        "x86-64"
#else
#warning "Unknown CPU, please extend me"
        "unknown"
#endif
;

static inline bool matchLocalOS(const QString &os)
{
        if(os==slocalOS || os=="all")return true;
#ifdef AURORA_INSTGEN_BUILD
        if(os=="any")return true;
#endif
        return false;
}

static inline bool matchLocalCPU(const QString &cpu)
{
        if(cpu==slocalCPU || cpu=="all")return true;
#ifdef AURORA_INSTGEN_BUILD
        if(cpu=="any")return true;
#endif
        return false;
}

static inline bool matchLocal(const QString &os,const QString &cpu)
{
        return matchLocalOS(os)&&matchLocalCPU(cpu);
}

#ifdef AURORA_INSTGEN_BUILD
static inline QString createArchiveName(const QString&name,const QString&os, const QString&cpu)
{
        //shortcut
        if(!name.contains('*'))return name;
        //create the replacement for *: os-cpu; any is replaced with the actual os/cpu
        QString ast;
        if(os=="any")ast=slocalOS;
        else ast=os;
        ast+="-";
        if(cpu=="any")ast+=slocalCPU;
        else ast+=cpu;
        return QString(name).replace('*',ast);
}


static inline QString resolveDir(QString dir,const QString&base=".")
{
        if(dir.isEmpty())dir=".";
        //stage 1: resolve pipe
        if(dir.startsWith("|")){
                QProcess prc;
                prc.start(dir.mid(1),QStringList(),QIODevice::ReadOnly);
                if(!prc.waitForFinished()){
                        qDebug()<<"Error: Failed to resolve directory externally.";
                        qDebug()<<"   Command:"<<dir;
                        qDebug()<<"    Result:"<<prc.exitCode()<<prc.readAllStandardError().trimmed();
                        return QString();
                }
                dir=QString::fromLocal8Bit(prc.readAllStandardOutput()).trimmed();
        }
        //stage 2: if absolute - use it, otherwise combine
        if(!QFileInfo(dir).isAbsolute() && !base.isEmpty())dir=base+"/"+dir;
        //stage 3: normalize
        dir=normalizePath(dir);
        //done
        return dir;
}
#endif

QString MetaFile::localPlatform()
{
        return slocalOS+"-"+slocalCPU;
}

Archive::Archive(const QDomElement& ael)
#ifdef AURORA_INSTGEN_BUILD
        :misSource(false)
#endif
{
        mName=ael.attribute("name");
        msha256=ael.attribute("sha256sum");
        mos=ael.attribute("os");
        mcpu=ael.attribute("cpu");
        mzipsize=ael.attribute("size").toLongLong();
        for(const QString&f:ael.text().split("\n",Qt::SkipEmptyParts))
                mDestFiles.append(f.trimmed());
}

#ifdef AURORA_INSTGEN_BUILD
QString getVarContent(const QString &vname)
{
	//time values
	if(vname=="TODAY")return QDateTime::currentDateTime().toString("yyyyMMdd");
	if(vname=="TODAYUTC")return QDateTime::currentDateTimeUtc().toString("yyyyMMdd");
	if(vname=="TIME")return QTime::currentTime().toString("HHmm");
	if(vname=="TIMEUTC")return QDateTime::currentDateTimeUtc().toString("HHmm");
	if(vname=="NOW")return QDateTime::currentDateTime().toString("yyyyMMddHHmm");
	if(vname=="NOWUTC")return QDateTime::currentDateTimeUtc().toString("yyyyMMddHHmm");
	//environment vars
	if(vname.startsWith("ENV:")){
		const QString ename=vname.mid(4);
		if(qEnvironmentVariableIsSet(ename.toLocal8Bit().constData()))
			return QString::fromLocal8Bit(qgetenv(ename.toLocal8Bit().constData()));
		else return QString();
	}
	//TODO: more variables...?
	return QString();
}
void versionReplacement(QString&vstr)
{
	QString out,vname;
	bool isvar=false;
	for(QChar c:vstr){
		if(isvar){
			if(c=='}'){
				isvar=false;
				out+=getVarContent(vname);
				vname.clear();
			}else
				vname+=c;
		}else{
			if(c=='{')isvar=true;
			else out+=c;
		}
	}
	vstr=out;
}
#endif

MetaFile::MetaFile(const QString& fname)
{
        QFile fd(fname);
        if(!fd.open(QIODevice::ReadOnly)){
                qDebug()<<"Unable to open Meta File";
                return;
        }
        QDomDocument doc;
        if(!doc.setContent(&fd)){
                qDebug()<<"Meta File is not XML";
                return;
        }
        QDomElement root=doc.documentElement();
        mValid=root.tagName()=="AuroraInfo";
        QDomElement el=doc.elementsByTagName("CurrentVersion").at(0).toElement();
        mBuilt=QDateTime::fromString(el.attribute("buildDate"),Qt::ISODate);
        mInstalled=QDateTime::fromString(el.attribute("installDate"),Qt::ISODate);
        mVersionH=el.attribute("version");
        mVersionM=el.attribute("mrv");
        el=doc.elementsByTagName("Settings").at(0).toElement();
        mBaseUrl=el.attribute("baseurl");
        mIndexFile=el.attribute("indexfile");
        mPollInterval=el.attribute("pollinterval","-1").toInt();
        mFullTargetDir=el.attribute("fulltargetdir");
        //go through archive sources
        QDomNodeList nl;
#ifdef AURORA_INSTGEN_BUILD
        nl=doc.elementsByTagName("ArchiveSource");
        for(int i=0;i<nl.size();i++){
                el=nl.at(i).toElement();
                if(el.isNull())continue;
                if(!el.hasAttribute("name")){
                        qDebug()<<"Warning: ArchiveSource at line"<<el.lineNumber()<<"column"<<el.columnNumber()<<"has no name, skipping it.";
                        continue;
                }
                const QString aname=el.attribute("name");
                const QString buildbase=resolveDir(el.attribute("buildbase","."));
                const QString zipbase=el.attribute("zipbase",".");
                qDebug()<<"Found Archive Source"<<aname<<"; buildbase:"<<buildbase<<"; zipbase:"<<zipbase;
                //go through platforms, look for match
                QDomNodeList nlp=el.elementsByTagName("Platform");
                for(int j=0;j<nlp.size();j++){
                        QDomElement elp=nlp.at(j).toElement();
                        if(elp.isNull())continue;
                        const QString os=elp.attribute("os","any");
                        const QString cpu=elp.attribute("cpu","any");
                        if(!matchLocal(os,cpu)){
                                qDebug()<<"  Skipping OS"<<os<<"CPU"<<cpu;
                                continue;
                        }
                        qDebug()<<"  Recording OS"<<os<<"CPU"<<cpu;
                        const QString pbuildbase=resolveDir(elp.attribute("buildbase","."),buildbase);
                        const QString pzipbase=resolveDir(elp.attribute("zipbase","."),zipbase);
                        //generate Archive object
                        Archive arc(elp,pbuildbase,pzipbase,aname);
                        qDebug()<<"    adding archive"<<arc.name();
                        mArchives.append(arc);
                }
        }
        versionReplacement(mVersionH);
	versionReplacement(mVersionM);
#endif
        //go through finished archives
        nl=doc.elementsByTagName("Archive");
        for(int i=0;i<nl.size();i++){
                el=nl.at(i).toElement();
                if(el.isNull())continue;
                if(!el.hasAttribute("name")){
                        qDebug()<<"Warning: Archive at line"<<el.lineNumber()<<"column"<<el.columnNumber()<<"has no name, skipping it.";
                        continue;
                }
                mArchives.append(Archive(el));
        }
}

bool MetaFile::writeMetaFile(const QString& filename) const
{
        QFile fd(filename);
        if(!fd.open(QIODevice::WriteOnly|QIODevice::Truncate)){
                qDebug()<<"Unable to write to meta file"<<filename;
                return false;
        }
        //generate XML
        QDomDocument doc;
        QDomElement root=doc.createElement("AuroraInfo");
        //version info
        QDomElement el=doc.createElement("CurrentVersion");
#ifdef AURORA_INSTGEN_BUILD
        //this is instgen - we just built it
        el.setAttribute("buildDate",QDateTime::currentDateTime().toString(Qt::ISODate));
#else
        //this is the installer lib - we just installed a pre-built version
        el.setAttribute("buildDate",mBuilt.toString(Qt::ISODate));
        el.setAttribute("installDate",QDateTime::currentDateTime().toString(Qt::ISODate));
#endif
        el.setAttribute("version",mVersionH);
        el.setAttribute("mrv",mVersionM);
        root.appendChild(el);

        //settings defaults
        el=doc.createElement("Settings");
        el.setAttribute("baseurl",mBaseUrl);
        el.setAttribute("indexfile",mIndexFile);
        el.setAttribute("pollinterval",mPollInterval);
        root.appendChild(el);

        //built archives
        for(const Archive&arc:mArchives){
                //skip non-built archives
                if(arc.sha256sum().isEmpty())continue;
                //store vital info
                el=doc.createElement("Archive");
                el.setAttribute("name",arc.name());
                el.setAttribute("size",arc.zipSize());
                el.setAttribute("sha256sum",arc.sha256sum());
                el.setAttribute("os",arc.operatingSystem());
                el.setAttribute("cpu",arc.cpu());
                //store file names
                QString fs;
                for(const QString&f:arc.destFiles())fs+=f+"\n";
                el.appendChild(doc.createTextNode(fs.trimmed()));
                root.appendChild(el);
        }
        //write to file
        doc.appendChild(root);
        fd.write(doc.toByteArray());
        fd.close();
        return true;
}

#ifdef AURORA_INSTGEN_BUILD

QString MetaFile::fullTargetDir() const
{
        return createArchiveName(mFullTargetDir,slocalOS,slocalCPU);
}

bool ArchiveFile::fileMatches(QString filename) const
{
        filename=normalizePath(filename);
        return mpat.exactMatch(filename) && !mexclude.exactMatch(filename);
}

static inline QStringList listdir(const QString&src,QString dirn=QString())
{
        if(dirn.isEmpty())dirn=".";
        QDir d(src+"/"+dirn);
        QStringList ret;
        for(const QFileInfo&fi:d.entryInfoList(QDir::Dirs|QDir::Files|QDir::NoDotAndDotDot|QDir::Readable)){
                if(fi.isFile())
                        ret.append(dirn+"/"+fi.fileName());
                else
                        ret.append(listdir(src,dirn+"/"+fi.fileName()));
        }
//         qDebug()<<"scanned"<<d.path()<<ret;
        return ret;
}

QStringList ArchiveFile::getFiles() const
{
        //split pattern
        QStringList ret;
        for(QString c:listdir(mSource,".")){
                c=normalizePath(c);
                if(mpat.exactMatch(c) && !mexclude.exactMatch(c))
                        ret.append(c);
        }
        return ret;
}

Archive::Archive(const QDomElement& pfel, const QString& bb, const QString& zb,const QString&nm)
        :mBuildBase(bb),mZipBase(zb),misSource(true)
{
        mos=pfel.attribute("os","any");
        mcpu=pfel.attribute("cpu","any");
        if(nm.contains("*"))mName=createArchiveName(nm,mos,mcpu);
        else mName=nm;
        //files...
        QDomNodeList nl=pfel.elementsByTagName("Files");
        for(int i=0;i<nl.size();i++){
                QDomElement el=nl.at(i).toElement();
                if(el.isNull())continue;
                //bases
                const QString buildbase=resolveDir(el.attribute("buildbase","."),mBuildBase);
                const QString zipbase=resolveDir(el.attribute("zipbase","."),mZipBase);
                const QString exclude=el.attribute("exclude");
                //files
                for(QString fp:el.text().split("\n")){
                        fp=fp.trimmed();
                        if(fp.isEmpty())continue;
                        mFiles.append(ArchiveFile(buildbase,zipbase,fp,exclude));
                }
        }
}

void Archive::resetPlatform()
{
        if(mos=="any")mos=slocalOS;
        if(mcpu=="any")mcpu=slocalCPU;
}

//end of instgen methods
#endif

QStringList MetaFile::localPlatformArchiveNames() const
{
        QStringList ret;
        for(const Archive&a:mArchives)
                if(matchLocal(a.operatingSystem(),a.cpu()))
                        ret.append(a.name());
        return ret;
}

QList< Archive > MetaFile::localPlatformArchives() const
{
        QList<Archive> ret;
        for(const Archive&a:mArchives)
                if(matchLocal(a.operatingSystem(),a.cpu()))
                        ret.append(a);
        return ret;
}
