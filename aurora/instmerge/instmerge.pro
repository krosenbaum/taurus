TEMPLATE = app

TARGET = instmerge
DESTDIR = $$PWD/../../bin
QT -= gui
QT += xml
CONFIG += release hide_symbols separate_debug_info console c++11

#FIXME:
equals(QT_MAJOR_VERSION, 6): QT += core5compat

OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

DEFINES += AURORA_INSTGEN_BUILD=1

INCLUDEPATH += . $$PWD/../common
DEPENDPATH += $$INCLUDEPATH

SOURCES += main.cpp \
        ../common/metafile.cpp

HEADERS += \
        ../common/metafile.h


linux {
  QMAKE_CFLAGS += -fPIE
  QMAKE_CXXFLAGS += -fPIE
  QMAKE_LFLAGS += -pie
  #make sure we find our libs
  QMAKE_LFLAGS += -Wl,-rpath,\'\$$ORIGIN/../lib\'
}
QMAKE_CFLAGS += -fstack-protector-all -Wstack-protector
QMAKE_CXXFLAGS += -fstack-protector-all -Wstack-protector

win32 {
  QMAKE_LFLAGS +=  -Wl,--nxcompat -Wl,--dynamicbase
  LIBS += -lssp
}
