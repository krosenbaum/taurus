// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#include <QCoreApplication>
#include <QStringList>
#include <QDebug>
#include <QDir>
#include <QCryptographicHash>

#include "metafile.h"
#include "zipgen.h"
#include "fhelp.h"

void copyFile(const QString&src,const QString&ffn)
{
        QDir().mkpath(QFileInfo(ffn).path());
        QFileInfo fi(src);
        if(fi.isSymLink()){
                QFile(relativeLinkPath(src)).link(ffn);
        }else{
                QFile(src).copy(ffn);
                QFile(ffn).setPermissions(fi.permissions());
        }
}

void delPath(const QString&p)
{
        QDir d(p);
        if(!d.exists())return;
        d.removeRecursively();
}

void buildArchive(Archive&arc,const QString&fulldir)
{
        if(!arc.isSource()){
                qDebug()<<"Skipping non-source Archive"<<arc.name();
                return;
        }
        qDebug()<<"Building Archive"<<arc.name();
        //create zip file
        ZipGen zip(arc.name());
        //scan source files
        for(const ArchiveFile &af:arc.filePatterns()){
                const QString src=af.sourcePath()+"/";
                const QString dest=af.destinationPath()+"/";
                qDebug()<<"  build src-dir:"<<src<<"; zipdir:"<<dest<<"; pattern:"<<af.pattern().pattern()<<"; exclude:"<<af.excludePattern().pattern();
                for(const QString&fn:af.getFiles()){
                        qDebug()<<"    adding"<<src+fn<<"as"<<dest+fn;
                        arc.addDestFile(dest+fn);
                        //copy to ZIP
                        zip.addFile(src+fn,dest+fn);
                        //copy to test install
                        if(fulldir.isEmpty())continue;
                        copyFile(src+fn,fulldir+"/"+dest+fn);
                }
        }
        //close zip
        zip.close();
        //get checksum
        ArchiveFileInfo zinfo(arc.name());
        if(!zinfo.isValid()){
                qDebug()<<"Ooops! Unable to read my own ZIP file!"<<arc.name();
                return;
        }
        arc.setSha256sum(zinfo.sha256sum());
        arc.setZipSize(zinfo.size());
        //correct local platform
        arc.resetPlatform();
}

int main(int ac,char**av)
{
        QCoreApplication app(ac,av);
        //get parameters
        if(app.arguments().size()!=2){
                qDebug()<<"Usage: instgen metafile.aurora";
                return 1;
        }
        
        //get file
        const QString mfname=app.arguments().at(1);
        QFileInfo inf(mfname);
        if(!inf.exists()){
                qDebug()<<"File"<<mfname<<"does not exist.";
                return 1;
        }
        QDir::setCurrent(inf.absolutePath());
        qDebug()<<"Loading"<<mfname;
        qDebug()<<"=========================\n";
        qDebug()<<"Setting CWD to"<<QDir::currentPath();
        MetaFile mf(inf.fileName());
        if(!mf.isValid()){
                qDebug()<<"File"<<mfname<<"is not a valid Aurora Meta File.";
                return 1;
        }
        
        qDebug()<<"";
        qDebug()<<"Building"<<mfname<<"for platform spec"<<MetaFile::localPlatform()<<"\n";
        
        //actually build
        const QString fulldir=mf.fullTargetDir();
        if(fulldir.isEmpty())
                qDebug()<<"Not performing a test installation.";
        else{
                qDebug()<<"Test installation in"<<fulldir;
                delPath(fulldir);
        }
        for(Archive&arc:mf.archives())
                buildArchive(arc,fulldir);
        
        //write new meta file
        if(mf.writeMetaFile(mf.indexFile())){
                qDebug()<<"You find your new Meta-Infos in"<<mf.indexFile();
                qDebug()<<"Please verify it. If you built several platforms, copy all <Archive> tags into one file.";
                qDebug()<<"Please sign this file using:"<<QString("gpg --detach-sign --armor %1").arg(mf.indexFile());
                qDebug()<<"When done, upload all archives, this file and the signature to"<<mf.baseUrl();
        }
        if(!fulldir.isEmpty()){
                mf.writeMetaFile(mf.fullTargetDir()+"/"+mf.indexFile());
                qDebug()<<"You will find the test installation in:"<<mf.fullTargetDir();
                qDebug()<<"Please tar/zip it for initial installations.";
        }
        return 0;
}