// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//


#include "zipgen.h"
#include "fhelp.h"

#include <unistd.h>

#include <QDebug>
#include <QtXml>

ZipGen::ZipGen(QString filename)
        :QObject(),
         mtgt(filename)
{
        if(mtgt.open(QIODevice::ReadWrite|QIODevice::Truncate))
                mzip.open(&mtgt);
        else
                qWarning()<<"Unable to create ZIP file"<<filename;
}

ZipGen::~ZipGen()
{
        close();
}

void ZipGen::close()
{
        if(mzip.isOpen()){
                writeMetaFile();
                mzip.close();
        }
        if(mtgt.isOpen())
                mtgt.close();
}

bool ZipGen::addFile(const QString& sourcefile, const QString& destfile)
{
        if(!mzip.isOpen())return false;
        //check file exists
        QFileInfo info(sourcefile);
        //is it a symlink?
        if(info.isSymLink()){
                addSymLink(destfile,relativeLinkPath(sourcefile));
                return true;
        }
        //normal file? store.
        if(!info.isFile())return false;
        QFile fd(sourcefile);
        if(!fd.open(QIODevice::ReadOnly))
                return false;
        const bool r=mzip.storeFile(destfile,fd,info.birthTime());
        //store permissions
        setPermissions(destfile,info.permissions());
        return r;
}

void ZipGen::addSymLink(const QString& destfile, const QString& target)
{
        msymlinks.insert(destfile,target);
}

void ZipGen::setPermissions(const QString& destfile, QFileDevice::Permissions perms)
{
        mperms.insert(destfile,perms);
}

void ZipGen::writeMetaFile()
{
        //IMPORTANT NOTE: if you change this method, make sure you also change
        // the dloader/unzipper.cpp version of readMetaFile

        //only if we have a ZIP
        if(!mzip.isOpen())return;
        //only if we have to say something
        //if(msymlinks.size()==0 && mperms.size()==0)return;
        //create XML form
        QDomDocument doc;
        QDomElement root=doc.createElement("AuroraFileMetaData");
        for(const QString&fn:msymlinks.keys()){
                QDomElement sym=doc.createElement("SymLink");
                sym.setAttribute("file",fn);
                sym.setAttribute("target",msymlinks[fn]);
                root.appendChild(sym);
        }
        for(const QString&fn:mperms.keys()){
                QDomElement prm=doc.createElement("Perm");
                prm.setAttribute("file",fn);
                prm.setAttribute("perm",(int)mperms[fn]);
                root.appendChild(prm);
        }
        doc.appendChild(root);
        mzip.storeFile(".aurorameta",doc.toByteArray());
}
