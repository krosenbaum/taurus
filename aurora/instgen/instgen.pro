TEMPLATE = app

TARGET = instgen
DESTDIR = $$PWD/../../bin
QT -= gui
QT += xml
CONFIG += release hide_symbols separate_debug_info console

#FIXME:
equals(QT_MAJOR_VERSION, 6): QT += core5compat

include ($$PWD/../../zip.pri)

OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp

DEFINES += AURORA_INSTGEN_BUILD=1

INCLUDEPATH += $$PWD/../common
DEPENDPATH += $$INCLUDEPATH

SOURCES += main.cpp \
        zipgen.cpp \
        ../common/metafile.cpp

HEADERS += \
        zipgen.h \
        ../common/metafile.h


include (../../zip.pri)

gcc {
  QMAKE_CXXFLAGS += -std=gnu++11
}

linux {
  QMAKE_CFLAGS += -fPIE
  QMAKE_CXXFLAGS += -fPIE
  QMAKE_LFLAGS += -pie
  #make sure we find our libs
  QMAKE_LFLAGS += -Wl,-rpath,\'\$$ORIGIN/../lib\'
}
QMAKE_CFLAGS += -fstack-protector-all -Wstack-protector
QMAKE_CXXFLAGS += -fstack-protector-all -Wstack-protector

win32 {
  QMAKE_LFLAGS +=  -Wl,--nxcompat -Wl,--dynamicbase
  LIBS += -lssp
}
