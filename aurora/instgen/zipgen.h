// Copyright (C) 2013 by Konrad Rosenbaum <konrad@silmor.de>
// protected under the GNU LGPL version 3 or at your option any newer.
// See COPYING.LGPL file that comes with this distribution.
//

#ifndef AURORA_ZIPGEN_H
#define AURORA_ZIPGEN_H

#include <QObject>
#include <QFile>
#include <QMap>

#include <Zip>

class ZipGen:public QObject
{
        Q_OBJECT
public:
        ZipGen(QString filename);
        ~ZipGen();
        
public slots:
        bool addFile(const QString&sourcefile,const QString&destfile);
        
        void addSymLink(const QString&destfile,const QString&target);
        void setPermissions(const QString&destfile,QFile::Permissions);
        
        void close();
private:
        QMap<QString,QString>msymlinks;
        QMap<QString,QFile::Permissions>mperms;
        QtZipHelper::Zip mzip;
        QFile mtgt;
        
        void writeMetaFile();
};

#endif
