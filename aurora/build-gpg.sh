#!/bin/bash
set -e

fmake(){
        echo -e 'all:\n\techo ok\n\n%:\n\techo ok $@\n\n' >./$1/Makefile
}

test -f /proc/cpuinfo && {
  CPUS=$(expr $(grep ^processor </proc/cpuinfo |tail -1 |cut -d : -f 2) + 2)
  export MAKEFLAGS="-j $CPUS"
  echo Using $CPUS CPUs for Make.
}

echo Cleaning up old binaries
rm -rf gpg

cd `dirname $0`
cd gnupg
echo Building GnuPG
git clean -dfx
git reset --hard
./autogen.sh
./configure --disable-gnupg-iconv --enable-minimal \
  --disable-card-support --disable-bzip2 --disable-exec --disable-keyserver-helpers \
  --disable-nls --disable-asm --disable-rpath  --disable-regex --with-included-zlib \
  --with-libcurl=no --disable-threads --without-libiconv-prefix --without-libintl-prefix \
  --with-libusb=no --with-readline=no --enable-rsa \
  --prefix=`pwd`/../gpg
fmake doc
nice make
make install

cd ..
#clean up and optimization
strip gpg/bin/* || true
echo
echo Done.
