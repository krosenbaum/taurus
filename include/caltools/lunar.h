// Calculating Easter Dates
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#ifndef CALTOOLS_LUNAR_H
#define CALTOOLS_LUNAR_H

#include <QDate>

namespace CalTools{

///Phases of the Moon (note: this only applies to earth's moon, not to Tatooine, Europa, Io or any other moon)
enum class LunarPhase {
	///New Moon, not visible with the naked eye
	NewMoon=0,
	///getting bigger, it's a crescent
	Waxing25=1,
	///getting bigger, it's a half moon
	Waxing50=2,
	///getting bigger, it's almost full
	Waxing75=3,
	///getting bigger, it's a crescent
	WaxingCrescent=1,
	///getting bigger, it's a half moon
	WaxingHalf=2,
	///getting bigger, it's almost full
	WaxingGibbous=3,
	///getting bigger, it's a half moon, first quarter of the full cycle
	FirstQuarter=2,
	///full moon
	FullMoon=4,
	///getting smaller, it's almost full
	Waning75=5,
	///getting smaller, it's a half moon
	Waning50=6,
	///getting smaller, it's a crescent
	Waning25=7,
	///getting smaller, it's almost full
	WaningGibbous=5,
	///getting smaller, it's a half moon
	WaningHalf=6,
	///getting smaller, it's a crescent
	WaningCrescent=7,
	///getting smaller, it's a half moon, third quarter of the full cycle
	ThirdQuarter=6,
};

#ifndef CALTOOLS_EXPORT
#define CALTOOLS_EXPORT Q_DECL_IMPORT
#endif

///returns the phase of the moon for a specific date
/// note: this function is quite 
LunarPhase lunarPhaseForDate(QDate d);

//end of namespace
}

#endif
