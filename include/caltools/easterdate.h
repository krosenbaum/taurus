// Calculating Easter Dates
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#ifndef CALTOOLS_EASTER_H
#define CALTOOLS_EASTER_H

#include <QDate>

#if 0
#include <QDebug>
#define DD(x) qDebug()<<""#x "="<<x;
#else
#define DD(x)
#endif

#ifndef CALTOOLS_EXPORT
#define CALTOOLS_EXPORT Q_DECL_IMPORT
#endif


// formulas were found at http://www.dateofeaster.com/
// some more explanations (in German): https://de.wikipedia.org/wiki/Osterzyklus#Die_Mondgleichung
//   and: https://de.wikipedia.org/wiki/Gau%C3%9Fsche_Osterformel

namespace CalTools {

///calculates the date of easter for a specific year
QDate CALTOOLS_EXPORT easterSunday(quint16 year);

///calculates the eastern orthodox date for Easter sunday
QDate CALTOOLS_EXPORT orthodoxEasterSunday(quint16 year);

//TODO: date of jewish pessach


//end of namespace
}

#undef DD

#endif
