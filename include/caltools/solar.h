// Calculating Sun phases
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#ifndef CALTOOLS_SOLAR_H
#define CALTOOLS_SOLAR_H

#include <QDateTime>

namespace CalTools {

//TODO: calculate sunrise/sundown
// https://en.wikipedia.org/wiki/Sunrise_equation

#ifndef CALTOOLS_EXPORT
#define CALTOOLS_EXPORT Q_DECL_IMPORT
#endif

///Represents geographical coordinates.
class CALTOOLS_EXPORT GeoCoord
{
	double mwest=1000,mnorth=0;
public:
	///Instantiates an invalid coordinate instance.
	GeoCoord(){}
	///Instantiates a coordinate instance.
	///\param north - the latitude in degrees north, negative values are south (-90 .. 0 .. 90)
	///\param west - the longitude in degrees west, negative values are east (-180 .. 0 .. 180)
	GeoCoord(double north,double west):mwest(west),mnorth(north){/*for the corrections*/ moveWest(0);moveNorth(0);}
	///Copies the instance.
	GeoCoord(const GeoCoord&)=default;
	///Moves the instance.
	GeoCoord(GeoCoord&&)=default;
	
	///Copies the instance.
	GeoCoord& operator=(const GeoCoord&)=default;
	///Moves the instance.
	GeoCoord& operator=(GeoCoord&&)=default;
	
	///returns true if the coordinates are valid
	bool isValid()const{return mwest<=180 && mwest>=-180 && mnorth<=90 && mnorth>=-90;}
	///returns the longitude in degrees west (negative is east)
	double degWest()const{return mwest;}
	///returns the longitude in degrees east (negative is west)
	double degEast()const{return -mwest;}
	///returns the latitude in degrees north (negative is south)
	double degNorth()const{return mnorth;}
	///returns the latitude in degrees south (negative is north)
	double degSouth()const{return -mnorth;}
	
	///moves the coordinates further west
	void moveWest(double);
	///moves the coordinates further east
	void moveEast(double);
	///moves the coordinates further north (can wrap around beyond the north pole)
	void moveNorth(double);
	///moves the coordinates further south (can wrap around beyond the south pole)
	void moveSouth(double);
};

///Calculator for sunset and sunrise
class CALTOOLS_EXPORT SolarInfo
{
public:
	///Instantiates an invalid instance that will return invalid times for all values.
	SolarInfo(){}
	///Instantiates an instance for the given date and coordinates on earth.
	SolarInfo(QDate date,GeoCoord geo);
	
	///copies the instance
	SolarInfo(const SolarInfo&)=default;
	///moves the instance
	SolarInfo(SolarInfo&&)=default;
	
	///copies the instance
	SolarInfo& operator=(const SolarInfo&)=default;
	///moves the instance
	SolarInfo& operator=(SolarInfo&&)=default;
	
	///returns true if the solar calculator has valid date and coordinates
	bool isValid()const{return mdate.isValid();}
	
	///What exact part of twilight we are calculating.
	enum class TwilightType {
		///Begin of sunset or sunrise - when the sun touches the horizon.
		Begin,
		///End of civil twilight - the sun is 6 degrees below the horizon.
		Civil,
		///End of nautic twilight - the sun is 12 degrees below the horizon.
		Nautical,
		///End of astronomical twilight - the sun is 18 degrees below the horizon.
		Astronomical
	};
	
	///Calculate sunrise.
	///\param type the phase of sunrise that will be calculated
	///\returns the time of the sunrise phase in UTC
	QDateTime sunrise(TwilightType type=TwilightType::Begin)const;

	///Calculate sunset.
	///\param type the phase of sunset that will be calculated
	///\returns the time of the sunset phase in UTC
	QDateTime sunset(TwilightType type=TwilightType::Begin)const;
	
	///\returns The time of highest sun (zenith).
	QDateTime highNoon()const{return QDateTime(mdate,mmidday,Qt::UTC);}
	
private:
	///the date for which we calculate times
	QDate mdate;
	///the time at which the sun is at the zenith in UTC
	QTime mmidday;
	///time differences in fractions of a day
	double mbegin=0,mcivil=0,mnautic=0,mastro=0;
	///geographical coordinates
	GeoCoord mcoord;
	
	///calculate a time relative to the calculated zenith
	QDateTime gentime(double diff)const;
};

//end of namespace
}

#endif
