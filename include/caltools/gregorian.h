// Calculating various aspects of the Gregorian calendar
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#ifndef CALTOOLS_GREGORIAN_H
#define CALTOOLS_GREGORIAN_H

#include <QDate>

namespace CalTools{

// https://en.wikipedia.org/wiki/Leap_year
/// checks for leap years (works only within the Gregorian Calendar!)
static inline
bool isLeapYear(quint16 year)
{
	if(year%4 == 0){
		if(year%100 == 0){
			if(year%400 == 0)return true;
			return false;
		}
		return true;
	}
	return false;
}

///returns how many days there are in a specific month
static inline
quint8 numDaysInMonth(quint16 year,quint8 month)
{
	switch(month){
		case 1:case 3:case 5:case 7:
		case 8:case 10:case 12:
			return 31;
		case 4:case 6:case 9:case 11:
			return 30;
		case 2:
			return 28+(isLeapYear(year)?1:0);
		default:
			return 0;
	}
}

///finds the first instance of a week day in a month
/// \param year the Year in which to search
/// \param month the Month in which to search
/// \param dayOfWeek the week day to look for: 0/7 is sunday (\see QDate)
static inline
QDate firstWeekDayInMonth(quint16 year,quint8 month,quint8 dayOfWeek=0)
{
	if(dayOfWeek==0)dayOfWeek=7;
	if(dayOfWeek>7)return QDate();
	//check first day
	QDate d(year,month,1);
	int dw=d.dayOfWeek();
	if(dw==dayOfWeek)return d;
	//move on
	if(dayOfWeek<dw)dayOfWeek+=7;
	return d.addDays(dayOfWeek-dw);
}

///finds how many instances of a specific weekday there are in a month
/// \param year the Year in which to search
/// \param month the Month in which to search
/// \param dayOfWeek the week day to look for: 0/7 is sunday (\see QDate)
static inline
int numWeekDaysInMonth(quint16 year,quint8 month,quint8 dayOfWeek=0)
{
	if(dayOfWeek==0)dayOfWeek=7;
	if(dayOfWeek>7)return 0;
	//get first day of that type, then calculate
	int d=firstWeekDayInMonth(year,month,dayOfWeek).day();
	return (numDaysInMonth(year,month)-d)/7 + 1;
}

///finds the last instance of a week day in a month
/// \param year the Year in which to search
/// \param month the Month in which to search
/// \param dayOfWeek the week day to look for: 0/7 is sunday (\see QDate)
static inline
QDate lastWeekDayInMonth(quint16 year,quint8 month,quint8 dayOfWeek=0)
{
	if(dayOfWeek==0)dayOfWeek=7;
	if(dayOfWeek>7)return QDate();
	//check last day
	QDate d(year,month,numDaysInMonth(year,month));
	int dw=d.dayOfWeek();
	if(dw==dayOfWeek)return d;
	if(dw<dayOfWeek)dw+=7;
	return d.addDays(dayOfWeek-dw);
}

///find the first instance of a specific week day after a given date
/// \param date the date from which to search
/// \param dayOfWeek the week day to look for (default is sunday)
/// \param inclusive if true date is returned if it is the week day in question, otherwise a date 7 days later
static inline
QDate findDayAfter(const QDate &date,quint8 dayOfWeek=0,bool inclusive=false)
{
	if(dayOfWeek==0)dayOfWeek=7;
	if(dayOfWeek>7)return QDate();
	//same one?
	int dw=date.dayOfWeek();
	if(inclusive && dw==dayOfWeek)return date;
	if(dayOfWeek<=dw)dayOfWeek+=7;
	return date.addDays(dayOfWeek-dw);
}

///find the first instance of a specific week day before a given date
/// \param date the date from which to search
/// \param dayOfWeek the week day to look for (default is sunday)
/// \param inclusive if true date is returned if it is the week day in question, otherwise a date 7 days earlier
static inline
QDate findDayBefore(QDate date,quint8 dayOfWeek=0,bool inclusive=false)
{
	if(dayOfWeek==0)dayOfWeek=7;
	if(dayOfWeek>7)return QDate();
	//same one?
	int dw=date.dayOfWeek();
	if(inclusive && dw==dayOfWeek)return date;
	if(dayOfWeek>=dw)dw+=7;
	return date.addDays(dayOfWeek-dw);
}

//end of namespace
}

#endif
