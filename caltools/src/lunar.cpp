// Calculating Easter Dates
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#include <lunar.h>
#include <math.h>

namespace CalTools {

/// amount of phases used for internal calculations
const int LunarPhases_CycleLength=8;

/// average length of a lunar cycle in solar days
const double LunarCycleLengthInDays=29.530588853l;

/// Julian day of a known new moon
const int JulianDayJan1st1900=2415021;

///returns the phase of the moon for a specific date
/// note: this function is quite 
LunarPhase lunarPhaseForDate(QDate d)
{
	//calculate where in the cycle we are (in days)
	double phase=fmod(d.toJulianDay()-JulianDayJan1st1900+1.0, LunarCycleLengthInDays);
	if(phase<0.0)phase+=LunarCycleLengthInDays;
	//convert to enum base
	int p=phase*double(LunarPhases_CycleLength)/LunarCycleLengthInDays;
	//this should not be necessary, but you never know how screwed the FPU is...
	if(p<0)p=0;
	if(p>=LunarPhases_CycleLength)p=LunarPhases_CycleLength-1;
	return LunarPhase(p);
}


}
