// Calculating Sun phases
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#include <solar.h>

#include <math.h>


void CalTools::GeoCoord::moveWest(double w)
{
	mwest+=w;
	while(mwest>180)mwest-=360;
	while(mwest<-180)mwest+=360;
}

void CalTools::GeoCoord::moveEast(double e){moveWest(-e);}

void CalTools::GeoCoord::moveNorth(double n)
{
	mnorth+=n;
	while(mnorth>180)mnorth-=360;
	while(mnorth<-180)mnorth+=360;
	if(mnorth>90){
		moveWest(180);
		mnorth=180-mnorth;
	}
	if(mnorth<-90){
		moveWest(180);
		mnorth=-180-mnorth;
	}
}

void CalTools::GeoCoord::moveSouth(double s){moveNorth(-s);}

static inline double deg2rad(double d){return d*M_PI/180.;}
static inline double rad2deg(double r){return r*180./M_PI;}

//argument of perihelion
static inline double aperi(double n){return 102.9372 + 0.3179526*n/36525.;}

CalTools::SolarInfo::SolarInfo(QDate date,GeoCoord geo)
{
	if(!date.isValid() || !geo.isValid())return;
	mcoord=geo;
	
	// /////
	// section: calculate midday UTC
	
	// day since 2000-1-1 plus slight correction
	double n=date.toJulianDay() - 2451545.0 + 0.0008;
	// mean solar noon
	double J = n - mcoord.degWest()/360.;
	// solar mean anomaly
	double M = fmod((357.5291 + .98560028 * J), 360.);
	// equation of the center
	double C = 1.9148*sin(deg2rad(M)) + 0.02*sin(2*deg2rad(M)) + 0.0003*sin(3*deg2rad(M));
	// ecliptic longitude
	double lambda = fmod((M + C + 180. + aperi(n)), 360.);
	// solar transit
	double Jtrans=2451545.5 + J + 0.0053*sin(deg2rad(M)) - 0.0069*sin(deg2rad(2*lambda));
	// convert to date and time
	mdate=QDate::fromJulianDay(floor(Jtrans));
	double t=Jtrans - floor(Jtrans);
	mmidday=QTime::fromMSecsSinceStartOfDay(t*24*3600*1000);
	
	// /////
	// section: calculate twilight diffs
	auto calcdiff=[&](double angle)->double{
		//declination of the sun
		double sindelta=sin(deg2rad(lambda))*sin(deg2rad(23.44));
		double delta=rad2deg(asin(sindelta));
		//hour angle
		double cosomega=(sin(deg2rad(angle))-sin(deg2rad(mcoord.degNorth()))*sindelta) / (cos(deg2rad(mcoord.degNorth()))*cos(deg2rad(delta)));
		double omega=acos(cosomega);
		//calculate difference
		return omega/2./M_PI;
	};
	mbegin=calcdiff(0.83);
	mcivil=calcdiff(6.0);
	mnautic=calcdiff(12.0);
	mastro=calcdiff(18.0);
}

QDateTime CalTools::SolarInfo::sunrise(TwilightType type)const
{
	switch(type){
		case TwilightType::Begin:return gentime(-mbegin);
		case TwilightType::Civil:return gentime(-mcivil);
		case TwilightType::Nautical:return gentime(-mnautic);
		case TwilightType::Astronomical:return gentime(-mastro);
		//should not happen:
		default:return QDateTime();
	}
}

QDateTime CalTools::SolarInfo::sunset(TwilightType type)const
{
	switch(type){
		case TwilightType::Begin:return gentime(mbegin);
		case TwilightType::Civil:return gentime(mcivil);
		case TwilightType::Nautical:return gentime(mnautic);
		case TwilightType::Astronomical:return gentime(mastro);
		//should not happen:
		default:return QDateTime();
	}
}

QDateTime CalTools::SolarInfo::gentime(double diff)const
{
	if(!mdate.isValid() || isnan(diff) || isinf(diff))
		return QDateTime();

	return highNoon().addSecs(diff*24*3600);
}
