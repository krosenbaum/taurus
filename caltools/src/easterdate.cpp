// Calculating Easter Dates
//
// (c) Konrad Rosenbaum, 2017
// protected under the GNU LGPL v3 or at your option any newer

#include <easterdate.h>

// Debugger function, make if "#if 1" to enable debugging output
#if 0
#include <QDebug>
#define DD(x) qDebug()<<""#x "="<<x;
#else
#define DD(x)
#endif

// formulas were found at http://www.dateofeaster.com/
// some more explanations (in German): https://de.wikipedia.org/wiki/Osterzyklus#Die_Mondgleichung
//   and: https://de.wikipedia.org/wiki/Gau%C3%9Fsche_Osterformel

namespace CalTools {

///calculates the date of easter for a specific year
QDate easterSunday(quint16 year)
{
	DD(year);
	const int c=year/100;		// century
	DD(c);
	const int q=(c-15)*3/4+10;	// gregorian correction
	DD(q);
	const int l=7-((year/4+year+4-q)%7); // dominical number
	DD(l);
	const int g=year%19+1;		// golden number
	DD(g);
	const int j=(g*11-10)%30;	// julian epact
	DD(j);
	const int s=q-10;		// solar equation
	DD(s);
	const int m=(c-14)*8/25;	//lunar equation
	DD(m);
	int e=j-s+m;
	while(e<=0)e+=30;
	e%=30;				// epact
	DD(e);
	if(g>11 && e==25)e=26;		// second epact 25 rule
	DD(e);
	if(e==24)e=25;			// epact 24 equals epact 25
	DD(e);
	int d;
	if(e<24)d=44-e;			// paschal full moon
	else d=74-e;
	DD(d);
	
	d+= 7 - ((d+10-l)%7); 		// find sunday following this
	DD(d);
	int mo=3; //
	if(d>61){mo=5;d-=61;}		// may
	else if(d>31){mo=4;d-=31;}	// april
	return QDate(year,mo,d);
}

///calculates the eastern orthodox date for Easter sunday
QDate orthodoxEasterSunday(quint16 year)
{
	DD(year);
	const int l=7-(year/4+year+4)%7; //dominical
	DD(l);
	const int g=year%19+1;		//gregorian
	DD(g);
	const int e=(g*11-11)%30;	//epact
	DD(e);
	const int d=(e>16)?(66-e):(36-e); // paschal
	DD(d);
	const int w=(d+3-l)%7;		// weekday of paschal
	DD(w);
	int d2=d+7-w;			// sunday after paschal in julian calendar
	DD(d2);
	const int q=(year/100-15)*3/4+10; // gregorian correction
	DD(q);
	d2+=q;
	DD(d2);
	
	int mo=3;
	if(d2>61){mo=5;d2-=61;}
	else if(d2>31){mo=4;d2-=31;}
	return QDate(year,mo,d2);
}

//TODO: date of jewish pessach


//end of namespace
}

#undef DD
