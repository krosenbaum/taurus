TEMPLATE = lib
TARGET = caltools
VERSION =
QT -= gui

DESTDIR = $$PWD/../../lib

INCLUDEPATH += ../../include/caltools

DEFINES += CALTOOLS_EXPORT=Q_DECL_EXPORT

SOURCES += lunar.cpp easterdate.cpp solar.cpp
