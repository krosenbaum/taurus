#include "gregorian.h"
#include <QTest>

using namespace CalTools;

class TClass:public QObject
{
	Q_OBJECT
private slots:
	void leapyear();
	void daysInMonth();
	void weekDaysInMonth();
	void firstDay();
	void lastDay();
	void dayAfter();
	void dayBefore();
};

#include "gregtest.moc"

void TClass::leapyear()
{
	QCOMPARE(isLeapYear(1904),true);
	QCOMPARE(isLeapYear(2004),true);
	QCOMPARE(isLeapYear(2000),true);
	QCOMPARE(isLeapYear(1901),false);
	QCOMPARE(isLeapYear(1902),false);
	QCOMPARE(isLeapYear(1903),false);
	QCOMPARE(isLeapYear(1900),false);
	QCOMPARE(isLeapYear(1800),false);
}

void TClass::daysInMonth()
{
	QCOMPARE(numDaysInMonth(2000,1),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,2),(quint8)29);
	QCOMPARE(numDaysInMonth(2001,2),(quint8)28);
	QCOMPARE(numDaysInMonth(2000,3),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,4),(quint8)30);
	QCOMPARE(numDaysInMonth(2000,5),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,6),(quint8)30);
	QCOMPARE(numDaysInMonth(2000,7),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,8),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,9),(quint8)30);
	QCOMPARE(numDaysInMonth(2000,10),(quint8)31);
	QCOMPARE(numDaysInMonth(2000,11),(quint8)30);
	QCOMPARE(numDaysInMonth(2000,12),(quint8)31);
}

void TClass::weekDaysInMonth()
{
	QCOMPARE(numWeekDaysInMonth(2017,5),4);
	QCOMPARE(numWeekDaysInMonth(2017,5,0),4);
	QCOMPARE(numWeekDaysInMonth(2017,5,7),4);
	QCOMPARE(numWeekDaysInMonth(2017,5,1),5);
	QCOMPARE(numWeekDaysInMonth(2017,5,2),5);
	QCOMPARE(numWeekDaysInMonth(2017,5,3),5);
	QCOMPARE(numWeekDaysInMonth(2017,5,4),4);
	QCOMPARE(numWeekDaysInMonth(2017,5,5),4);
	QCOMPARE(numWeekDaysInMonth(2017,5,6),4);
}

void TClass::firstDay()
{
	QCOMPARE(firstWeekDayInMonth(2017,5,1),QDate(2017,5,1));
	QCOMPARE(firstWeekDayInMonth(2017,5,2),QDate(2017,5,2));
	QCOMPARE(firstWeekDayInMonth(2017,5,3),QDate(2017,5,3));
	QCOMPARE(firstWeekDayInMonth(2017,5,4),QDate(2017,5,4));
	QCOMPARE(firstWeekDayInMonth(2017,5,5),QDate(2017,5,5));
	QCOMPARE(firstWeekDayInMonth(2017,5,6),QDate(2017,5,6));
	QCOMPARE(firstWeekDayInMonth(2017,5,7),QDate(2017,5,7));
	QCOMPARE(firstWeekDayInMonth(2017,5,0),QDate(2017,5,7));

	QCOMPARE(firstWeekDayInMonth(2017,6,4),QDate(2017,6,1));
	QCOMPARE(firstWeekDayInMonth(2017,6,5),QDate(2017,6,2));
	QCOMPARE(firstWeekDayInMonth(2017,6,6),QDate(2017,6,3));
	QCOMPARE(firstWeekDayInMonth(2017,6,7),QDate(2017,6,4));
	QCOMPARE(firstWeekDayInMonth(2017,6,0),QDate(2017,6,4));
	QCOMPARE(firstWeekDayInMonth(2017,6,1),QDate(2017,6,5));
	QCOMPARE(firstWeekDayInMonth(2017,6,2),QDate(2017,6,6));
	QCOMPARE(firstWeekDayInMonth(2017,6,3),QDate(2017,6,7));

	QCOMPARE(firstWeekDayInMonth(2017,6,33),QDate());
}

void TClass::lastDay()
{
	QCOMPARE(lastWeekDayInMonth(2017,5,1),QDate(2017,5,29));
	QCOMPARE(lastWeekDayInMonth(2017,5,2),QDate(2017,5,30));
	QCOMPARE(lastWeekDayInMonth(2017,5,3),QDate(2017,5,31));//
	QCOMPARE(lastWeekDayInMonth(2017,5,4),QDate(2017,5,25));
	QCOMPARE(lastWeekDayInMonth(2017,5,5),QDate(2017,5,26));
	QCOMPARE(lastWeekDayInMonth(2017,5,6),QDate(2017,5,27));
	QCOMPARE(lastWeekDayInMonth(2017,5,7),QDate(2017,5,28));
	QCOMPARE(lastWeekDayInMonth(2017,5,0),QDate(2017,5,28));

	QCOMPARE(lastWeekDayInMonth(2017,5,10),QDate());
}

void TClass::dayAfter()
{
	const QDate d(2017,5,17);
	QCOMPARE(findDayAfter(d),QDate(2017,5,21));
	QCOMPARE(findDayAfter(d,0),QDate(2017,5,21));
	QCOMPARE(findDayAfter(d,7),QDate(2017,5,21));
	QCOMPARE(findDayAfter(d,0,false),QDate(2017,5,21));
	QCOMPARE(findDayAfter(d,0,false),QDate(2017,5,21));
	
	QCOMPARE(findDayAfter(d,1,false),QDate(2017,5,22));
	QCOMPARE(findDayAfter(d,2,false),QDate(2017,5,23));
	QCOMPARE(findDayAfter(d,3,false),QDate(2017,5,24));
	QCOMPARE(findDayAfter(d,4,false),QDate(2017,5,18));
	QCOMPARE(findDayAfter(d,5,false),QDate(2017,5,19));
	QCOMPARE(findDayAfter(d,6,false),QDate(2017,5,20));
	QCOMPARE(findDayAfter(d,7,false),QDate(2017,5,21));

	QCOMPARE(findDayAfter(d,1,true),QDate(2017,5,22));
	QCOMPARE(findDayAfter(d,2,true),QDate(2017,5,23));
	QCOMPARE(findDayAfter(d,3,true),QDate(2017,5,17));
	QCOMPARE(findDayAfter(d,4,true),QDate(2017,5,18));
	QCOMPARE(findDayAfter(d,5,true),QDate(2017,5,19));
	QCOMPARE(findDayAfter(d,6,true),QDate(2017,5,20));
	QCOMPARE(findDayAfter(d,7,true),QDate(2017,5,21));

	QCOMPARE(findDayAfter(d,9,false),QDate());
}

void TClass::dayBefore()
{
	const QDate d(2017,5,17);
	QCOMPARE(findDayBefore(d),QDate(2017,5,14));
	QCOMPARE(findDayBefore(d,0),QDate(2017,5,14));
	QCOMPARE(findDayBefore(d,7),QDate(2017,5,14));
	QCOMPARE(findDayBefore(d,0,false),QDate(2017,5,14));
	QCOMPARE(findDayBefore(d,0,false),QDate(2017,5,14));
	
	QCOMPARE(findDayBefore(d,1,false),QDate(2017,5,15));
	QCOMPARE(findDayBefore(d,2,false),QDate(2017,5,16));
	QCOMPARE(findDayBefore(d,3,false),QDate(2017,5,10));
	QCOMPARE(findDayBefore(d,4,false),QDate(2017,5,11));
	QCOMPARE(findDayBefore(d,5,false),QDate(2017,5,12));
	QCOMPARE(findDayBefore(d,6,false),QDate(2017,5,13));
	QCOMPARE(findDayBefore(d,7,false),QDate(2017,5,14));

	QCOMPARE(findDayBefore(d,1,true),QDate(2017,5,15));
	QCOMPARE(findDayBefore(d,2,true),QDate(2017,5,16));
	QCOMPARE(findDayBefore(d,3,true),QDate(2017,5,17));
	QCOMPARE(findDayBefore(d,4,true),QDate(2017,5,11));
	QCOMPARE(findDayBefore(d,5,true),QDate(2017,5,12));
	QCOMPARE(findDayBefore(d,6,true),QDate(2017,5,13));
	QCOMPARE(findDayBefore(d,7,true),QDate(2017,5,14));

	QCOMPARE(findDayBefore(d,9,false),QDate());
}

QTEST_MAIN(TClass)