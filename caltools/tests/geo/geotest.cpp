#include <solar.h>

#include <QTest>


using namespace CalTools;

class GT:public QObject{
	Q_OBJECT
private slots:
	void validity();
	void moveLatitude();
	void moveLongitude();
};

void GT::validity()
{
	GeoCoord c1;
	QVERIFY(!c1.isValid());
	GeoCoord c2(10,30);
	QVERIFY(c2.isValid());
	c2.moveWest(200);
	QVERIFY(c2.isValid());
	c2.moveNorth(100);
	QVERIFY(c2.isValid());
}

void GT::moveLatitude()
{
	GeoCoord c1(10,30);
	c1.moveNorth(10);
	QCOMPARE(c1.degNorth(),20);
}
void GT::moveLongitude(){}

QTEST_MAIN(GT)