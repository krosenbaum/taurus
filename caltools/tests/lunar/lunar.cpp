#include "lunar.h"
#include <QTest>

using namespace CalTools;

class TClass:public QObject
{
	Q_OBJECT
private slots:
	void lunarPhases();
};

#include "lunar.moc"

void TClass::lunarPhases()
{
	QCOMPARE(lunarPhaseForDate(QDate(2017,4,26)),LunarPhase::NewMoon);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,1)),LunarPhase::Waxing25);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,3)),LunarPhase::Waxing50);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,6)),LunarPhase::Waxing75);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,10)),LunarPhase::FullMoon);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,16)),LunarPhase::Waning75);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,19)),LunarPhase::Waning50);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,21)),LunarPhase::Waning25);
	QCOMPARE(lunarPhaseForDate(QDate(2017,5,25)),LunarPhase::NewMoon);
}

QTEST_MAIN(TClass)