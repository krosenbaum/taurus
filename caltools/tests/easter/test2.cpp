#include "easterdate.h"
#include <QDebug>
#include <QTest>


class ED{
public:
	ED(quint16 y,quint8 m1,quint8 d1,quint8 m2=0,quint8 d2=0){year=y;wmonth=m1;wday=d1;emonth=m2?m2:m1;eday=d2?d2:d1;}
	ED(){year=0;wmonth=wday=emonth=eday=0;}
	ED(const ED&)=default;
	
	inline bool isWest(QDate d)const{return d.year()==year && d.month()==wmonth && d.day()==wday;}
	inline bool isEast(QDate d)const{return d.year()==year && d.month()==emonth && d.day()==eday;}
	
	inline QDate west()const{return QDate(year,wmonth,wday);}
	inline QDate east()const{return QDate(year,emonth,eday);}
	
	quint16 year;
	quint8 wmonth,wday,emonth,eday;
};

//https://en.wikipedia.org/wiki/List_of_dates_for_Easter
QList<ED> initTV()
{
	QList<ED> r;
	r<<ED(1997,3, 30, 4, 27)
	<<ED(1998,4, 12, 4, 19)
	<<ED(1999,4, 4, 4, 11)
	<<ED(2000,4, 23, 4, 30)
	<<ED(2001,4, 15)
	<<ED(2002,3, 31, 5, 5)
	<<ED(2003,4, 20, 4, 27)
	<<ED(2004,4, 11)
	<<ED(2005,3, 27, 5, 1)
	<<ED(2006,4, 16, 4, 23)
	<<ED(2007,4, 8)
	<<ED(2008,3, 23, 4, 27)
	<<ED(2009,4, 12, 4, 19)
	<<ED(2010,4, 4)
	<<ED(2011,4, 24)
	<<ED(2012,4, 8, 4, 15)
	<<ED(2013,3, 31, 5, 5)
	<<ED(2014,4, 20)
	<<ED(2015,4, 5, 4, 12)
	<<ED(2016,3, 27, 5, 1)
	<<ED(2017,4, 16)
	<<ED(2018,4, 1, 4, 8)
	<<ED(2019,4, 21, 4, 28)
	<<ED(2020,4, 12, 4, 19)
	<<ED(2021,4, 4, 5, 2)
	<<ED(2022,4, 17, 4, 24)
	<<ED(2023,4, 9, 4, 16)
	<<ED(2024,3, 31, 5, 5)
	<<ED(2025,4, 20)
	<<ED(2026,4, 5, 4, 12)
	<<ED(2027,3, 28, 5, 2)
	<<ED(2028,4, 16)
	<<ED(2029,4, 1, 4, 8)
	<<ED(2030,4, 21, 4, 28)
	<<ED(2031,4, 13)
	<<ED(2032,3, 28, 5, 2)
	<<ED(2033,4, 17, 4, 24)
	<<ED(2034,4, 9)
	<<ED(2035,3, 25, 4, 29)
	<<ED(2036,4, 13, 4, 20)
	<<ED(2037,4, 5);
	return r;
}

class TClass:public QObject
{
	Q_OBJECT
private slots:
	void western_easter();
	void eastern_easter();
};

#include "test2.moc"

void TClass::western_easter()
{
	for(ED ed:initTV()){
		QDate d=CalTools::easterSunday(ed.year);
		QVERIFY(ed.isWest(d));
	}
}

void TClass::eastern_easter()
{
	for(ED ed:initTV()){
		QDate d=CalTools::orthodoxEasterSunday(ed.year);
		QVERIFY(ed.isEast(d));
	}
}

QTEST_MAIN(TClass)