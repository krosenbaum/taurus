// CLiP - Command Line Parser
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2021
// protected under the GNU GPL v.3 or at your option any newer

#include "clip.h"
#include "clip_p.h"

#include <QDebug>

ClipRegistry& ClipRegistry::instance()
{
    static ClipRegistry cr;
    return cr;
}

static inline ClipRegistry& creg(){return ClipRegistry::instance();}

ClipRegistry::ClipRegistry()
{
    Command c;
    c.id=0;
    c.numargs=-1;
    c.parentid=-1;
    mcmd.append(c);
}

ClipRegistry::~ClipRegistry()
{
    //end of CLiP
}

bool ClipRegistry::hasCommand(int id)const
{
    if(id<0 || id>=nextid)return false;
    if(id==0)return true;
    for(const auto&c:mcmd)
        if(c.id==id)return true;
    return false;
}
bool ClipRegistry::hasOption(int id)const
{
    if(id<=0 || id>=nextid)return false;
    for(const auto&o:mopt)
        if(o.id==id)return true;
    return false;
}

int ClipRegistry::addCommand(int parent,QStringList cmdnames)
{
    if(!hasCommand(parent)){
        qDebug()<<"CLiP Warning: attempt to register sub-command to non-command. Refusing.";
        qDebug()<<"  Parent:"<<parent<<"Sub Names:"<<cmdnames;
        return -1;
    }
    //check command is not doubled
    for(const auto&c:subCommands(parent)){
        for(auto cnm:c.names)
            if(cmdnames.contains(cnm)){
                qDebug()<<"CLiP Warning: double definition of command name"<<cnm<<"- refusing to register.";
                qDebug()<<"  complete list of new names:"<<cmdnames.join(", ").toUtf8().data();
                qDebug()<<"      list of existing names:"<<c.names.join(", ").toUtf8().data();
                return -1;
            }
    }
    //register command
    Command c;
    c.names=cmdnames;
    c.parentid=parent;
    c.id=nextid++;
    mcmd.append(c);
    return c.id;
}
int ClipRegistry::addOption(int cmd, QChar optc, QString opts, Clip::OptFlags flags)
{
    if(!hasCommand(cmd)){
        qDebug()<<"CLiP Warning: attempt to register option for non-command. Refusing.";
        qDebug()<<"   Parent:"<<cmd<<"Short:"<<optc<<"Long:"<<opts;
        return -1;
    }
    if(optc==0 && opts.isEmpty()){
        qDebug()<<"CLiP Warning: attempt to register empty option. Refusing.";
        return -1;
    }
    //check option does not exist yet for this command
    for(const auto&o:options(cmd)){
        if(optc!=0 && o.shortopt==optc){
            qDebug()<<"CLiP Warning: double definition of short option"<<optc;
            qDebug()<<"  new Option:"<<QString("-%1 %2%3").arg(optc).arg(opts.isEmpty()?"":"--").arg(opts);
            qDebug()<<"  old Option:"<<QString("-%1 %2%3").arg(o.shortopt).arg(o.longopt.isEmpty()?"":"--").arg(o.longopt);
            qDebug()<<"  parent Command:"<<commandPathStr(cmd).join(' ');
            return -1;
        }
        if(!opts.isEmpty() && o.longopt==opts){
            qDebug()<<"CLiP Warning: double definition of long option"<<opts;
            qDebug()<<"  new Option:"<<QString("-%1 %2%3").arg(optc).arg(opts.isEmpty()?"":"--").arg(opts);
            qDebug()<<"  old Option:"<<QString("-%1 %2%3").arg(o.shortopt).arg(o.longopt.isEmpty()?"":"--").arg(o.longopt);
            qDebug()<<"  parent Command:"<<commandPathStr(cmd).join(' ');
            return -1;
        }
    }
    //add to list
    Option o;
    o.parentid=cmd;
    o.id=nextid++;
    o.shortopt=optc;
    o.longopt=opts;
    o.flags=flags;
    mopt.append(o);
    return o.id;
}

bool ClipRegistry::setArguments(int cmd,int count)
{
    if(!hasCommand(cmd))return false;
    //find cmd
    for(auto&co:mcmd){
        if(co.id==cmd){
            co.numargs=count;
            return true;
        }
    }
    //not found, should be unreachable
    qDebug()<<"Major oops in CLiP: trying to set argument number for unknown command with known ID. Clearly it has never been to Singapore!";
    return false;
}

bool ClipRegistry::setCallback(int id,ClipCallback cb)
{
    if(id<0||id>=nextid)return false;
    for(auto&c:mcmd)if(c.id==id){c.cb=cb;return true;}
    for(auto&o:mopt)if(o.id==id){o.cb=cb;return true;}
    //should be unreachable
    qDebug()<<"Major oops in CLiP: trying to register callback for unknown object with known ID. Davy Jones is coming! Run!";
    return false;
}

ClipRegistry::Command ClipRegistry::command(int id)const
{
    if(id<0 || id>=nextid)return Command();
    for(const auto&c:mcmd)
        if(c.id==id)return c;
    return Command();
}

ClipRegistry::Option ClipRegistry::option(int id)const
{
    if(id<1 || id>=nextid)return Option();
    for(const auto&o:mopt)
        if(o.id==id)
            return o;
    return Option();
}

int ClipRegistry::parent(int id)const
{
    if(id<0 || id>=nextid)return -1;
    for(const auto&c:mcmd)if(c.id==id)return c.parentid;
    for(const auto&o:mopt)if(o.id==id)return o.parentid;
    //should be unreachable
    qDebug()<<"Major oops in CLiP: looking for parent of unknown object with known ID. Yes, savvy! But why is the rum gone?";
    return -1;
}

QList<int>ClipRegistry::commandPathNum(int id)const
{
    QList<int>r;
    if(id<0 || id>=nextid)return r;
    if(id==0){r<<0;return r;}
    //is it an option?
    auto o=option(id);
    if(o.id>0){r<<id;id=o.parentid;}
    if(!hasCommand(id)){
        qDebug()<<"Major oops in CLiP: tracing path of option without parent or unknown object with known ID. But why is the rum always gone?!";
        return r;
    }
    //go through parents backwards
    do{
        auto c=command(id);
        r.prepend(id);
        id=c.parentid;
    }while(id>=0);
    return r;
}

QStringList ClipRegistry::commandPathStr(int id)const
{
    QStringList r;
    if(id<0 || id>=nextid)return r;
    if(id==0){r<<0;return r;}
    //is it an option?
    auto o=option(id);
    if(o.id>0)
    {
        QString on;
        if(o.shortopt>0)on=QString("-%1").arg(o.shortopt);
        if(!o.longopt.isEmpty()){
            if(!on.isEmpty())on+=" ";
            on+=QString("--%1").arg(o.longopt);
            if(o.flags&Clip::HasArgument)on+=" [arg]";
        }
        r<<on;
        id=o.parentid;
    }
    if(!hasCommand(id)){
        qDebug()<<"Major oops in CLiP: tracing path of option without parent or unknown object with known ID. Drink up me hearties, yo ho!";
        return r;
    }
    //go through parents backwards, skip global
    do{
        auto c=command(id);
        r.prepend(c.names.value(0));
        id=c.parentid;
    }while(id>0);
    return r;
}

QList<int> ClipRegistry::optionNums(int cmdid)const
{
    QList<int> r;
    if(!hasCommand(cmdid))return r;
    for(const auto&o:mopt)
        if(o.parentid==cmdid)
            r<<o.id;
    return r;
}
QList<int> ClipRegistry::subCommandNums(int cmdid)const
{
    QList<int> r;
    if(!hasCommand(cmdid))return r;
    for(const auto&c:mcmd)
        if(c.parentid==cmdid)
            r<<c.id;
    return r;
}
QList<ClipRegistry::Option> ClipRegistry::options(int cmdid)const
{
    QList<Option>ro;
    if(!hasCommand(cmdid))return ro;
    for(const auto&o:mopt)
        if(o.parentid==cmdid)ro<<o;
    return ro;
}

QList<ClipRegistry::Command> ClipRegistry::subCommands(int cmdid)const
{
    QList<Command>rc;
    if(!hasCommand(cmdid))return rc;
    for(const auto&c:mcmd)
        if(c.parentid==cmdid)rc<<c;
    return rc;
}


bool ClipRegistry::setDescription(int id,QString ds,QString dl)
{
    if(id<0||id>=nextid)return false;
    for(auto&c:mcmd)
        if(c.id==id){
            c.dshort=ds;
            c.dlong=dl;
            return true;
        }
    for(auto&o:mopt)
        if(o.id==id){
            o.dshort=ds;
            o.dlong=dl;
            return true;
        }
    //should be unreachable
    qDebug()<<"Major oops in CLiP: tracing path of option without parent or unknown object with known ID. ...but you've heard of it!";
    return false;
}

int ClipRegistry::findCommand(QStringList names,int start)const
{
    if(start<0||start>=nextid)return -1;
    if(!hasCommand(start))return -1;
    //find it
    int id=start;
    for(const auto&n:names){
        bool found=false;
        for(const auto&c:mcmd){
            if(c.parentid==id && c.names.contains(n)){
                id=c.id;
                found=true;
                break;
            }
        }
        if(!found)return -1;
    }
    //return
    return id;
}

int ClipRegistry::findOption(QString name,int parent)const
{
    if(parent<0||parent>=nextid)return -1;
    if(!hasCommand(parent))return -1;
    //find it
    for(const auto&o:mopt)
        if(o.parentid==parent && o.longopt==name)
            return o.id;
    //nothing found
    return -1;
}

int ClipRegistry::findOption(QChar name,int parent)const
{
    if(parent<0||parent>=nextid)return -1;
    if(!hasCommand(parent))return -1;
    //find it
    for(const auto&o:mopt)
        if(o.parentid==parent && o.shortopt==name)
            return o.id;
    //nothing found
    return -1;
}

bool ClipRegistry::commandHasOptions(int id)const
{
    if(id<0||id>=nextid)return false;
    for(const auto&o:mopt){
        if(o.id==id)return false;
        if(o.parentid==id)return true;
    }
    return false;
}
bool ClipRegistry::commandHasSubcommands(int id)const
{
    if(id<0||id>=nextid)return false;
    for(const auto&c:mcmd)
        if(c.parentid==id)return true;
    return false;
}



ClipRef::~ClipRef(){}

bool ClipRef::hasParent() const
{
    if(subType()!=SubType::Opt){
        ClipRegistry::Command c=creg().command(id);
        if(c.id>=0)return c.parentid>=0;
    }
    if(subType()==SubType::Cmd)return false;
    ClipRegistry::Option o=creg().option(id);
    return o.parentid>=0;
}

ClipCmd ClipRef::parent() const
{
    if(subType()!=SubType::Opt){
        ClipRegistry::Command c=creg().command(id);
        if(c.id>=0)return ClipCmd(c.parentid);
    }
    if(subType()==SubType::Cmd)return false;
    ClipRegistry::Option o=creg().option(id);
    return ClipCmd(o.parentid);
}

bool ClipRef::isCommand() const
{
    //shortcuts
    if(subType()==SubType::Opt)return false;
    //actually test
    return creg().hasCommand(id);
}

bool ClipRef::isOption() const
{
    //shortcut
    if(subType()==SubType::Cmd)return false;
    //test
    return creg().hasOption(id);
}

ClipRef & ClipRef::operator=(const ClipRef&r)
{
    if(r.id<0){
        id=-1;
        return *this;
    }
    switch(subType()){
        case SubType::Ref:id=r.id;break;
        case SubType::Cmd:id=r.isCommand()?r.id:0;break;
        case SubType::Opt:id=r.isOption()?r.id:0;break;
        default:id=-1;break;//should be unreachable
    }
    return *this;
}

ClipRef & ClipRef::operator=(ClipRef &&r)
{
    if(r.id<0){
        id=-1;
        return *this;
    }
    switch(subType()){
        case SubType::Ref:id=r.id;break;
        case SubType::Cmd:id=r.isCommand()?r.id:0;break;
        case SubType::Opt:id=r.isOption()?r.id:0;break;
        default:id=-1;break;//should be unreachable
    }
    return *this;
}

ClipCmd ClipRef::asCommand() const
{
    if(subType()!=SubType::Opt)return ClipCmd(id);
    else return ClipCmd();
}

ClipOpt ClipRef::asOption() const
{
    if(subType()!=SubType::Cmd)return ClipOpt(id);
    else return ClipOpt();
}

ClipRef & ClipRef::setCallback(ClipCallback cb)
{
    creg().setCallback(id,cb);
    return *this;
}

ClipOpt & ClipOpt::setCallback(ClipCallback cb)
{
    creg().setCallback(id,cb);
    return *this;
}

ClipCmd & ClipCmd::setCallback(ClipCallback cb)
{
    creg().setCallback(id,cb);
    return *this;
}


ClipOpt::ClipOpt(int ida)
{
    if(creg().hasOption(ida))id=ida;
    else id=-1;
}

ClipOpt::~ClipOpt(){}

QChar ClipOpt::shortOption() const
{
    if(id<1)return QChar();
    return creg().option(id).shortopt;
}

QString ClipOpt::longOption() const
{
    if(id<1)return QString();
    return creg().option(id).longopt;
}

Clip::OptFlags ClipOpt::flags() const
{
    if(id<1)return Clip::OptFlags();
    return creg().option(id).flags;
}


ClipCmd::ClipCmd(int ida)
{
    if(creg().hasCommand(ida))id=ida;
    else id=-1;
}

ClipCmd::~ClipCmd(){}

QStringList ClipCmd::names() const
{
    if(id<0)return QStringList();
    return creg().command(id).names;
}

int ClipCmd::arguments() const
{
    if(id<0)return 0;
    return creg().command(id).numargs;
}

ClipCmd & ClipCmd::setArguments(int num)
{
    if(num<0)num=-1;//just make sure it is an exact value
    if(id>=0)
        creg().setArguments(id,num);
    return *this;
}

QList<ClipOpt> ClipCmd::options() const
{
    QList<int>oids=creg().optionNums(id);
    QList<ClipOpt>ro;
    for(int i:oids)ro.append(ClipOpt(i));
    return ro;
}

QList<ClipCmd> ClipCmd::subCommands() const
{
    QList<int>oids=creg().subCommandNums(id);
    QList<ClipCmd>rc;
    for(int i:oids)rc.append(ClipCmd(i));
    return rc;
}

ClipOpt ClipCmd::option(QChar shortopt, QString longopt, Clip::OptFlags flags)
{
    return ClipOpt(creg().addOption(id,shortopt,longopt,flags));
}

ClipCmd ClipCmd::subCommand(QStringList names)
{
    return ClipCmd(creg().addCommand(id,names));
}



ClipCmd Clip::globalCommand()
{
    return ClipCmd(0);
}

ClipOpt Clip::option(QChar shortopt, Clip::OptFlags flags)
{
    return globalCommand().option(shortopt,flags);
}

ClipOpt Clip::option(QString longopt, Clip::OptFlags flags)
{
    return globalCommand().option(longopt,flags);
}

ClipOpt Clip::option(QChar shortopt,QString longopt,OptFlags flags)
{
    return globalCommand().option(shortopt,longopt,flags);
}

ClipCmd Clip::command(QString name)
{
    return globalCommand().subCommand(name);
}

ClipCmd Clip::command(QStringList names)
{
    return globalCommand().subCommand(names);
}

static ClipWriter errorfunc,helpfunc;
static QString argv0;

Clip Clip::setErrorFunction(ClipWriter w)
{
    errorfunc=w;
    return Clip();
}

Clip Clip::setHelpFunction(ClipWriter w)
{
    helpfunc=w;
    return Clip();
}

Clip Clip::setWriterFunction(ClipWriter w)
{
    errorfunc=w;
    helpfunc=w;
    return Clip();
}

static bool allowGlobalOptions=true,allowGlobalArgs=false,positionalOptions=false;

Clip Clip::setAlwaysAllowGlobal(bool allowGlobalOptions, bool allowGlobalArgs)
{
    ::allowGlobalOptions=allowGlobalOptions;
    ::allowGlobalArgs=allowGlobalArgs;
    return Clip();
}

Clip Clip::setOptionsPositional(bool po)
{
    positionalOptions=po;
    return Clip();
}

static inline ClipWriter getErrorFunc()
{
    if(errorfunc)return errorfunc;
    else return [](QString s){qDebug().noquote()<<s;};
}

static inline ClipWriter getHelpFunc()
{
    if(helpfunc)return helpfunc;
    else return [](QString s){qDebug().noquote()<<s;};
}

Clip Clip::setArgv0(QString a)
{
    argv0=a;
    return Clip();
}

static inline void initArgv0()
{
    if(!argv0.isEmpty())return;
    argv0=QCoreApplication::arguments().value(0);
    if(argv0.isEmpty())argv0=QCoreApplication::applicationFilePath();
    if(argv0.isEmpty())argv0=QCoreApplication::applicationName();
}

bool Clip::exec(const QStringList& arguments)
{
    QSharedPointer<ClipExec>ce(new ClipExec(arguments));
    ce->setSelf(ce);
    return ce->run();
}

void Clip::printHelp(QStringList cmd)
{
    initArgv0();
    //basics, start FIXME: check behavior first
    QString hlp=tr("Usage: %1").arg(argv0);
    auto gcmd=creg().command(0);
    if(gcmd.numargs!=0 || creg().commandHasOptions(0))
        hlp+=tr(" [global arguments]");
    //verify sub-command
    int cid=creg().findCommand(cmd,0);
    if(cid<0)
        getErrorFunc()(tr("Invalid sub-command %1").arg(cmd.join(' ')));
    if(cid>0){
        gcmd=creg().command(cid);
        hlp+=" "+cmd.join(' ');
        if(gcmd.numargs!=0 || creg().commandHasOptions(0))
            hlp+=tr(" [command arguments]");
    }
    hlp+="\n"+gcmd.dlong+"\n";
    //print options
    if(creg().commandHasOptions(gcmd.id)){
        hlp+=tr("\nOptions:\n");
        for(const auto&o:creg().options(gcmd.id)){
            //TODO: add arguments and hint on optional; better formatting
            hlp+="\t";
            if(o.shortopt!=0)hlp+=QString(" -")+o.shortopt;
            if(!o.longopt.isEmpty())hlp+=QString(" --")+o.longopt;
            hlp+=" : "+o.dshort+"\n"; //FIXME: what about dlong?
        }
    }
    //print sub-commands
    if(creg().commandHasSubcommands(gcmd.id)){
        hlp+=tr("\nSub-Commands:\n");
        for(const auto&c:creg().subCommands(gcmd.id)){
            hlp+="\t"+c.names.join('|')+"\n"+c.dlong+"\n";
        }
    }
    //actually print
    getHelpFunc()(hlp);
}

void Clip::printHelp(ClipRef)
{
    //TODO FIXME HELP!!
}

static ClipHelpCallback helpCallback=nullptr;

Clip Clip::setHelpCallback(ClipHelpCallback hcb)
{
    helpCallback=hcb;
}

static Clip::BehaviorFlags parserBehavior=Clip::DefaultBehavior;

Clip Clip::setParserBehavior(Clip::BehaviorFlags bf)
{
    ::parserBehavior=bf;
}

Clip::BehaviorFlags Clip::parserBehavior()
{
    return ::parserBehavior;
}

static void generateHelp(ClipRef ref,ClipValue value,ClipErrorCode code)
{
    //TODO: this should be more elaborate...
    Clip::printHelp(ref);
}

static void callForHelp(ClipRef ref,ClipValue value,ClipErrorCode code)
{
    //check behavior:
    switch(parserBehavior&Clip::Mask_HelpCallback){
        case Clip::AlwaysCallForHelp:
            if(helpCallback){
                helpCallback(ref,value,code);
                break;
            }
            //no callback known, fall through to auto-help:
        case Clip::AlwaysAutoHelp:
            generateHelp(ref,value,code);
            break;
        case Clip::AutoHelpOnParserError:
            //skip callback failures, already handled by the callback...
            if(code!=ClipErrorCode::CallbackFailed)generateHelp(ref,value,code);
            break;
        case Clip::NeverAutoHelp:break;//nothing to do, assume caller will either take care or not care
        default:break;
    }
}


ClipExec::ClipExec(const QStringList&arguments)
:margs(arguments)
{}

#define ReturnSuccess {mstate=State::Success;return true;}
#define ReturnError {mstate=State::Error;return false;}
bool ClipExec::run()
{
    //init
    mvalues.clear();
    minid=nextid;
    //argv[0]
    if(margs.size()==0){
        qDebug()<<"Error: running command line parser without values.";
        ReturnError;
    }
    Value global;
    global.refid=0;
    global.pos=0;
    global.vt=VType::Argv0;
    global.name=margs[0];
    mvalues.insert(nextid++,global);
    //parse arguments & classify
    enum Mode{Argv0,GOpt,Cmd,COpt,Args};
    QList<int> needArg;
    for(int i=1;i<margs.size();i++){
        //TODO
    }
    //TODO: run callbacks (NOTE: use self, not this!!), going backwards through commands and left->right through options and arguments

    //TODO: done
    ReturnError;
}



ClipValue::ClipValue(QSharedPointer<ClipExec>x,int i)
:master(x),id(i)
{}

bool ClipValue::operator==(const ClipValue&v)const
{
    return !master.isNull() && id>=0 && master==v.master && id==v.id;
}

bool ClipValue::operator!=(const ClipValue&v)const{return !operator==(v);}

QList<ClipValue> ClipValue::allValues() const
{
    QList<ClipValue> r;
    if(master.isNull())return r;
    for(int i=0;i<master->mvalues.size();i++)r<<ClipValue(master,i);
    return r;
}

QList<ClipValue> ClipValue::allArguments() const
{
    QList<ClipValue> r;
    if(master.isNull())return r;
    for(int i=0;i<master->mvalues.size();i++)
        if(master->mvalues[i].vt==ClipExec::VType::Arg)
            r<<ClipValue(master,i);
    return r;
}

QStringList ClipValue::allArgumentsStringList() const
{
    QStringList r;
    if(master.isNull())return r;
    for(int i=0;i<master->mvalues.size();i++)
        if(master->mvalues[i].vt==ClipExec::VType::Arg)
            r<<master->mvalues[i].values;
    return r;
}

QList<ClipValue> ClipValue::allCommands() const
{
    QList<ClipValue> r;
    if(master.isNull())return r;
    for(int i=0;i<master->mvalues.size();i++)
        if(master->mvalues[i].vt==ClipExec::VType::Argv0 || master->mvalues[i].vt==ClipExec::VType::Cmd)
            r<<ClipValue(master,i);
    return r;
}

QList<ClipValue> ClipValue::allOptions() const
{
    QList<ClipValue> r;
    if(master.isNull())return r;
    for(int i=0;i<master->mvalues.size();i++)
        if(master->mvalues[i].vt==ClipExec::VType::Opt)
            r<<ClipValue(master,i);
    return r;
}

bool ClipValue::isValid()const
{
    if(master.isNull() || id<0)return false;
    if(id<master->minid || id>=master->nextid)return false;//faster than contains
    return master->mvalues.contains(id);//apparently I'm paranoid!
}

bool ClipValue::isArgument() const
{
    if(!isValid())return false;
    return master->mvalues[id].vt==ClipExec::VType::Arg;
}

bool ClipValue::isOption() const
{
    if(!isValid())return false;
    return master->mvalues[id].vt==ClipExec::VType::Opt;
}

bool ClipValue::isCommand() const
{
    if(!isValid())return false;
    return master->mvalues[id].vt==ClipExec::VType::Argv0 || master->mvalues[id].vt==ClipExec::VType::Cmd;
}

bool ClipValue::isGlobal() const
{
    if(!isValid())return false;
    return master->mvalues[id].vt==ClipExec::VType::Argv0;
}

QList<ClipValue> ClipValue::childArguments() const
{
    QList<ClipValue> rc;
    if(!isValid())return rc;
    for(int i=master->minid;i<master->nextid;i++)
        if(master->mvalues[i].parent==id && master->mvalues[i].vt==ClipExec::VType::Arg)
            rc.append(ClipValue(master,i));
    return rc;
}

ClipValue ClipValue::childCommand() const
{
    if(!isValid())return ClipValue();
    for(int i=master->minid;i<master->nextid;i++)
        if(master->mvalues[i].parent==id && master->mvalues[i].vt==ClipExec::VType::Cmd)
            return ClipValue(master,i);
    return ClipValue();
}

QList<ClipValue> ClipValue::childOptions() const
{
    QList<ClipValue> rc;
    if(!isValid())return rc;
    for(int i=master->minid;i<master->nextid;i++)
        if(master->mvalues[i].parent==id && master->mvalues[i].vt==ClipExec::VType::Opt)
            rc.append(ClipValue(master,i));
    return rc;
}

QList<ClipValue> ClipValue::children() const
{
    QList<ClipValue> rc;
    if(!isValid())return rc;
    for(int i=master->minid;i<master->nextid;i++)
        if(master->mvalues[i].parent==id)
            rc.append(ClipValue(master,i));
    return rc;
}

ClipValue ClipValue::parent() const
{
    if(!isValid())return ClipValue();
    int pid=master->mvalues.value(id).parent;
    if(master->mvalues.contains(pid))return ClipValue(master,pid);
    else return ClipValue();
}

QString ClipValue::name() const
{
    if(!isValid())return QString();
    return master->mvalues.value(id).name;
}

QStringList ClipValue::arguments() const
{
    if(!isValid())return QStringList();
    return master->mvalues.value(id).values;
}

ClipValue ClipValue::topValue() const
{
    if(master.isNull() || master->state()==ClipExec::State::Init)
        return ClipValue();
    return ClipValue(master,master->minid);//argv0 is always the first one
}

ClipValue ClipValue::targetCommand() const
{
    if(!isValid() || master->state()!=ClipExec::State::Success)
        return ClipValue();
    //find last command in queue
    int id=master->minid;
    for(int i=id;i<master->nextid;i++)
        if(master->mvalues[i].vt==ClipExec::VType::Cmd)
            id=i;
    //return it
    return ClipValue(master,id);
}

ClipRef ClipValue::reference() const
{
    if(master.isNull())return ClipRef();
    return ClipRef(master->mvalues.value(id).refid);
}

ClipCmd ClipValue::commandRef() const
{
    return reference();
}

ClipOpt ClipValue::optionRef() const
{
    return reference();
}

ClipCmd ClipValue::parentRef() const
{
    //maybe there is a more efficient way? can't be bothered now.
    return parent().reference();
}

ClipCmd ClipValue::targetCommandRef() const
{
    //maybe there is a more efficient way? can't be bothered now.
    return targetCommand().reference();
}

QString ClipValue::commandLineString() const
{
    if(!isValid())return QString();
    int idx=master->mvalues[id].pos;
    if(idx<0||idx>=master->margs.size())return QString();
    return master->margs[idx];
}
