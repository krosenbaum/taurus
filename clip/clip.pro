TEMPLATE = lib
TARGET = clip
CONFIG += dll create_prl hide_symbols separate_debug_info c++11
QT -= gui
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp
DEFINES += CLIP_LIBRARY_BUILD
VERSION =
DESTDIR = $$OUT_PWD/../lib

HEADERS += \
	clip.h clip_p.h

SOURCES += \
	clip.cpp

INCLUDEPATH += . ../include/clip
DEPENDPATH += $$INCLUDEPATH
