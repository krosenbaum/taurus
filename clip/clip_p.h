// CLiP - Command Line Parser
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QCoreApplication>
#include <QMap>

#include "clip.h"

//Internal Registry for commands and options.
class ClipRegistry
{
    Q_DECLARE_TR_FUNCTIONS(Clip)
public:
    //returns singleton
    static ClipRegistry& instance();

    //internal command structure
    struct Command{
        QStringList names;
        int id=-1,parentid=-1,numargs=0;
        ClipCallback cb;
        QString dshort,dlong;
    };
    //internal option structure
    struct Option{
        QChar shortopt;
        QString longopt;
        Clip::OptFlags flags;
        int id=-1,parentid=-1;
        ClipCallback cb;
        QString dshort,dlong;
    };

    //ID of the global command root
    constexpr int globalCommand()const{return 0;}
    //add a sub-command to a command (use parent=0 for "under global")
    int addCommand(int parent,QStringList cmdnames);
    //add a sub-command to a command (use parent=0 for "under global")
    inline int addCommand(int parent,QString cmdname){return addCommand(parent,QStringList()<<cmdname);}
    //add an option to a command (use parent=0 for "under global")
    int addOption(int cmd, QChar optc, QString opts, Clip::OptFlags flags);
    //add number of arguments for a command
    bool setArguments(int cmd,int count);
    //set a callback for command or option
    bool setCallback(int id,ClipCallback);
    //set description for command or option
    bool setDescription(int id,QString dshort,QString dlong);

    //check whether command exist
    bool hasCommand(int)const;
    //check whether option exists
    bool hasOption(int)const;
    //return parent cmd of any object
    int parent(int id)const;

    //return the full hierarchy of a command or option, starting at 0
    QList<int>commandPathNum(int id)const;
    //return the full hierarchy of a command or option as strings, starting at 0
    QStringList commandPathStr(int id)const;

    //reverse search: look for a final command by moving through the hierarchy
    int findCommand(QStringList names,int start=0)const;
    //reverse search: look for a final option by moving through the hierarchy
    int findOption(QString name,int parent=0)const;
    //reverse search: look for a final option by moving through the hierarchy
    int findOption(QChar name,int parent=0)const;

    //returns a command object by ID
    Command command(int)const;
    //returns an option by ID
    Option option(int)const;
    //returns all sub-options of a command
    QList<int> optionNums(int cmdid=0)const;
    //returns all sub-commands of a command
    QList<int> subCommandNums(int cmdid=0)const;
    //returns all sub-options of a command
    QList<Option> options(int cmdid=0)const;
    //returns all sub-commands of a command
    QList<Command> subCommands(int cmdid=0)const;

    //returns true if the command has options
    bool commandHasOptions(int)const;
    //returns true if the command has sub-commands
    bool commandHasSubcommands(int)const;
private:
    QList<Option> mopt;
    QList<Command> mcmd;
    int nextid=1;

    //hidden constructor/destructor for singleton
    ClipRegistry();
    ~ClipRegistry();
    //make sure copying is not possible
    ClipRegistry(const ClipRegistry&)=delete;
    ClipRegistry(ClipRegistry&&)=delete;
    ClipRegistry& operator=(const ClipRegistry&)=delete;
    ClipRegistry& operator=(ClipRegistry&&)=delete;
};

//internal: command line parser and caller of callbacks
// only used from Clip::exec and from ClipValue
// DANGER: this class meddles with shared and weak pointers - make sure you fully understand them before you meddle too!
class ClipExec
{
    friend class ClipValue;
    Q_DECLARE_TR_FUNCTIONS(ClipExec)
public:
    //instantiates the parser
    ClipExec(const QStringList&arguments);
    ~ClipExec(){self.clear();}
    //must be called before run or the whole shared value experience will go horribly wrong!
    void setSelf(QWeakPointer<ClipExec>s){self=s;}
    bool run();

    enum class State{
        //constructed, but no parsing yet
        Init,
        //parser is running, at least the Argv0 exists in mvalues
        Running,
        //parsing done, in the callback phase
        Calling,
        //callbacks done, run() has returned true
        Success,
        //run() has returned false
        Error
    };
    State state()const{return mstate;}
private:
    enum class VType{Unknown,Cmd,Opt,Arg,Argv0};
    struct Value{
        int pos=-1;//position in argument list
        int parent=-1;//points to parent value
        int refid=-1;//points to definition in ClipRegistry
        VType vt=VType::Unknown;//kind of token (command,option, ...)
        QString name;
        QStringList values;
    };
    QStringList margs;
    QMap<int,Value>mvalues;
    State mstate=State::Init;
    int nextid=1,minid=1;
    QWeakPointer<ClipExec>self;
};
