// CLiP - Command Line Parser
// (c) Konrad Rosenbaum <konrad@silmor.de>, 2021
// protected under the GNU GPL v.3 or at your option any newer

#pragma once

#include <QStringList>
#include <QtCore/QtGlobal>
#include <QCoreApplication>
#include <QFlags>
#include <QSharedPointer>

#include <functional>

#if defined(CLIP_LIBRARY_BUILD)
#  define CLIP_EXPORT Q_DECL_EXPORT
#else
#  define CLIP_EXPORT Q_DECL_IMPORT
#endif

class ClipRef;
class ClipValue;

///\relates Clip
///A callback when a specific command or option has been parsed and is ready to be processed.
///\code
///bool myCallback(ClipRef ref,ClipValue value)
///{
///  qDebug()<<"Callback for"<<ref.isOption()?"an option":"a command";
///}
///\endcode
///
///\param ref - a reference to the option or command definition
///\param value - the list of values that apply
///\returns should return true if the values are valid, if it returns false the argument processing fails and stops
typedef std::function<bool(ClipRef,ClipValue)> ClipCallback;

///\relates Clip
///A function to write to the console to complain about arguments.
typedef std::function<void(QString)>ClipWriter;

///Error codes
enum class ClipErrorCode{
    ///Please output a help page (both InterceptHelpCommand and AlwaysCallForHelp are set)
    HelpNeeded,
    ///A callback has returned false.
    CallbackFailed,
    ///The option is not known to the parser (may be on wrong command).
    UnknownOption,
    ///The option occured too often.
    UnexpectedOption,
    ///The long option has a value although not expected to have one.
    UnexpectedOptionValue,
    ///The long option is expected to have a value, but none has been found.
    MissingOptionValue,
    ///What appears to be a command is unknown to the parser.
    UnknownCommand,
    ///Too many arguments were received.
    UnexpectedArgument,
    ///Not enough arguments were received.
    MissingArgument,
    ///An expected option is missing.
    MissingOption,
};

///\relates Clip
///A callback in case an error occurs.
///\code
///void myHelpCallback(ClipRef ref,ClipValue value,ErrorCode code)
///{
///  qDebug()<<"There was an error...";
///}
///}\endcode
///
///\param ref - a reference to the option or command definition
///\param value - the concrete value(s) that caused the error, this may be the parent of a missing item
///\param code - the exact reason for the error
typedef std::function<void(ClipRef,ClipValue,ClipErrorCode)> ClipHelpCallback;

class ClipOpt;
class ClipCmd;

///A reference to a command or option. See ClipCmd and ClipOpt for details.
///References are internally shared.
class CLIP_EXPORT ClipRef
{
protected:
    ///@private
    int id=-1;
    friend class Clip;
    friend class ClipOpt;
    friend class ClipCmd;
    friend class ClipValue;
    ///@private
    ClipRef(int i):id(i){}
    ///@private
    enum class SubType{Ref,Opt,Cmd};
    ///@private
    virtual SubType subType()const{return SubType::Ref;}
public:
    ClipRef()=default;
    ClipRef(const ClipRef&)=default;
    ClipRef(ClipRef&&)=default;
    virtual ~ClipRef();
    ClipRef& operator=(const ClipRef&);
    ClipRef& operator=(ClipRef&&);

    ///returns true if the reference points to a valid command or option
    bool isValid()const{return id>=0;}
    ///returns true if this references an option
    bool isOption()const;
    ///returns true if this references a command
    bool isCommand()const;

    ///returns this reference as an option (or an invalid reference if it is not an option)
    ClipOpt asOption()const;
    ///returns this reference as a command (or an invalid command it it is not a command)
    ClipCmd asCommand()const;

    ///returns the parent command of this reference (returns an invalid command for the global root or any invalid reference)
    ClipCmd parent()const;
    ///returns true if this reference has a parent, the only valid reference without a parent is the global root
    bool hasParent()const;

    ///sets a callback function for this reference, callbacks get called during Clip::exec
    ClipRef& setCallback(ClipCallback);
};

///Master class for creating and maintaining options and commands.
class CLIP_EXPORT Clip
{
    Q_DECLARE_TR_FUNCTIONS(Clip)
    Q_GADGET
    ///@private
    Clip(){}
public:
    ///@private
    Clip(const Clip&){}

    ///Flags for individual options.
    enum OptFlag {
        ///the option has no argument.
        NoArgument=0,
        ///the option does have an argument.
        HasArgument=2,
        ///the argument is optional
        OptionalArgument=2,
        ///the argument is mandatory
        MandatoryArgument=3,

        ///the option itself is mandatory (it must appear)
        MandatoryOption=4,
        ///the option is optional (may or may not appear)
        OptionalOption=0,
        //the option can appear multiple times
        MultiOption=8,
        ///the option can appear only once
        SingleOption=0,
    };
    ///@private
    Q_DECLARE_FLAGS(OptFlags,OptFlag);
    ///@private
    Q_FLAG(OptFlags);
#ifdef DOXYGEN_RUN
    ///Flags for options. See #OptFlag.
    typedef flags OptFlags;
#endif

    ///Flags for general behavior.
    enum BehaviorFlag
    {
        ///only the options defined in the outermost leaf command are legal
        LeafOptionsOnly                 = 0000000,
        ///global options are always allowed: if an option is in front of all sub-commands
        ///or is not defined in the sub-command then it is looked up in the global definitions
        GlobalOptionsAlwaysAllowed      = 0000001,
        ///options are looked up at the upper-most sub-command first, then descending through the sub-commands right to left
        HierarchicalOptionsDescending   = 0000002,
        ///options are looked up at the global level first, then ascending through the sub-commands from left to right
        HierarchicalOptionsAscending    = 0000003,
        ///global options and those for the upper most leaf command are legal, they are looked up in the sub-command first
        GlobalAndLeafOptionsPreferLeaf  = 0000004,
        ///global options and those for the upper most leaf command are legal, they are looked up globally first
        GlobalAndLeafOptionsPreferGlobal= 0000005,
        ///@private
        Mask_OptionPosition             = 0000007,

        ///when using long options then the value cannot be detached, they must always have the format --long=value
        NoDetachedLongOptions           = 0000000,
        ///when using long options then the value may be detached if the value is optional or appears directly after
        ///the option: --long value
        DetachedLongOptionsAllowed      = 0000010,

        ///arguments which overflow the last sub-command are assigned to the global layer
        MakeExtraArgumentsGlobal        = 0000020,
        ///arguments which overflow the last sub-command lead to an error
        ArgumentsAlwaysAtLeaf           = 0000000,

        ///if an option appears multiple times then the additional values are assigned to the first appearance of the
        ///option, potentially changing the order of later appearances
        AggregateOptions                = 0000000,
        ///if an option appears multiple times then a different value object is created for each appearance of the
        ///option, thus preserving the order of all options
        PositionalOptions               = 0000040,

        ///The stand-alone single dash "-" is interpreted as a simple argument (this is often used to denote stdin).
        DashIsArgument                  = 0000000,
        ///The stand-alone single dash "-" will be regarded as an error.
        DashIsIllegal                   = 0000100,

        ///A double dash will be interpreted as the end of commands and options, any token that follows it is a simple argument.
        DoubleDashIsSeparator           = 0000000,
        ///A double dash will be interpreted as the short option with the character dash (or minus).
        DoubleDashIsShortOption         = 0001000,
        ///A double dash on its own will be interpreted as a syntax error.
        DoubleDashIsIllegal             = 0002000,
        ///@private
        Mask_DoubleDash                 = 0007000,

        ///The parser allows both long and short options.
        LongAndShortOptions             = 0000000,
        ///No token will be interpreted as a long option, instead each character will be interpreted as a short option.
        NoLongOptionsExist              = 0010000,
        ///No token will be interpreted as short option, instead the parser looks for a long option.
        NoShortOptionsExist             = 0020000,
        ///No token will be interpreted as an option, everything is either a command or a simple argument.
        NoOptions                       = 0030000,
        ///@private
        Mask_OptionTypes                = 0070000,

        ///if the parser encounters an error it automatically generates a "Usage" page and returns false.
        ///No error text is generated if a callback returns false: the callback is expected to handle this.
        AutoHelpOnParserError           = 0000000,
        ///Regardless of situation the parser never generates an automatic help page, it simply returns false without
        ///calling any more callbacks.
        NeverAutoHelp                   = 0100000,
        ///Always generates a help page if a parser error is encountered or if a callback returns false.
        AlwaysAutoHelp                  = 0200000,
        ///Calls the help callback (see #setHelpCallback) in case of any error.
        AlwaysCallForHelp               = 0300000,
        ///@private
        Mask_HelpCallback               = 0300000,

        ///Intercepts the help sub-command or option at any level and outputs the corresponding "Usage" page or
        ///calls the help callback (see #setHelpCallback). This also allows a more relaxed syntax in case of the
        ///help command or option.
        InterceptHelpCommand            = 0400000,

        ///The default behavior is to:
        /// - allow short and long options, but on the leaf sub-command only
        /// - do not allow long options to detach their values
        /// - force all arguments to belong to the leaf sub-command
        /// - interpret a single dash as an simple argument (usually it stands for the special file "stdin")
        /// - use double dash to separate commands and options from pure arguments
        /// - output automatic help on parser errors, leave ordinary help functions to the calling layer
        DefaultBehavior=0
    };
    ///@private
    Q_DECLARE_FLAGS(BehaviorFlags,BehaviorFlag);
    ///@private
    Q_FLAG(BehaviorFlags);
#ifdef DOXYGEN_RUN
    ///Flags for general behavior. See #BehaviorFlag.
    typedef flags BehaviorFlags;
#endif

    ///convenience constant for commands that take no arguments
    static const int NoCommandArguments=0;
    ///convenience constant for commands that take a variable number of arguments
    static const int AnyCommandArguments=-1;

    ///returns a reference to the global root
    static ClipCmd globalCommand();

    ///creates a new global option
    static ClipOpt option(QChar shortopt,QString longopt,OptFlags flags=OptionalOption);
    ///creates a new global option
    static ClipOpt option(QChar shortopt,OptFlags flags=OptionalOption);
    ///creates a new global option
    static ClipOpt option(QString longopt,OptFlags flags=OptionalOption);

    ///creates a new sub-command under the global root
    static ClipCmd command(QString name);
    ///creates a new sub-command under the global root
    static ClipCmd command(QStringList names);

    ///sets a function to write error messages - per default uses qDebug()
    static Clip setErrorFunction(ClipWriter);
    ///sets a function to write help - per default uses qDebug()
    static Clip setHelpFunction(ClipWriter);
    ///sets both the error and help function to the same one
    static Clip setWriterFunction(ClipWriter);

    ///sets the callback for generating help
    static Clip setHelpCallback(ClipHelpCallback);

    ///sets a value for argv[0], per default it queries QCoreApplication
    static Clip setArgv0(QString);

    ///sets the behavior of the parser and caller, see #BehaviorFlag for details
    static Clip setParserBehavior(BehaviorFlags);

    static BehaviorFlags parserBehavior();

    ///parses the arguments and calls back the set functions
    static bool exec(const QStringList&arguments);

    ///prints help text
    ///\param cmd if given: the string of sub-commands to print help for
    static void printHelp(QStringList cmd=QStringList());

    ///prints help text for the given command or option
    static void printHelp(ClipRef);
};

Q_DECLARE_OPERATORS_FOR_FLAGS(Clip::OptFlags);

///References an option.
class CLIP_EXPORT ClipOpt:public ClipRef
{
    friend class Clip;
    friend class ClipRef;
    friend class ClipCmd;
    ClipOpt(int);
protected:
    ///@private
    virtual SubType subType()const override{return SubType::Opt;}
public:
    ///creates an invalid option reference
    ClipOpt()=default;
    ///copies an option reference
    ClipOpt(const ClipOpt&)=default;
    ///copies an option reference
    ClipOpt(ClipOpt&&)=default;
    ///copies an option reference
    ClipOpt(const ClipRef&r):ClipOpt(r.id){}
    ///@private
    ~ClipOpt();
    ///copies an option reference
    ClipOpt& operator=(const ClipOpt&)=default;
    ///copies an option reference
    ClipOpt& operator=(ClipOpt&&)=default;

    ///returns the short option character
    QChar shortOption()const;
    ///returns the long option name
    QString longOption()const;
    ///returns flags for this option
    Clip::OptFlags flags()const;

    ///overrides the callback for this option
    ClipOpt& setCallback(ClipCallback);
};

///References a sub-command
class CLIP_EXPORT ClipCmd:public ClipRef
{
    friend class Clip;
    friend class ClipRef;
    ClipCmd(int);
protected:
    ///@private
    virtual SubType subType()const override{return SubType::Cmd;}
public:
    ///creates an invalid command
    ClipCmd()=default;
    ///copies a command
    ClipCmd(const ClipCmd&)=default;
    ///copies a command
    ClipCmd(ClipCmd&&)=default;
    ///copies a command
    ClipCmd(const ClipRef&r):ClipCmd(r.id){}
    ///@private
    ~ClipCmd();
    ///copies a command
    ClipCmd& operator=(const ClipCmd&)=default;
    ///copies a command
    ClipCmd& operator=(ClipCmd&&)=default;

    ///returns all names of the command
    QStringList names()const;
    ///returns how many arguments the command takes
    int arguments()const;
    ///returns all sub-options of this command
    QList<ClipOpt>options()const;
    ///returns all sub-commands of this command
    QList<ClipCmd>subCommands()const;

    ///sets how many arguments the command can take
    ClipCmd& setArguments(int);
    ///overrides the callback for this command
    ClipCmd& setCallback(ClipCallback);

    ///creates a new sub-command for this command
    ClipCmd subCommand(QString name){return subCommand(QStringList()<<name);}
    ///creates a new sub-command for this command
    ClipCmd subCommand(QStringList);

    ///creates a new option for this command
    ClipOpt option(QChar shortopt,QString longopt,Clip::OptFlags flags=Clip::OptionalOption);
    ///creates a new option for this command
    ClipOpt option(QChar shortopt,Clip::OptFlags flags=Clip::OptionalOption){return option(shortopt,QString(),flags);}
    ///creates a new option for this command
    ClipOpt option(QString longopt,Clip::OptFlags flags=Clip::OptionalOption){return option(QChar(),longopt,flags);}
};

///@private
class ClipExec;

///represents a parser value with its context;
///values are shared and remain valid only while the parser is valid.
class CLIP_EXPORT ClipValue
{
    QSharedPointer<ClipExec>master;
    int id=-1;
    ClipValue(QSharedPointer<ClipExec>x,int i);
    friend class ClipExec;
public:
    ///constructs an invalid value reference
    ClipValue()=default;
    ///copies a value reference
    ClipValue(const ClipValue&)=default;
    ///copies a value reference
    ClipValue(ClipValue&&)=default;
    ///copies a value reference
    ClipValue& operator=(const ClipValue&)=default;
    ///copies a value reference
    ClipValue& operator=(ClipValue&&)=default;

    ///returns true if this is a valid value and both references point to the same value;
    ///tests for identity not identical spelling, so if you have the same option with the same value twice the two references will still not be equal
    bool operator==(const ClipValue&v)const;
    ///returns true if the two values point to different command line arguments or if at least one is invalid
    bool operator!=(const ClipValue&v)const;

    ///returns whether this instance represents a valid reference to a command line argument/option/sub-command,
    ///values can be invalid if an error occured or if another parser run has been started since it was created
    bool isValid()const;

    ///returns true if this value is an option (-x or --xyz)
    bool isOption()const;
    ///returns true if this value is a sub-command or the global context
    bool isCommand()const;
    ///returns true if this value is a simple argument (file name or similar)
    bool isArgument()const;
    ///returns true if this value is the global context that contains all other values
    bool isGlobal()const;

    ///returns a reference to the definition of this value
    ClipRef reference()const;
    ///returns a reference to the definition of this value if it is an option
    ClipOpt optionRef()const;
    ///returns a reference to the definition of this value if it is a command (or the parent command if it is an argument)
    ClipCmd commandRef()const;
    ///returns a reference to the definition of this value's parent command (or the global command context)
    ClipCmd parentRef()const;
    ///returns a reference to the definition of the outermost command context (see also Clip::globalCommand() for the global context)
    ClipCmd targetCommandRef()const;

    ///returns a reference to the parent value of this value (invalid if this is the global root context - argv[0])
    ClipValue parent()const;
    ///returns a reference to the root context (argv[0])
    ClipValue topValue()const;
    ///returns all child values of this value (direct children only, only valid for commands)
    QList<ClipValue> children()const;
    ///returns all options that are children of this value (only valid for global and target command)
    QList<ClipValue> childOptions()const;
    ///returns all simple arguments of this value (can also be retrieved as QStringList via arguments() ; only valid for global and target command)
    QList<ClipValue> childArguments()const;
    ///if this is a command and it has a sub-command: return a reference to the direct sub-command
    ClipValue childCommand()const;
    ///returns a reference to the outermost leaf command value (if there are no sub-commands then this is identical to topValue())
    ClipValue targetCommand()const;

    ///returns all values that currently exist in the parser context (regardless of hierarchy)
    QList<ClipValue> allValues()const;
    ///returns all options that currently exist in the parser context (regardless of hierarchy)
    QList<ClipValue> allOptions()const;
    ///returns all simple arguments that currently exist in the parser context (regardless of hierarchy)
    QList<ClipValue> allArguments()const;
    ///returns all simple arguments that currently exist in the parser context as a string list (regardless of hierarchy); this is different from arguments()
    QStringList allArgumentsStringList()const;
    ///returns all commands that currently exist in the parser context (regardless of hierarchy)
    QList<ClipValue> allCommands()const;

    ///returns all arguments of this context:
    /// - isOption()==true: the argument of the option, if options are non-positional then options of the same name are accumulated:
    ///  - if the value is "--option=value" then arguments() contains just {"value"}
    ///  - for "--option=value1 --option=value2 -o value3" with positional options you get 3 separate ClipValues
    ///  - for the same command line with non-positional options you get 1 ClipValue with name()=="option" and arguments()=={"value1","value2","value3"}
    /// - isCommand()==true: the normal (non-option and non-sub-command) arguments of the command - they also exist as separate ClipValues in childArguments() if you prefer to parse them this way
    /// - isGlobal()==true: like isCommand()==true
    /// - isArgument()==true: a single argument value on its own
    QStringList arguments()const;
    ///the name of the context:
    /// - isGlobal()==true: the argv[0] of the program (as far as it can be determined)
    /// - isCommand()==true: the name of the command in the form it appears
    /// - isOption()==true: the name of the option as it appears, without dashes
    ///  - if the command line value was "--option=value" then name()=="option"
    ///  - if the command line value was "-o value" then name()=="o"
    ///  - if the same option appears multiple times and options are non-positional then the first appearance is used
    /// - isArgument()==true: name() is always an empty string
    QString name()const;

    ///returns the string found on the command line (for long options this is the full "--option=value")
    QString commandLineString()const;
};

/** \mainpage
 * \code
 * bool myCallback(ClipRef def,ClipValue root)
 * {
 *   qDebug()<<"parser complete, starting to analyze values...";
 * }
 *
 * int main(int argc,char**argv)
 * {
 *   QApplication app(argc,argv);
 *   if(!Clip::exec(app.arguments()))return 1;
 *   return app.exec();
 * }
 *
 * auto opt1=Clip::option("hello");
 * auto cmd1=Clip::command("sub");
 * auto opt2=cmd1.option("subopt");
 * auto gcmd=Clip::globalCommand().setCallback(&myCallback);
 * \endcode
 *
 * #Clip is the static global interface to define arguments, commands and options.
 *
 * #ClipOpt and #ClipCmd are interfaces to define and revise sub-divisions.
 *
 * #ClipValue is used during parsing to convey actual command line values.
 */
