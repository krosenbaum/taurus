<html>
<title>Package Description Language</title>

<h1>Package Description Language</h1>

Package Description Files are XML files that are used in three capacities:

<ol>
<li>A manually written file to describe how to package files.</li>
<li>An automatically generated (by instgen) version containing information about generated archives, this is called the index file and is the one you should sign before uploading archives.</li>
<li>A slightly enhanced version of (2) that is put into the installation directory after a successful update. This is used to by Aurora to compare the currently installed version with the one available online.</li>
</ol>

<h2>Manual Description File</h2>

This section describes the syntax of the version you write manually to tell instgen how to create archives (in the
<a href="packages.html">source example</a>: myindex-source.xml):

<pre>
&lt;AuroraInfo>
  &lt;CurrentVersion version="2.0.1" mrv="020001"/>
  
  &lt;Settings 
    baseurl="http://www.example.com/myprogram" indexfile="myindex.xml" 
    pollinterval="1800" defaultchannel="mine"
    fulltargetdir="dist-*">
      &lt;Channel id="mine" name="Mine, My Own">mydir&lt;/Channel>
      &lt;Channel id="yours" name="Mine, My Own">yourdir&lt;/Channel>
  &lt;Settings>

  &lt;ArchiveSource name="myprogram-*.zip" buildbase="bin">
    &lt;Platform os="linux">
        &lt;Files exclude="*.debug">
                myprogram
                gpg
                lib*.so*
        &lt;/Files>
    &lt;/Platform>
    &lt;Platform os="windows">
        &lt;Files>
                *.exe
                Aurora.dll
                QtZip*.dll
        &lt;/Files>
    &lt;/Platform>
  &lt;/ArchiveSource>

  &lt;!-- GPG Data -->
  &lt;ArchiveSource name="updatekeys.zip">
    &lt;Platform os="all" cpu="all">
      &lt;Files buildbase="mygpg" zipbase="aurora-gpg">
        gpg.conf
        pubring.gpg
      &lt;/Files>
    &lt;/Platform>
  &lt;/ArchiveSource>
&lt;/AuroraInfo>
</pre>

<h3>Version Information</h3>

The CurrentVersion tag must contain the following attributes:
<ul>
<li>version="...": human readable version number (not used by program, for display)
<li>mrv="...": machine readable (ASCII sort-algo used for comparison), this one is used to determine if a new version is available for update
</ul>

Both attributes may use macros embedded in { and }:
<table frame="1" border="1">
<tr><td><b>Macro</b></td><td><b>Description</b></td></tr>
<tr><td>{TODAY}</td><td>The current local date in format YYYYMMDD.</td></tr>
<tr><td>{TODAYUTC}</td><td>The current date of the UTC time zone in format YYYYMMDD.</td></tr>
<tr><td>{TIME}</td><td>The current local time in format HHMM.</td></tr>
<tr><td>{TIMEUTC}</td><td>The current time in the UTC time zone in format HHMM.</td></tr>
<tr><td>{NOW}</td><td>The current local date and time in format YYYYMMDDhhmm.</td></tr>
<tr><td>{NOWUTC}</td><td>The current date and time of the UTC time zone in format YYYYMMDDhhmm.</td></tr>
<tr><td>{ENV:*}</td><td>The content of an environment variable or empty if it does not exist. Replace * with the name of an environment variable.</td></tr>
</table>

<h3>Basic Settings</h3>

The Settings tag describes basic polling behavior and where to look for new versions of the software:

<ul>
<li>baseurl - the directory on the web server in whose subdirectories the index file and the archives are found
<li>indexfile - the name of the index file, make sure this differes from this files name and it is not used by the program already, the signature is expected in a file with ".asc" appended
<li>pollinterval - the interval in seconds in which the application will poll for updates, make this a useful value, e.g. 1800 means 30minutes
<li>fulltargetdir - a local directory that will receive a copy of the content of all generated archives and the index file - you can use this for testing purposes
<li>defaultchannel - the download channel that is used by Aurora for updates if the user has not changed it
</ul>

The baseurl, indexfile, pollinterval attributes are copied to the index file(s).<p>

The Channel tag allows to specify several download directories with (potentially) different versions of the program. If no Channel tags are given then a channel with no name and the baseurl as download target is assumed - this matches the behavior or older versions of Aurora.<p>

The following attributes are allowed:
<ul>
<li>id - a machine readable ID for the channel - this is stored in the Aurora settings if the user selects a non-default channel as installation source
<li>name - a human readable name for the channel
</ul>

The content of the Channel tag is a whitespace separated list of directories. If those are full URLs (incl. "http://" or "https://") then this URL is used, if they are relative paths then they are taken as relative to the baseurl. Multiple URLs are checked sequencially.<p>

<h3>Source Specs for Archives</h3>

Each ArchiveSource tag describes one component or archive of the project. It generates at least one ZIP file, possibly one for each CPU and OS combination.<p>

The following attributes are legal:

<ul>
<li>name - the file name of the archive, if a * appears it is replaced by the OS/CPU spec (e.g. "linux-x86") for the local platform when building; You must include a * if there is more than one Platform spec
<li>buildbase - the directory in which to look for files, this attribute is valid here, in Platform, and Files; instead of a relative (to this file) or absolute path you can specify a program to determine the path, e.g. this will give you the Qt root dir: <tt>buildbase="|qmake -query QT_INSTALL_PREFIX"</tt>
<li>zipbase - the directory inside the archive file, or in the final installation, this must be a relative path
</ul>

These tags are not copied to the index file, instead &lt;Archive> tags are generated.

<h4>Platform Specs</h4>

Each Platform tag contains a list of files or file patterns for one platform. The following attributes can appear directly inside a Platform tag:
<ul>
<li>os="..." - operating system, valid values are: "linux", "windows", "any", "all" (the default is "any")<ul>
		<li>any=same spec for all OS, separate archive files
		<li>all=same spec for all OS, just one archive file for all of them
		</ul></li>
<li>cpu="..." - the CPU type this platform refers to, valid values are: "x86", "x86-64", "any", "all" (default "any")<ul>
		<li>any=same spec for all CPUs, separate archive files
		<li>all=same spec for all CPUs, just one archive file for all of them
		</ul></li>
<li>buildbase/zipbase - see above, if one is specified here it is appended to the one from above</li>
</ul>

Each Platform tag must contain one or multiple Files tags, with the following attributes:
<ul>
<li>exclude - an optional pattern that tells instgen which files never to include, even if they match an individual pattern
<li>buildbase/zipbase - see above
</ul>

The content of this tag: every non-empty line is a file name or pattern that is searched in the current buildbase directory and included below zipbase in the archive.<p>

Note on patterns: they are similar, but not identical to real shell patterns. The wildcards "?" and "*" can also match directory separators.

<h4>GnuPG Note</h4>

The example above contains a specification for an updatekeys.zip file. This or a similar spec must exist in a valid Aurora description file. Otherwise the target installation will be unable to update itself, since this data is needed for signature verification.<p>

See the <a href="packages.html">packaging</a> documentation for details.

<h2>Package Meta-Data/Index File</h2>

The CurrentVersion and Settings tags are copied to the index file. During step 2 instgen adds a buildDate="..." attribute to the CurrentVersion tag in the index file with the current time as of the instgen run in it. The fulltargetdir attribute of the &lt;Settings> tag is removed.<p>

The &lt;ArchiveSource> tags are not copied, however it generates &lt;Archive> tags for each archive that it creates containing details about the corresponding archive file. E.g. on a 64bit Linux the above generates:

<pre>
&lt;AuroraInfo>
  &lt;CurrentVersion version="2.0.1" mrv="020001" <font color="red">buildDate="2013-01-22T13:56:32"</font>/>
  
  &lt;Settings baseurl="http://www.example.com/myprogram" indexfile="myindex.xml" 
    pollinterval="1800"/>

  &lt;Archive name="myprogram-linux-x86-64.zip" size="2833008" cpu="x86-64" os="linux"
    sha256sum="b2e037f60c57f15f952cd826617a7ffb485a0203d38b977719d0700bfd887dfa">
        ./myprogram
        ./gpg
        ./libAurora.so
        ./libAurora.so.1
        ./libAurora.so.1.0
        ./libAurora.so.1.0.0
        ./libQtZipHelper.so
        ./libQtZipHelper.so.1
        ./libQtZipHelper.so.1.0
        ./libQtZipHelper.so.1.0.0
  &lt;/Archive>

  &lt;Archive name="updatekeys.zip" size="1307" cpu="all" os="all"
    sha256sum="66caecb9a29ce2ba4c66dde099af5205e1647b1086bbed976f54679962d5eb46">
      aurora-gpg/gpg.conf
      aurora-gpg/pubring.gpg
  &lt;/Archive>
&lt;/AuroraInfo>
</pre>

<h2>Cached Index File</h2>

Once your program picks up the update and installs it, the index file that is copied into the root dir of the program gains an installDate attribute at the Settings tag (ISO timestamp), but is otherwise identical to the above.


</html>
