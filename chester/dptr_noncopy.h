//d-ptr header
//
// (c) Konrad Rosenbaum, 2010-2011
// Copying and distribution of this file, with or without modification,
// are permitted in any medium without royalty provided the copyright
// notice and this notice are preserved.  This file is offered as-is,
// without any warranty.

#include "dptr_base.h"


#ifndef DPTR_NONCOPY_CLASS_0_1_H
#define DPTR_NONCOPY_CLASS_0_1_H
//hide the namespace
/// \cond never
namespace Chester_0_1{
/// \endcond

/** \brief Base class of non-shared d-pointers.

Use in conjunction with DECLARE_NONCOPY_DPTR and DEFINE_NONCOPY_DPTR */
class NonCopyDPtr
{
	public:
		///instantiates a non-shared d-pointer
		NonCopyDPtr(){}
		///deletes a non-shared d-pointer
		virtual ~NonCopyDPtr(){}
	private:
		///hides the copy constructor
		NonCopyDPtr(const NonCopyDPtr&){}
		///hides the assignment operator
		NonCopyDPtr& operator=(const NonCopyDPtr&){return *this;}
};

//hide the namespace
/// \cond never
};
using namespace Chester_0_1;
/// \endcond
#endif

#ifdef DEFINE_NONCOPY_DPTR
#undef DEFINE_NONCOPY_DPTR
#endif

/** \brief Creates definitions for methods of the non-shared, non-copy d-pointer wrapper. 

This variant is not shared between instances of the containing class.

To be used in implementation where the actual d-pointer class is implemented.

\param Class the base class within which the d-pointer was declared*/
#define DEFINE_NONCOPY_DPTR(Class) \
 Class::DPrivate::DPrivate(){d=new Class::Private;}\
 Class::DPrivate::DPrivate(const Class::DPrivate&){}\
 Class::DPrivate::~DPrivate(){delete d;}\
 Class::DPrivate& Class::DPrivate::operator=(const Class::DPrivate&)\
 {return *this;}
