#include <DPtrBase>

#include <QString>

class ClassWithDptr
{
	DECLARE_DPTR(d)
	public:
		QString toString()const;
		int num()const;
		void setNum(int);
};

class ClassWithSDptr
{
	DECLARE_SHARED_DPTR(d)
	public:
		QString toString()const;
		int num()const;
		void setNum(int);
		ClassWithSDptr clone()const;
};

class ClassWithNDptr
{
	DECLARE_NONCOPY_DPTR(d)
	public:
		QString toString()const;
};


#include <QObject>

class DPtrTest:public QObject
{
	Q_OBJECT
	private slots:
		void simpleDP();
		void sharedDP();
		void noncopyDP();
};
