TEMPLATE = app
TARGET = dptrtest
QT -= gui
CONFIG += qtestlib debug
INCLUDEPATH += .. ../../include/chester

SOURCES += dptrtest.cpp dptrpriv.cpp
HEADERS += dptrtest.h
