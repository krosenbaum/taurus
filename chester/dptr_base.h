//d-ptr header
//
// (c) Konrad Rosenbaum, 2010-2013
// Copying and distribution of this file, with or without modification,
// are permitted in any medium without royalty provided the copyright
// notice and this notice are preserved.  This file is offered as-is,
// without any warranty.


#ifdef DPTR_CLASS_NAME
#undef DPTR_CLASS_NAME
#endif

#ifdef DPTR_NAME
#undef DPTR_NAME
#endif

#ifdef DPTR_WRAPPER_NAME
#undef DPTR_WRAPPER_NAME
#endif

/** \def DPTR_CLASS_NAME(Class)
\brief Expands to the fully qualified name of the d-pointer class.
\param Class the fully qualified name of the class the d-pointer was declared in.*/
#define DPTR_CLASS_NAME(Class) Class::Private

/** \def DPTR_NAME
\brief Expands to the local name of d-pointer classes (Private).
*/
#define DPTR_NAME Private

/** \def DPTR_WRAPPER_NAME
Expands to the local name of the d-pointer wrapper class (DPrivate).
*/
#define DPTR_WRAPPER_NAME DPrivate

#ifdef DECLARE_DPTR
#undef DECLARE_DPTR
#endif

/** \def DECLARE_DPTR(dp)
\brief Declares a smart non-shared d-pointer, to be used inside class declaration.

It also declares the internal nested classes DPrivate and Private - Private is the actual d-pointer class, while DPrivate is a wrapper that automatically allocates, copies and deallocates the d-pointer.

The wrapper DPointer class contains these methods:
   - constructor, copy constructor for creating instances of Private
    - used by automatic and explicit constructors of the containing class
   - destructor for deallocation of Private
    - used by the destructor of the containing class
   - assignment operator to copy the content of Private
    - used by the assignment operator of the containing class
   - pointer operator to actually access the Private data

You can use DEFINE_DPTR to define the necessary methods for a non-shared d-pointer. It is recommended (but not necessary) that non-shared d-pointer classes (Private) are derived from DPtr.

The d-pointer class Private is only forward declared, you have to fully declare and implement it in the code where you are using it, i.e. where you are implementing the containing class.

Use the DECLARE_DPTRX version if your surrounding class is part of a library's exported API.

\param dp name of the d-pointer
\param export_spec use Q_DECL_EXPORT or Q_DECL_IMPORT to signal public API export or import
*/
#define DECLARE_DPTR(dp) DECLARE_DPTRX(dp,)
#define DECLARE_DPTRX(dp,export_spec) \
 private:\
 class Private; \
 class export_spec DPrivate{\
  public:DPrivate();DPrivate(const DPrivate&);~DPrivate();\
  DPrivate&operator=(const DPrivate&);\
  const Private*operator->()const{return d;}\
  Private*operator->(){return d;}\
  DPrivate clone()const;\
  private:Private*d;\
 }; \
 DPrivate dp;

#ifdef DECLARE_SHARED_DPTR
#undef DECLARE_SHARED_DPTR
#endif

/** \brief Declares a smart shared d-pointer, to be used inside class declaration.

It also declares the internal nested classes DPrivate and Private - Private is the actual d-pointer class, while DPrivate is a wrapper that automatically allocates, copies and deallocates the d-pointer.

The wrapper DPointer class contains these methods:
   - constructor, copy constructor for creating instances of Private
    - used by automatic and explicit constructors of the containing class
   - destructor for deallocation of Private
    - used by the destructor of the containing class
   - assignment operator to copy the content of Private
    - used by the assignment operator of the containing class
   - pointer operator to actually access the Private data

You can use DEFINE_DPTR to define the necessary methods for a non-shared d-pointer or DEFINE_SHARED_DPTR if you want to share d-pointer data between instances of the containing class. It is recommended that non-shared d-pointer classes (Private) are derived from DPtr and the shared variants be derived from SharedDPtr.

The d-pointer class Private is only forward declared, you have to fully declare and implement it in the code where you are using it, i.e. where you are implementing the containing class.

Warning: shared d-pointers are not thread safe and they are only re-entrant if instances sharing the same d-pointer are only located in one thread, while instances with different d-pointers may be spread over different threads.

Use the DECLARE_SHARED_DPTRX version if your surrounding class is part of a library's exported API.

\param dp name of the d-pointer
\param export_spec use Q_DECL_EXPORT or Q_DECL_IMPORT to signal public API export or import
*/
#define DECLARE_SHARED_DPTR(dp) DECLARE_SHARED_DPTRX(dp,)
#define DECLARE_SHARED_DPTRX(dp,export_spec) \
 private:\
 class Private; \
 class export_spec DPrivate{\
  public:DPrivate();DPrivate(const DPrivate&);~DPrivate();\
  DPrivate&operator=(const DPrivate&);\
  const Private*operator->()const{return d;}\
  Private*operator->(){return d;}\
  DPrivate clone()const;\
  void decouple();\
  private:Private*d;\
 }; \
 DPrivate dp;


#ifdef DECLARE_NONCOPY_DPTR
#undef DECLARE_NONCOPY_DPTR
#endif

/** \brief Declares a smart non-shared and non-copyable d-pointer, to be used inside class declaration.

It also declares the internal nested classes DPrivate and Private - Private is the actual d-pointer class, while DPrivate is a wrapper that automatically allocates, and deallocates the d-pointer. These are usable for content classes that do not allow copying (e.g. Qt's QObject and its subclasses).

The wrapper DPointer class contains these methods:
   - constructor for creating instances of Private
    - used by automatic and explicit constructors of the containing class
   - destructor for deallocation of Private
    - used by the destructor of the containing class
   - private assignment operator and copy constructor
    - effectively hiding and blocking them
   - pointer operator to actually access the Private data

You can use DEFINE_NONCOPY_DPTR to define the necessary methods for a non-shared, non-copy d-pointer. It is recommended that d-pointer classes (Private) are derived from NonCopyDPtr.

The d-pointer class Private is only forward declared, you have to fully declare and implement it in the code where you are using it, i.e. where you are implementing the containing class.

Use the DECLARE_NONCOPY_DPTRX version if your surrounding class is part of a library's exported API.

\param dp name of the d-pointer
\param export_spec use Q_DECL_EXPORT or Q_DECL_IMPORT to signal public API export or import
*/
#define DECLARE_NONCOPY_DPTR(dp) DECLARE_NONCOPY_DPTRX(dp,)
#define DECLARE_NONCOPY_DPTRX(dp,export_spec) \
 private:\
 class Private; \
 class export_spec DPrivate{\
  public:DPrivate();~DPrivate();\
  const Private*operator->()const{return d;}\
  Private*operator->(){return d;}\
  private:Private*d;\
  DPrivate(const DPrivate&);DPrivate&operator=(const DPrivate&);\
 }; \
 DPrivate dp;
