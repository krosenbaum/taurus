//d-ptr header
//
// (c) Konrad Rosenbaum, 2010-2011
// Copying and distribution of this file, with or without modification,
// are permitted in any medium without royalty provided the copyright
// notice and this notice are preserved.  This file is offered as-is,
// without any warranty.

#include "dptr_base.h"


#ifndef DPTR_CLASS_0_1_H
#define DPTR_CLASS_0_1_H

//hide the namespace
/// \cond never
namespace Chester_0_1{
/// \endcond

/** \brief Base class of non-shared d-pointers.

Use in conjunction with DECLARE_DPTR and DEFINE_DPTR */
class DPtr
{
	public:
		///instantiates a non-shared d-pointer
		DPtr(){}
		///deletes a non-shared d-pointer
		virtual ~DPtr(){}
};

//hide the namespace
/// \cond never
};
using namespace Chester_0_1;
/// \endcond
#endif

#ifdef DEFINE_DPTR
#undef DEFINE_DPTR
#endif

/** \brief Creates definitions for methods of the non-shared d-pointer wrapper. 

This variant is not shared between instances of the containing class, but it is able to copy its content (using the contents copy constructor and assignment operator). You cannot use this variant if any of the content classes have inaccessable copy constructors or assignment operators (like Qt's QObject and its subclasses).

To be used in implementation where the actual d-pointer class is implemented.

\param Class the base class within which the d-pointer was declared*/
#define DEFINE_DPTR(Class) \
 Class::DPrivate::DPrivate(){d=new Class::Private;}\
 Class::DPrivate::DPrivate(const Class::DPrivate&dp){d=new Class::Private(*(dp.d));}\
 Class::DPrivate::~DPrivate(){delete d;}\
 Class::DPrivate Class::DPrivate::clone()const{DPrivate r;*(r.d)=*d;return r;}\
 Class::DPrivate& Class::DPrivate::operator=(const Class::DPrivate&dp)\
 {if(d!=dp.d)*d=*(dp.d);return *this;}

