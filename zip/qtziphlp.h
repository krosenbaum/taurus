//
// Description: Zip helper, base class
//

/* Copyright (c) 2013 Konrad Rosenbaum <konrad@silmor.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#ifndef ZIPHELPER_QTZIPHLP_H
#define ZIPHELPER_QTZIPHLP_H

#include "qtzipbase.h"
#include <QDateTime>

class QIODevice;

namespace QtZipHelper {

class Zip;
class Unzip;

///Helper class: reports information about a specific file entry in the ZIP archive.
class ZIPHLP_EXPORT ZipFileInfo
{
        public:
                ///Empty/invalid info object.
                ZipFileInfo();
                ///Copy an info object.
                ZipFileInfo(const ZipFileInfo&);
                ///Copy an info object.
                ZipFileInfo& operator=(const ZipFileInfo&);
                
                ///returns true if this is a valid info object
                bool isValid()const{return !mName.isEmpty();}
                
                ///returns true if this is a valid info object, so the info object
                ///can be used in if-statements directly
                operator bool()const{return !mName.isEmpty();}
                
                ///returns the file name of the stored file
                QString fileName()const{return mName;}
                ///returns the creation time of the file stored in the ZIP
                QDateTime createTime()const{return mTime;}
                ///returns the code of the compression method used with this file
                int rawCompressionMethod()const{return mMethod;}
                ///returns the CRC32 stored for this file
                ulong fileCRC()const{return mCRC;}
                ///returns the uncompressed file size stored for this file
                ulong uncompressedSize()const{return mUSize;}
                
        private:
                friend class Zip;
                friend class Unzip;
                ///\internal instantiates an info object with actual info
                ZipFileInfo(const QString&,const QDateTime&,int,ulong,ulong);
                
                QDateTime mTime;
                QString mName;
                int mMethod;
                ulong mCRC,mUSize;
};

///This class represents a ZIP file being assembled.
class ZIPHLP_EXPORT Zip:public ZipHlpBase
{
        Q_OBJECT
        friend class Unzip;
        public:
                ///create a new ZIP object
                explicit Zip(QObject* parent = 0);
                ///delete the ZIP object and implicitly close it
                virtual ~Zip();
                
                ///Defines how the ZIP file is opened.
                enum OpenMode {
                        ///Creates a fresh ZIP archive, even if the file exists.
                        OpenTruncate=0,
                        ///If the file is new: create it, if it exists: append more files to the existing archive.
                        OpenAppend=1
                };
                ///returns true if the ZIP file is open for adding files
                bool isOpen() const;
        public slots:
                ///attached this ZIP object to the given device, does not take ownership of the device,
                ///you are responsible for closing or reparenting it
                virtual bool open(QIODevice *d, OpenMode mode=OpenTruncate);
                ///closes the ZIP object (writing headers, trailers, etc.), but does not close the
                ///actual QIODevice
                virtual void close();
                ///stores the content of the given QIODevice in the ZIP archive, compressing
                ///it at the same time, the method assumes that the device is at the correct
                ///position to start reading (i.e. if it is a file: you are responsible for
                ///seeking to the start first)
                virtual bool storeFile(const QString &name, QIODevice &file,
                        const QDateTime &time = QDateTime::currentDateTime());
                ///stores the byte array as a file in the ZIP file, compressing it
                virtual bool storeFile(const QString&name, QByteArray content,
                        const QDateTime &time = QDateTime::currentDateTime());
                ///assumes the file is properly compressed and the info object contains the
                ///correct information - use this for copying from an Unzip object
                virtual bool storeRawFile(const ZipFileInfo&info, QIODevice &file);
                ///assumes the file is properly compressed and the info object contains the
                ///correct information - use this for copying from an Unzip object
                virtual bool storeRawFile(const ZipFileInfo&info, QByteArray content);
};

///This class represents a ZIP file being read.
class ZIPHLP_EXPORT Unzip:public ZipHlpBase
{
        Q_OBJECT
        public:
                ///create a new ZIP object
                explicit Unzip(QObject*parent=0);
                ///delete the ZIP object
                virtual ~Unzip();
                ///returns true if the ZIP file is currently open for reading
                virtual bool isOpen() const;
        public slots:
                ///attaches a QIODevice to the ZIP object, does not take ownership of the device,
                ///you are responsible for closing or reparenting it
                virtual bool open(QIODevice *d);
                ///closes the ZIP object (abandoning internal state), but does not close the
                ///actual QIODevice
                virtual void close();
                ///returns the amount of files stored in this ZIP file
                virtual int fileCount()const;
                ///jumps to the first file stored in the ZIP file, returning its information
                virtual ZipFileInfo firstFile();
                ///moves to the next file in the ZIP file, returning its information
                virtual ZipFileInfo nextFile();
                ///tries to find a file with the given name, returning its information
                virtual ZipFileInfo locateFile(const QString &name, Qt::CaseSensitivity cs = Qt::CaseSensitive);
                ///returns information about the file currently being unpacked
                virtual ZipFileInfo currentFile()const;
                ///returns information about the file currently being unpacked 
                ///and stores its uncompressed content in the given QIODevice
                virtual ZipFileInfo currentFile(QIODevice &d)const;
                ///returns information about the file currently being unpacked 
                ///and stores its uncompressed content in the given byte array
                virtual ZipFileInfo currentFile(QByteArray&)const;
                ///returns the uncompressed content of the current file
                virtual QByteArray currentFileContent()const;
                ///returns information about the file currently being unpacked 
                ///and stores its still compressed content in the given QIODevice
                virtual ZipFileInfo currentFileRaw(QIODevice &d)const;
                ///returns information about the file currently being unpacked 
                ///and stores its still compressed content in the given byte array
                virtual ZipFileInfo currentFileRaw(QByteArray&)const;
                ///returns the still compressed content of the current file
                virtual QByteArray currentFileRawContent()const;
                ///returns the name of the file currently being uncompressed
                virtual QString currentFileName()const;
                ///copies the current file to the given Zip object, returns true on success
                virtual bool copyCurrentFile(Zip&)const;
};

//end of namespace
};

using namespace QtZipHelper;

#endif
