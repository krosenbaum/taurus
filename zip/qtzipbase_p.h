//
// Description: Zip helper, base class, private parts

/* Copyright (c) 2013 Konrad Rosenbaum <konrad@silmor.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "qtzipbase.h"
#include "zlib.h"
#include "ioapi.h"
#include "zip.h"
#include "unzip.h"
#include <string.h>
#include <QIODevice>

class QIODevice;

///\internal private data of ZipHlpBase
struct QtZipHelper::ZipHlpBase::Private {
        QIODevice*device;
        zlib_filefunc_def ffunc;
        zipFile zip;
        unzFile unzip;
        
        void setDevice(QIODevice*d){
                if(d && d->isOpen()){
                        device=d;
                        ffunc.opaque=d;
                }else{
                        device=0;
                        ffunc.opaque=0;
                }
        }
        
        Private(){
                memset(&ffunc,0,sizeof(ffunc));
                zip=0;unzip=0;device=0;
        }
        ~Private(){
//                 if(device)delete device;
                device=0;
                ffunc.opaque=0;
        }
};
