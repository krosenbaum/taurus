//
// Description: Zip helper, base class
//

/* Copyright (c) 2013 Konrad Rosenbaum <konrad@silmor.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#ifndef ZIPHELPER_QTZIPBASE_H
#define ZIPHELPER_QTZIPBASE_H

#include <QObject>


#if defined(ZIPHLP_LIBRARY_BUILD)
#  define ZIPHLP_EXPORT Q_DECL_EXPORT
#else
#  define ZIPHLP_EXPORT Q_DECL_IMPORT
#endif

namespace QtZipHelper {
///Base class of Zip and Unzip, see those for details
class ZIPHLP_EXPORT ZipHlpBase:public QObject
{
        public:
                ///create Zip object with parent
                explicit ZipHlpBase(QObject* parent = 0);
                ///delete the object
                virtual ~ZipHlpBase();
                
                ///abstract base function: return true if the ZIP file is open
                virtual bool isOpen()const =0;
                ///abstract base function: close the ZIP file
                virtual void close()=0;
        protected:
                struct Private;
                Private*d;
};

//end of namespace
};

using namespace QtZipHelper;

#endif
