//
// Description: Zip helper, base class

/* Copyright (c) 2013-17 Konrad Rosenbaum <konrad@silmor.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "qtzipbase_p.h"
#include "qtziphlp.h"

#include <QBuffer>
#include <QDebug>

static const int BUFSIZE=4096;

//###################################################################
// ZIP file information wrapper

ZipFileInfo::ZipFileInfo()
{
        mMethod=-1;
        mCRC=mUSize=0;
}

ZipFileInfo::ZipFileInfo(const ZipFileInfo& i)
{
        operator=(i);
}

ZipFileInfo& ZipFileInfo::operator=(const ZipFileInfo& i)
{
        mName=i.mName;
        mTime=i.mTime;
        mMethod=i.mMethod;
        mCRC=i.mCRC;
        mUSize=i.mUSize;
        return *this;
}

ZipFileInfo::ZipFileInfo(const QString& n, const QDateTime& d, int m, ulong c, ulong u)
{
        mName=n;
        mTime=d;
        mMethod=m;
        mCRC=c;
        mUSize=u;
}


//###################################################################
// ZIP packer

Zip::Zip(QObject* parent): ZipHlpBase(parent)
{
}

Zip::~Zip()
{
        if(isOpen())close();
}

bool Zip::open(QIODevice* dev, Zip::OpenMode mode)
{
        if(isOpen()){
                qWarning()<<"Zip::open trying to open another device, while already open. Call close() first.";
                return false;
        }
        if(!dev){
                qWarning()<<"Zip::open called with NULL device, are you joking?";
                return false;
        }
        if(!dev->isOpen() || !dev->isWritable() || dev->isSequential()){
                qWarning()<<"Zip::open failed: the device must be open, writeable and random-access!";
                return false;
        }
        d->setDevice(dev);
        d->zip=zipOpen2("",mode==OpenAppend?APPEND_STATUS_ADDINZIP:0,0,&d->ffunc);
        return d->zip!=0;
}

bool Zip::isOpen() const
{
        return d->zip!=0;
}

void Zip::close()
{
        if(!isOpen()){
                qDebug()<<"Zip::close called twice. Ignoring.";
                return;
        }
        zipClose(d->zip,"");
        d->zip=0;
}

bool Zip::storeFile(const QString& name, QIODevice& file, const QDateTime& dtime)
{
        if(!isOpen()){
                qWarning()<<"Called Zip::storeFile while no zip is open.";
                return false;
        }
        if(!file.isReadable()){
                qWarning()<<"Zip::storeFile failed: the device to copy data from is not readable.";
                return false;
        }
        
        //create meta data
        zip_fileinfo info;
        const QTime&time=dtime.time();
        info.tmz_date.tm_hour=time.hour();
        info.tmz_date.tm_min=time.minute();
        info.tmz_date.tm_sec=time.second();
        const QDate&date=dtime.date();
        info.tmz_date.tm_year=date.year();
        info.tmz_date.tm_mon=date.month()-1;
        info.tmz_date.tm_mday=date.day();
        info.dosDate=info.external_fa=info.internal_fa=0;
        
        //create new header
        const int r=zipOpenNewFileInZip(d->zip, name.toLatin1(), &info, 0,0,0,0,0, Z_DEFLATED, Z_DEFAULT_COMPRESSION);
        if(r!=ZIP_OK){
                qWarning()<<"Zip::storeFile unable to create new ZIP entry, code"<<r;
                return false;
        }
        
        //add data
        char buf[BUFSIZE];int l;
        while((l=file.read(buf,BUFSIZE))>0)
                zipWriteInFileInZip(d->zip, buf, l);
        zipCloseFileInZip(d->zip);
        
        return l>=0;
}

bool Zip::storeFile(const QString& name, QByteArray content, const QDateTime& time)
{
        if(!isOpen()){
                qWarning()<<"Called Zip::storeFile while no zip is open.";
                return false;
        }
        QBuffer buf;
        buf.setBuffer(&content);
        buf.open(QIODevice::ReadOnly);
        return storeFile(name,buf,time);
}

bool Zip::storeRawFile(const ZipFileInfo& zinfo, QIODevice& file)
{
        if(!isOpen()){
                qWarning()<<"Called Zip::storeRawFile while no zip is open.";
                return false;
        }
        if(!file.isReadable()){
                qWarning()<<"Zip::storeRawFile failed: the device to copy data from is not readable.";
                return false;
        }
        
        //create meta data
        zip_fileinfo info;
        const QTime&time=zinfo.createTime().time();
        info.tmz_date.tm_hour=time.hour();
        info.tmz_date.tm_min=time.minute();
        info.tmz_date.tm_sec=time.second();
        const QDate&date=zinfo.createTime().date();
        info.tmz_date.tm_year=date.year();
        info.tmz_date.tm_mon=date.month()-1;
        info.tmz_date.tm_mday=date.day();
        info.dosDate=info.external_fa=info.internal_fa=0;
        
        //create new header
        const int r=zipOpenNewFileInZip2(d->zip, zinfo.fileName().toLatin1(), &info, 0,0,0,0,0, zinfo.rawCompressionMethod(), Z_DEFAULT_COMPRESSION, 1);
        if(r!=ZIP_OK){
                qWarning()<<"Zip::storeRawFile unable to create new ZIP entry, code"<<r;
                return false;
        }
        
        //add data
        char buf[BUFSIZE];int l;
        while((l=file.read(buf,BUFSIZE))>0)
                zipWriteInFileInZip(d->zip, buf, l);
        zipCloseFileInZipRaw(d->zip, zinfo.uncompressedSize(), zinfo.fileCRC());
        
        return l>=0;
}

bool Zip::storeRawFile(const ZipFileInfo& info, QByteArray content)
{
        if(!isOpen()){
                qWarning()<<"Called Zip::storeRawFile while no zip is open.";
                return false;
        }
        QBuffer buf;
        buf.setBuffer(&content);
        buf.open(QIODevice::ReadOnly);
        return storeRawFile(info,buf);
}

//###################################################################
// ZIP unpacker

Unzip::Unzip(QObject* parent)
        :ZipHlpBase(parent)
{
}

Unzip::~Unzip()
{
        if(isOpen())close();
}

bool Unzip::isOpen() const
{
        return d->unzip!=0;
}

bool Unzip::open(QIODevice* dev)
{
        if(isOpen()){
                qWarning()<<"Unzip::open called while already open.";
                return false;
        }
        if(dev==0){
                qWarning()<<"Unzip::open called with NULL device. Sorry, but no.";
                return false;
        }
        if(!dev->isOpen() || !dev->isReadable() || dev->isSequential()){
                qWarning()<<"Unzip::open failed: the device must be open, readable and random-access.";
                return false;
        }
        
        d->ffunc.opaque=dev;
        d->unzip=unzOpen2("",&d->ffunc);
        return d->unzip!=0;
}

void Unzip::close()
{
        if(!isOpen()){
                qWarning()<<"Double close on Unzip. Ignoring.";
                return;
        }
        unzClose(d->unzip);
        d->unzip=0;
}


int Unzip::fileCount() const
{
        if(!isOpen()){
                qWarning()<<"Unzip::fileCount failed: no zip file open.";
                return 0;
        }
        unz_global_info info;
        if(unzGetGlobalInfo(d->unzip,&info) == UNZ_OK)
                return info.number_entry;
        else{
                qWarning()<<"Unzip::fileCount failed, assuming it is empty.";
                return 0;
        }
}

ZipFileInfo Unzip::firstFile()
{
        if(!isOpen()){
                qWarning()<<"Unzip::firstFile failed: no zip file open.";
                return ZipFileInfo();
        }
        if(unzGoToFirstFile(d->unzip) == UNZ_OK)
                return currentFile();
        else{
                qWarning()<<"Unzip::firstFile cannot go to first file.";
                return ZipFileInfo();
        }
}

ZipFileInfo Unzip::nextFile()
{
        if(!isOpen()){
                qWarning()<<"Unzip::nextFile failed: no zip file open.";
                return ZipFileInfo();
        }
        if(unzGoToNextFile(d->unzip) == UNZ_OK)
                return currentFile();
        else
                return ZipFileInfo();
}

ZipFileInfo Unzip::locateFile(const QString& name, Qt::CaseSensitivity cs)
{
        if(!isOpen()){
                qWarning()<<"Unzip::locateFile failed: no zip file open.";
                return ZipFileInfo();
        }
        if(unzLocateFile(d->unzip,name.toLatin1(),cs==Qt::CaseSensitive?1:2) == UNZ_OK)
                return currentFile();
        else{
                qWarning()<<"Unable to locate file"<<name<<"in Unzip";
                return ZipFileInfo();
        }
}

QString Unzip::currentFileName() const
{
        return currentFile().fileName();
}

ZipFileInfo Unzip::currentFile() const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFile() failed: no zip file open.";
                return ZipFileInfo();
        }
        unz_file_info info;
        if(unzGetCurrentFileInfo(d->unzip,&info,0,0,0,0,0,0) != UNZ_OK){
                qWarning()<<"Unable to get file info for current file in Unzip::currentFile()";
                return ZipFileInfo();
        }
        if(info.size_filename<=0)return ZipFileInfo();
        char buf[info.size_filename];
        if(unzGetCurrentFileInfo(d->unzip,&info,buf,info.size_filename,0,0,0,0) != UNZ_OK)
                return ZipFileInfo();
        return ZipFileInfo(
                QString::fromLatin1(buf,info.size_filename),
                QDateTime(
                        QDate(info.tmu_date.tm_year,info.tmu_date.tm_mon+1,info.tmu_date.tm_mday),
                        QTime(info.tmu_date.tm_hour,info.tmu_date.tm_min,info.tmu_date.tm_sec)
                ),
                info.compression_method,
                info.crc,
                info.uncompressed_size
                );
        
}

ZipFileInfo Unzip::currentFile(QIODevice& dev) const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFile(QIODevice&) failed: no zip file open.";
                return ZipFileInfo();
        }
        if(!dev.isWritable())return ZipFileInfo();
        if(unzOpenCurrentFile(d->unzip) != UNZ_OK){
                qWarning()<<"Unable to open current file in Unzip::currentFile(QIODevice&)";
                return ZipFileInfo();
        }
        char buf[BUFSIZE];int l;
        while((l=unzReadCurrentFile(d->unzip,buf,BUFSIZE)) > 0)
                dev.write(buf,l);
        unzCloseCurrentFile(d->unzip);
        return currentFile();
}

ZipFileInfo Unzip::currentFile(QByteArray& a) const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFile(QByteArray&) failed: no zip file open.";
                return ZipFileInfo();
        }
        a.clear();
        QBuffer buf;
        buf.setBuffer(&a);
        buf.open(QIODevice::WriteOnly);
        return currentFile(buf);
}

QByteArray Unzip::currentFileContent() const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFileContent failed: no zip file open.";
                return QByteArray();
        }
        if(unzOpenCurrentFile(d->unzip) != UNZ_OK){
                qWarning()<<"Unable to open current file in Unzip::currentFileContent";
                return QByteArray();
        }
        QByteArray ret;
        char buf[BUFSIZE];int l;
        while((l=unzReadCurrentFile(d->unzip,buf,BUFSIZE)) > 0)
                ret.append(buf,l);
        unzCloseCurrentFile(d->unzip);
        return ret;
}

ZipFileInfo Unzip::currentFileRaw(QIODevice& dev) const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFileRaw(QIODevice&) failed: no zip file open.";
                return ZipFileInfo();
        }
        if(!dev.isWritable()){
                qWarning()<<"Device must be writeable to retrieve data in Unzip::currentFileRaw(QIODevice&)";
                return ZipFileInfo();
        }
        if(unzOpenCurrentFile2(d->unzip,0,0,1) != UNZ_OK){
                qWarning()<<"Unable to open current file in Unzip::currentFileRaw(QIODevice&)";
                return ZipFileInfo();
        }
        char buf[BUFSIZE];int l;
        while((l=unzReadCurrentFile(d->unzip,buf,BUFSIZE)) > 0)
                dev.write(buf,l);
        unzCloseCurrentFile(d->unzip);
        return currentFile();
}

ZipFileInfo Unzip::currentFileRaw(QByteArray& a) const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFileRaw(QByteArray&) failed: no zip file open.";
                return ZipFileInfo();
        }
        a.clear();
        QBuffer buf;
        buf.setBuffer(&a);
        buf.open(QIODevice::WriteOnly);
        return currentFileRaw(buf);
}

QByteArray Unzip::currentFileRawContent() const
{
        if(!isOpen()){
                qWarning()<<"Unzip::currentFileRawContent failed: no zip file open.";
                return QByteArray();
        }
        if(unzOpenCurrentFile2(d->unzip,0,0,1) != UNZ_OK){
                qWarning()<<"Unable to open current file in Unzip::currentFileRawContent";
                return QByteArray();
        }
        QByteArray ret;
        char buf[BUFSIZE];int l;
        while((l=unzReadCurrentFile(d->unzip,buf,BUFSIZE)) > 0)
                ret.append(buf,l);
        unzCloseCurrentFile(d->unzip);
        return ret;
}

bool Unzip::copyCurrentFile(Zip& zip) const
{
        if(!zip.isOpen()){
                qWarning()<<"Unzip::copyCurrentFile failed: target zip is not open";
                return false;
        }
        if(!isOpen()){
                qWarning()<<"Unzip::copyCurrentFile failed: no zip file open to copy from.";
                return false;
        }
        
        //get meta data
        unz_file_info info;
        if(unzGetCurrentFileInfo(d->unzip,&info,0,0,0,0,0,0) != UNZ_OK){
                qWarning()<<"Unzip::copyCurrentFile failed: Unable to open source entry for reading";
                return false;
        }
        if(info.size_filename<=0)return false;
        char name[info.size_filename+1];
        if(unzGetCurrentFileInfo(d->unzip,&info,name,info.size_filename,0,0,0,0) != UNZ_OK){
                qWarning()<<"Unzip::copyCurrentFile failed: Unable to retrieve source file info";
                return false;
        }
        name[info.size_filename]=0;
        
        //open unzip file
        if(unzOpenCurrentFile2(d->unzip,0,0,1) != UNZ_OK)
                return false;

        //create output meta data
        zip_fileinfo zinfo;
        zinfo.tmz_date.tm_year=info.tmu_date.tm_year;
        zinfo.tmz_date.tm_mon=info.tmu_date.tm_mon;
        zinfo.tmz_date.tm_mday=info.tmu_date.tm_mday;
        zinfo.tmz_date.tm_hour=info.tmu_date.tm_hour;
        zinfo.tmz_date.tm_min=info.tmu_date.tm_min;
        zinfo.tmz_date.tm_sec=info.tmu_date.tm_sec;
        
        //create new header
        if(zipOpenNewFileInZip2(zip.d->zip, name, &zinfo, 0,0,0,0,0, info.compression_method, Z_DEFAULT_COMPRESSION, 1) != ZIP_OK){
                qWarning()<<"Unzip::copyCurrentFile failed: Unable to create destination entry.";
                unzCloseCurrentFile(d->unzip);
                return false;
        }
        
        //add data
        char buf[BUFSIZE];int l;
        while((l=unzReadCurrentFile(d->unzip,buf,BUFSIZE)) > 0)
                zipWriteInFileInZip(zip.d->zip, buf, l);
        unzCloseCurrentFile(d->unzip);
        return zipCloseFileInZipRaw(zip.d->zip, info.uncompressed_size, info.crc) == ZIP_OK;
}
