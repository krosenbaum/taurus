# project file for ZIP wrapper library

TEMPLATE = lib
TARGET = QtZipHelper
CONFIG += dll create_prl separate_debug_info hide_symbols
OBJECTS_DIR = .ctmp
MOC_DIR = .ctmp
RCC_DIR = .ctmp
LIBS += -lz
DEFINES += ZIPHLP_LIBRARY_BUILD=1
DESTDIR = $$PWD/../lib
QT -= gui
VERSION = 

SOURCES += \
		ioapi.c \
		unzip.c \
		zip.c \
		qtzipbase.cpp \
		qtziphlp.cpp

HEADERS += \
		ioapi.h \
		unzip.h \
		zip.h \
		qtzipbase.h \
		qtziphlp.h

