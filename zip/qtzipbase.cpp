//
// Description: Zip helper, base class
//

/* Copyright (c) 2013 Konrad Rosenbaum <konrad@silmor.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */

#include "qtzipbase_p.h"
#include <QIODevice>

//######################################################
// wrapper functions for zlib/zip

static voidpf ZCALLBACK myopen OF((voidpf opaque, const char* , int ))
{
        QIODevice*dev=reinterpret_cast<QIODevice*>(opaque);
        if(dev==0)return 0;
        if(!dev->isOpen())return 0;
        return (voidpf)dev;
}

static uLong  ZCALLBACK myread OF((voidpf opaque, voidpf , void* buf, uLong size))
{
        return reinterpret_cast<QIODevice*>(opaque)->read((char*)buf,size);
}

static uLong ZCALLBACK mywrite OF((voidpf opaque, voidpf , const void* buf, uLong size))
{
        return reinterpret_cast<QIODevice*>(opaque)->write((const char*)buf,size);
}

static long ZCALLBACK mytell OF((voidpf opaque, voidpf ))
{
        return reinterpret_cast<QIODevice*>(opaque)->pos();
}

static long ZCALLBACK myseek OF((voidpf opaque, voidpf , uLong offset, int origin))
{
        QIODevice*dev=reinterpret_cast<QIODevice*>(opaque);
        qint64 pos=offset;
        if(origin==ZLIB_FILEFUNC_SEEK_CUR)pos+=dev->pos();else
        if(origin==ZLIB_FILEFUNC_SEEK_END)pos+=dev->size();
        return dev->seek(pos)?0:-1;
}

static int ZCALLBACK myclose OF((voidpf opaque, voidpf ))
{
//         reinterpret_cast<QIODevice*>(opaque)->close();
        //we don't open it ourselves, so we do not close it either
        return 0;
}

static int ZCALLBACK mytesterror OF((voidpf opaque, voidpf ))
{
        return reinterpret_cast<QIODevice*>(opaque)->errorString().isEmpty()?0:1;
}


//######################################################
// base class, doing the wrapping

QtZipHelper::ZipHlpBase::ZipHlpBase(QObject* parent)
        :QObject(parent)
{
        d=new Private;
        d->ffunc.zopen_file=&myopen;
        d->ffunc.zclose_file=&myclose;
        d->ffunc.zerror_file=&mytesterror;
        d->ffunc.zread_file=&myread;
        d->ffunc.zseek_file=&myseek;
        d->ffunc.ztell_file=&mytell;
        d->ffunc.zwrite_file=&mywrite;
}

QtZipHelper::ZipHlpBase::~ZipHlpBase()
{
        if(d)delete d;
        d=0;
}
